public with sharing class AccountReceivablePageCX{
    public List<Order__c> listOfSIF {get;set;}
    public Order__c currentSIFInfo {get;set;}
    Map<id,Order__c> sifMap = new Map<id,Order__c>();
    Id actualSalesRecTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
    //Pagination Variables
    ApexPages.StandardSetController setController;
    public Boolean hasNext{get;set;}
    public Boolean hasPrevious{get;set;}
    private Integer pageSize = 10;
    public Integer pageNumber = 1;
    public List<Order__c> sifForPaginationList;

    
    public AccountReceivablePageCX(ApexPages.StandardController con){
        currentSIFInfo = new Order__c ();
        listOfSIF = new List<Order__c>();
    }
    
    //Replaced Outstanding_Amount1 to Outstanding_Amount2 - Kimiko Roberto - 03/15/2018 -
    public void initializePage(){
        sifForPaginationList = new List<Order__c>();
        
        listOfSIF = new List<Order__c>();
        if(currentSIFInfo.Account__c!=null){
            for(Order__c ord : [SELECT Id, Name, Invoice_No__c, Order_Value__c, Payment_Term1__c, Overdue__c, Outstanding_Amount2__c, Paid__c, Payment_Made__c,Remark__c, Contested__c, Returned_Check__c, Rehabilitated__c, New_Payment__c FROM Order__c WHERE Outstanding_Amount2__c>0  AND RecordTypeId = :actualSalesRecTypeId AND Account__c = :currentSIFInfo.Account__c AND Paid__c = false]){
                sifForPaginationList.add(ord);                
            }
            if(sifForPaginationList.size()>0){
                setController = new ApexPages.StandardSetController(sifForPaginationList);
                setController.setPageSize(pageSize);
                setController.setpageNumber(pageNumber);
                for(Order__c ord : (List<Order__c>)setController.getRecords().deepClone(true,true,true)){
                    listOfSIF.add(ord);    
                }
                
                if(setController.getHasNext()){
                    hasNext = true;
                }else{
                    hasNext = false;
                }
                if(setController.getHasPrevious()){
                    hasPrevious = true;
                }else{
                    hasPrevious = false;
                }
            }
            System.debug('\n\n\nSIF LIST : ' + listOfSIF + '\n\n\n');
            saveProgress();
        }

    }
    

    public void searchSIFs(){
        List<Order__c> tempListOfSIF =new List<Order__c>();
        saveProgress();
        listOfSIF =new List<Order__c>();
        String soqlClause = 'SELECT id, name, Invoice_No__c, Order_Value__c,Paid__c, Payment_Term1__c, Overdue__c, Outstanding_Amount2__c, Payment_Made__c, '+ 
                                'Remark__c, Contested__c, Returned_Check__c, Rehabilitated__c '+
                            ' FROM Order__c WHERE Outstanding_Amount2__c > 0 AND Paid__c = false AND Account__c = \''+currentSIFInfo.Account__c+'\'   AND RecordTypeId = :actualSalesRecTypeId  ';
        if(String.isNotEmpty(currentSIFInfo.Invoice_No__c)){
            soqlClause+= ' AND Invoice_No__c LIKE \'%'+currentSIFInfo.Invoice_No__c+'%\'';
            
        }
        if(String.isNotEmpty(currentSIFInfo.name)){
            soqlClause+= ' AND name LIKE \'%'+currentSIFInfo.name+'%\'';
        }
        //system.debug('$$ soqlClause:'+soqlClause);
        setController = new ApexPages.StandardSetController(Database.query(soqlClause));
        setController.setPageSize(pageSize);
        setController.setpageNumber(pageNumber);
        tempListOfSIF = (List<Order__c>)setController.getRecords().deepClone(true,true,true);
        if(setController.getHasNext()){
            hasNext = true;
        }else{
            hasNext = false;
        }
        if(setController.getHasPrevious()){
            hasPrevious = true;
        }else{
            hasPrevious = false;
        }
        //tempListOfSIF = Database.query(soqlClause);
        for(Order__c oSearch: tempListOfSIF){
            if(sifMap.containsKey(oSearch.Id)){
                listOfSIF.add(sifMap.get(oSearch.id));
            }else{
                listOfSIF.add(oSearch);
            }
        }
    }
    
    public void nextList(){
        System.debug('\n\n\n****************NEXT\n\n\n');
        //saveProgress();
        listOfSIF = new List<Order__c>();
        if(setController.getHasNext()){
            setController.next();
            for(Order__c oSearch : (List<Order__c>)setController.getRecords().deepClone(true,true,true)){
                if(sifMap.containsKey(oSearch.Id)){
                    listOfSIF.add(sifMap.get(oSearch.id));
                }else{
                    listOfSIF.add(oSearch);
                }
            }
            if(setController.getHasNext()){
                hasNext = true;
            }else{
                hasNext = false;
            }
            if(setController.getHasPrevious()){
                hasPrevious = true;
            }else{
                hasPrevious = false;
            }
        }
    }
    
    public void previousList(){
        saveProgress();
        listOfSIF = new List<Order__c>();
        if(setController.getHasPrevious()){
            setController.previous();
            for(Order__c oSearch : (List<Order__c>)setController.getRecords().deepClone(true,true,true)){
                if(sifMap.containsKey(oSearch.Id)){
                    listOfSIF.add(sifMap.get(oSearch.id));
                }else{
                    listOfSIF.add(oSearch);
                }
            }
            if(setController.getHasNext()){
                hasNext = true;
            }else{
                hasNext = false;
            }
            if(setController.getHasPrevious()){
                hasPrevious = true;
            }else{
                hasPrevious = false;
            }
        }
    }
    
    public void saveProgress(){
        for(Order__c o: listOfSIF){
            sifMap.put(o.id,o);
        }
    }
    
    
    public pageReference saveSIF2(){
        PageReference reidrectPage = new PageReference('/'+ currentSIFInfo.Account__c);
        saveProgress();
        List<Order__c> ordersToUpdate = sifMap.values();
        for(Order__c x :ordersToUpdate){
            if(x.Payment_Made__c==null){
                x.Payment_Made__c=0;
            }
            if(x.New_Payment__c==null){
                x.New_Payment__c=0;
            }
            x.Payment_Made__c+=x.New_Payment__c;
            x.New_Payment__c = null;
        }

        try{
            update ordersToUpdate;
        }catch (DmlException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, String.valueOf(e)));
            return null;
        }
        reidrectPage.setRedirect(true);
        return reidrectPage;



    }

    public pageReference saveAndNewSIF2(){
        PageReference reidrectPage = new PageReference('/apex/AccountReceivablePage');
        saveProgress();
        List<Order__c> ordersToUpdate = sifMap.values();
        for(Order__c x :ordersToUpdate){
            if(x.Payment_Made__c==null){
                x.Payment_Made__c=0;
            }
            if(x.New_Payment__c==null){
                x.New_Payment__c=0;
            }
            x.Payment_Made__c+=x.New_Payment__c;
            x.New_Payment__c = null;
        }
        
        try{
            update ordersToUpdate;
        }catch (DmlException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, String.valueOf(e)));
            return null;
        }

        reidrectPage.setRedirect(true);
        return reidrectPage;

    }

}