/*Iterator class to iterate over the csv data.
 *
 * Revision History:
 *
 * Version     Author           Date         Description
 * 1.0                        01/11/2018     Initial Draft
 */
public with sharing class CSVIterator implements Iterator<String>, Iterable<String>
{
   private String m_CSVData;
   private String m_introValue;
   private Boolean processedHeader = false;
   private List<String> lstHeaders;
   public CSVIterator(String fileData, String introValue, List<String> headersLst) {
      m_CSVData = fileData;
      m_introValue = introValue; 
      lstHeaders = headersLst;
   }
   
   public Boolean hasNext() {
      return m_CSVData.length() > 1 ? true : false;
   }
   
   public String next() {
      String row = m_CSVData.subString(0, m_CSVData.indexOf(m_introValue));
      m_CSVData = m_CSVData.subString(m_CSVData.indexOf(m_introValue) + m_introValue.length(),m_CSVData.length());
      if(processedHeader == false) {
          lstHeaders.addAll(row.split(','));
          processedHeader = true;
      }
      return row;
   }
   
   public Iterator<String> Iterator() {
      return this;   
   }
}