global with sharing class ContactReportForm {


    public ContactReportForm(ApexPages.StandardController objSC){

    }
    @remoteAction
    global static void insertContactReportData(String serializedContactReportObj, String discussionItems, String nextSteps){
        
        StaticResources.changesMadeByWebService = true;

        System.debug('------serializedContactReportObj--------'+serializedContactReportObj);

        Map<String,String> mapContactReportObj = (Map<String,String>) JSON.deserialize (serializedContactReportObj,Map<String,String>.class);

        Contact_Report__c objContRep = new Contact_Report__c();

        objContRep.Territory__c = mapContactReportObj.get('territory');

        if(mapContactReportObj.get('crDate') != '' && null != mapContactReportObj.get('crDate')){
            objContRep.Date__c = Date.parse(mapContactReportObj.get('crDate'));
        }

        objContRep.Outlet_Name__c = mapContactReportObj.get('outletName');
        objContRep.Venue__c = mapContactReportObj.get('place');

        objContRep.Objective__c = mapContactReportObj.get('objectives');
        //System.debug('-------mapContactReportObj.get-----'+mapContactReportObj.get('nextSteps'));
        objContRep.Issues_and_Concerns__c = mapContactReportObj.get('issuesConcerns');
        objContRep.Next_Step__c = mapContactReportObj.get('nextSteps');
        objContRep.Conforme__c = mapContactReportObj.get('conforme');
        objContRep.Id = mapContactReportObj.get('id');
        System.debug('------mapContactReportObj.get(status)------>'+mapContactReportObj.get('status'));
        objContRep.Status__c = mapContactReportObj.get('status');
        objContRep.Attendees__c = mapContactReportObj.get('attendees');

        if (objContRep.Status__c.equalsIgnoreCase('Submitted')) {
            objContRep.Submitted_Date_Time__c = System.now();
            System.debug('ObjectId---'+objContRep.Id);
            Id eformId = [SELECT eForm__c FROM Contact_Report__c WHERE Id = :objContRep.Id][0].eForm__c;
            System.debug('eformId ---'+eformId );
            Task objTask = [SELECT Id FROM Task WHERE eForm__c = :eformId];
            objTask.Task_Completion_Datetime__c = System.now();
            update objTask;
            //
        }
        update objContRep;
        addDiscussionList(objContRep.Id, discussionItems);
        addnextStepList(objContRep.Id, nextSteps);
        
        StaticResources.changesMadeByWebService = false;
        System.debug('-----objContRep-------'+objContRep);
    }

    public static void addDiscussionList(Id contact_report_id, String discussionItems){
        
        StaticResources.changesMadeByWebService = true;
        List<Map<String,String>> discussionItemList = (List<Map<String,String>>)JSON.deserialize(discussionItems, List<Map<String,String>>.class);
        System.debug('...discussionItemList: ' + discussionItemList);

        List<Contact_Report_Discussed_Items__c> discussionItemListtoInsert = new List<Contact_Report_Discussed_Items__c>();
        for(Map<String, String> discussionItemRow : discussionItemList) {

            System.debug('...discussionItemRow: ' + discussionItemRow);
            if(String.isBlank(discussionItemRow.get('Id'))){
                Contact_Report_Discussed_Items__c contactReportDiscussionItem = new Contact_Report_Discussed_Items__c();
                contactReportDiscussionItem.Contact_Report__c = contact_report_id;
                if( String.isNotBlank(discussionItemRow.get('Topic__c')) ) {
                    contactReportDiscussionItem.Topic__c = discussionItemRow.get('Topic__c');
                }
                if( String.isNotBlank(discussionItemRow.get('Agreements_Issues_Concerns__c')) ) {
                    contactReportDiscussionItem.Agreements_Issues_Concerns__c = discussionItemRow.get('Agreements_Issues_Concerns__c');
                }
                discussionItemListtoInsert.add(contactReportDiscussionItem);
            }
        }
        if(discussionItemListtoInsert != null && !discussionItemListtoInsert.IsEmpty()){
            insert discussionItemListtoInsert;
        }
        
        StaticResources.changesMadeByWebService = false;
    }

      public static void addnextStepList(Id contact_report_id, String nextSteps){
          
        StaticResources.changesMadeByWebService = true;
          
        List<Map<String,String>> nextStepsList = (List<Map<String,String>>)JSON.deserialize(nextSteps, List<Map<String,String>>.class);
        System.debug('...nextStepsList: ' + nextStepsList);

        List<Contact_Report_Next_Steps__c> nextStepItemList = new List<Contact_Report_Next_Steps__c>();
        for(Map<String, String> nextStepRow : nextStepsList) {

            System.debug('...NextItemRow: ' + nextStepRow);
            if(String.isBlank(nextStepRow.get('Id'))){
                Contact_Report_Next_Steps__c contactReportStep = new Contact_Report_Next_Steps__c();
                contactReportStep.Contact_Report__c = contact_report_id;
                if( String.isNotBlank(nextStepRow.get('Actvitivies__c')) ) {
                    contactReportStep.Actvitivies__c = nextStepRow.get('Actvitivies__c');
                }
                if( String.isNotBlank(nextStepRow.get('Person_In_Charge__c')) ) {
                    contactReportStep.Person_In_Charge__c = nextStepRow.get('Person_In_Charge__c');
                }
                 if( String.isNotBlank(nextStepRow.get('Target_Date__c')) ) {
                    contactReportStep.Target_Date__c = Date.parse(nextStepRow.get('Target_Date__c'));
                }
                nextStepItemList.add(contactReportStep);
            }
        }
        if(nextStepItemList!= null && !nextStepItemList.IsEmpty()){
            upsert nextStepItemList;
        }
          
        StaticResources.changesMadeByWebService = false;
    }

    global class SearchWrapper {
        global String text;
        global String id;
    }

    @remoteAction
    global static ContRepWrapper getExistingContactReport(Id contRepId) {

        Contact_Report__c objCR =  [SELECT Territory__c
                     , Date__c
                     , Outlet_Name__c
                     , Venue__c
                     , Objective__c
                     , Issues_and_Concerns__c
                     , Next_Step__c
                     , Conforme__c
                     , Attendees__c
                     , Status__c
                     , CreatedBy.Name
                     , CreatedById
                     , eForm__c
                     , eForm__r.OwnerId
                     ,(
                       SELECT Topic__c
                           , Agreements_Issues_Concerns__c
                       FROM Contact_Report_Discussed_Items__r
                       )
                       ,(
                       SELECT Actvitivies__c
                           , Person_In_Charge__c
                           ,Target_Date__c
                       FROM Contact_Report_Next_Steps__r
                    )
                FROM Contact_Report__c
                WHERE Id = :contRepId];

                System.debug('objCR----'+objCR);
                List<Task> objTaskList = [SELECT Check_in__c FROM Task WHERE eForm__c = :objCR.eForm__c];
                System.debug('objTaskList ----'+objTaskList);

                if(objTaskList != null && !objTaskList.isEmpty()) {
                    ContRepWrapper objCRWrap = new ContRepWrapper();
                    objCRWrap.objCR = objCR;
                    objCRWrap.objTask = objTaskList[0];
                    return objCRWrap;
                }
                else {
                    return null;
                }

    }

    global class ContRepWrapper{
        Contact_Report__c objCR;
        Task objTask;
    }

    @remoteAction
    global static Map<String,String> getAccountName(Id eformId) {
        if(eformId != null){
            Map<String,String> response = new Map<String,String>();
            system.debug('eformId ---'+eformId);
            List<Task> taskRecord = [SELECT Id
                                          , StartDateTime__c
                                          , WhatId
                                          , Account_Name__c
                                       FROM Task
                                       WHERE
                                       eForm__c =: eformId
                                       LIMIT 1];
            system.debug('TaskRecord---'+taskRecord );
            if(!taskRecord.IsEmpty() && (taskRecord[0].WhatId != null || taskRecord[0].Account_Name__c != '')){
                List<Account> accountName = [SELECT Name
                                       FROM Account
                                       WHERE Id =: taskRecord[0].WhatId
                                       LIMIT 1];
                DateTime dT = taskRecord[0].StartDateTime__c;
                response.put('StartDate', date.newinstance(dT.year(), dT.month(), dT.day()).format());
                if(!accountName.isEmpty()){
                    response.put('AccountName', accountName[0].Name);
                }
                else {
                    if(taskRecord[0].Account_Name__c != '') {
                        response.put('AccountName', taskRecord[0].Account_Name__c);
                    }
                    else{
                        return null;
                    }
                }
            }else{
                return null;
            }

            return response;
        }
        return null;
    }


}