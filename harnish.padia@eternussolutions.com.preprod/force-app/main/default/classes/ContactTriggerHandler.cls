/*-------------------------------------------------------------------------------------------
Author       :   eugenebasianomutya
Created Date :   May 01 2017 
Definition   :   Handler of the Contact Trigger
History      :  
-------------------------------------------------------------------------------------------*/
public class ContactTriggerHandler implements TriggerHandler {
    
    public void BeforeInsert(List<SObject> newSampleObjectList){
        
    }
    
    public void BeforeUpdate(List<SObject> newSampleObjectList, Map<Id, SObject> newSampleObjectMap,
                             List<SObject> oldSampleObjectList, Map<Id, SObject> oldSampleObjectMap){        

    }

    public void BeforeDelete(List<SObject> oldSampleObjectList, Map<Id, SObject> oldSampleObjectMap){
        
    }
    
    public void AfterInsert(List<SObject> newSampleObjectList, Map<Id, SObject> newSampleObjectMap){
        updateAccountFields(newSampleObjectMap);
    }
    
    public void AfterUpdate(List<SObject> newContactList, Map<Id, SObject> newContactMap,
                            List<SObject> oldContactList, Map<Id, SObject> oldContactMap ){
       updateAccountFields(newContactMap);
    }
    
    public void AfterDelete(List<SObject> oldSampleObjectList, Map<Id, SObject> oldSampleObjectMap){
        updateAccountFields(oldSampleObjectMap);
    }
    
    public void AfterUndelete(List<SObject> newSampleObjectList, Map<Id, SObject> newSampleObjectMap){
        updateAccountFields(newSampleObjectMap);
    }
    
    //AARP: Add new function to calculate Account's roll up fields upon Contact update/insert/delete
    //AARP: Updated to recalculate the Accounts fields regardless of context
    public void updateAccountFields(Map<Id, sObject> contactMap){
        Set<Id> accountIds = new Set<Id>();
        Id sasRecordTypeId = Schema.sObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        
        for(sObject tempContact : contactMap.values()){
            accountIds.add((Id)tempContact.get('AccountId'));
        }
        
        Map<Id, Account> relatedAccounts = new Map<Id, Account>([select Id, Primary_SAS_Contacts__c, Primary_DSP_Contacts__c, (select Id from DSPs__r where Is_Primary__c = true), (select RecordType.DeveloperName from Contacts where Is_Primary__c = true) from Account where Id in :accountIds]);
        
        for(Account relatedAccount : relatedAccounts.values()){
            Integer tempSASCount = 0;
            Integer tempDSPCount = 0;
            
            for(Contact relatedContact : relatedAccount.Contacts){
                if(relatedContact.RecordType.DeveloperName == 'SAS'){
                    tempSASCount++;
                }else if(relatedContact.RecordType.DeveloperName == 'Distributor_Personnel'){
                    tempDSPCount++;
                }
            }
            
            for(DSP__c relatedDSP : relatedAccount.DSPs__r){
                tempDSPCount++;
            }            
            
            relatedAccount.Primary_SAS_Contacts__c = tempSASCount;
            relatedAccount.Primary_DSP_Contacts__c = tempDSPCount;
        }

        List<Database.SaveResult> results = Database.update(relatedAccounts.values(), false);
        
        for(Integer x = 0; x < results.size(); x++){
            if(!results[x].isSuccess()){
                for(Database.Error error : results[x].getErrors()){
                    for(Contact childContact : relatedAccounts.values()[x].Contacts){
                        if(contactMap.containsKey(childContact.Id)){
                            Contact tempContact = (Contact)contactMap.get(childContact.Id);
                            tempContact.addError(error.getMessage());
                        }
                    }
                }
            }
        }
    }
    
    // public void UpdateAccountSalesType(Map<Id, SObject> newContactMap, Map<Id, SObject> oldContactMap ) {

    //     map<Id,Account> mAccount = new map<Id, Account>();
    //     set<Id> sAccountIds = new set<Id>();

    //     // Get Account of Contact
    //     // Get Account of Contact
    //     for(sObject con : newContactMap.values()){
    //             Contact newCon = (Contact) con;
    //             Contact oldCon = (Contact) oldContactMap.get(con.Id);

    //             if(newCon.Sales_Type__c != oldCon.Sales_Type__c) {
    //                 sAccountIds.add(newCon.AccountId);
    //             }
    //     }

        
    //     if(sAccountIds.size()>0){

    //         list<Contact> lContactsOfAccount = new list<Contact>();
    //         lContactsOfAccount = [Select Id, Sales_Type__c, AccountId, RecordType.Name from Contact where AccountId IN: sAccountIds 
    //                                 AND RecordType.Name =:'Distributor Personnel'];

    //         if(lContactsOfAccount.size()>0){
    //             map<Id, Account> mAccountToUpdate = new map<Id,Account>();
    //             for(Contact con: lContactsOfAccount){

    //                 if(con.RecordType.Name == 'Distributor Personnel'){
    //                     Account acc = new Account();
    //                     acc.Id = con.AccountId;
                        
    //                     if(mAccountToUpdate.size()>0){

    //                         if(mAccountToUpdate.get(con.AccountId) != NULL){
    //                             List<String> ContactPicklist = new List<String>();

    //                             if(con.Sales_Type__c != NULL){

    //                                 ContactPicklist = con.Sales_Type__c.split(';');
    //                                 for(string s: ContactPicklist){

    //                                     if(mAccountToUpdate.get(con.AccountId).Sales_Type__c != NULL){
    //                                         if(!mAccountToUpdate.get(con.AccountId).Sales_Type__c.contains(s)){
    //                                             mAccountToUpdate.get(con.AccountId).Sales_Type__c = mAccountToUpdate.get(con.AccountId).Sales_Type__c + ';' + s;
    //                                         }
    //                                     }else{
    //                                         mAccountToUpdate.get(con.AccountId).Sales_Type__c = s;
    //                                     }    
    //                                 }
    //                             }
 
    //                         }else{
    //                             acc.Sales_Type__c = Con.Sales_Type__c;
    //                             mAccountToUpdate.put(acc.id, acc);
    //                         }
    //                     }else{
    //                         acc.Sales_Type__c = Con.Sales_Type__c;
    //                         mAccountToUpdate.put(Con.AccountId, acc);
                            
    //                     }
    //                 }
    //             }

    //             if(mAccountToUpdate.size()>0){
    //                 update mAccountToUpdate.values();

    //             }
    //         }
    //     }

        

    // }   
    
   
}