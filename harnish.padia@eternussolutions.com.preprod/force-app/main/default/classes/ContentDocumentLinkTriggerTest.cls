@isTest
public class ContentDocumentLinkTriggerTest {
    
    private static testmethod void uploadFilePositive() {
        
        Test.startTest();
        
        Upload_Job__c objUpJob = new Upload_Job__c();
        objUpJob.Object_Name__c = 'Stock_Allocation__c';
        insert objUpJob;
        
        Blob csvContent = Blob.valueOf('Index No,Cap Size,Material Code,Percentage for Re Allocation,Primary Material Code,Allocation Priority,Quantity,Re Allocation Limit,Serving Plant Code,Start Date,End Date\r\n1,10,Material Code 1,100,,,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n2,10,Material Code 2,100,Material Code 2,2,50,100,Serv-Code1,03/11/2018,30/11/2018\r\n3,10,Material Code 3,100,Material Code 2,1,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n4,10,Material Code4,100,Material Code 2,3,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n5,10,Material Code 5,100,Material Code 1,1,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n6,10,Material Code 6,100,Material Code 1,2,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n7,10,Material Code 7,100,Material Code 1,4,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n8,10,Material Code 8,100,Material Code 1,3,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n9,10,Material Code 9,100,Material Code 6,1,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n10,10,Material Code 10,100,Material Code 6,2,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n11,10,Material Code 11,100,Material Code 9,1,50,100,Serv-Code 1,03/11/2018,30/11/2018\r\n');
        ContentVersion cv = new ContentVersion();
        cv.title = 'TestContentDocumentLinkTrigger.csv';
        cv.PathOnClient ='test';
        cv.VersionData = csvContent;
        cv.PathOnClient = 'TestContentDocumentLinkTrigger.csv';
        insert cv;
        
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = objUpJob.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Test.stopTest();
        
        List<ContentDocument> conDocLst = [SELECT LatestPublishedVersion.VersionData,
                                                  LatestPublishedVersion.Title,
                                                  CreatedDate
                                           FROM ContentDocument
                                           WHERE LatestPublishedVersion.Title LIKE '%Error%'
                                           ORDER BY CreatedDate ASC]; 
        /*for(ContentDocument conDoc : conDocLst) {
            String body = conDoc.LatestPublishedVersion.VersionData.toString();
            system.debug('=body===='+body);
            DateTime dtTime = conDoc.CreatedDate;
            system.debug('=CreatedDate===='+dtTime.millisecond());
        }*/
        System.assert(!conDocLst.isEmpty());
    }
    

}