/*-------------------------------------------------------------------------------------------
Author       :   Adjell Ian Pabayos
Created Date :   November 27 2017 
Definition   :   Handler of the DSP Trigger
History      :  
-------------------------------------------------------------------------------------------*/
public class DSPTriggerHandler implements TriggerHandler {
    
    public void BeforeInsert(List<SObject> newSampleObjectList){
        
    }
    
    public void BeforeUpdate(List<SObject> newSampleObjectList, Map<Id, SObject> newSampleObjectMap,
                             List<SObject> oldSampleObjectList, Map<Id, SObject> oldSampleObjectMap){        

    }

    public void BeforeDelete(List<SObject> oldSampleObjectList, Map<Id, SObject> oldSampleObjectMap){
        
    }
    
    public void AfterInsert(List<SObject> newSampleObjectList, Map<Id, SObject> newSampleObjectMap){
        updateAccountFields(newSampleObjectMap);
    }
    
    public void AfterUpdate(List<SObject> newContactList, Map<Id, SObject> newContactMap,
                            List<SObject> oldContactList, Map<Id, SObject> oldContactMap ){
       updateAccountFields(newContactMap);
    }
    
    public void AfterDelete(List<SObject> oldSampleObjectList, Map<Id, SObject> oldSampleObjectMap){
        updateAccountFields(oldSampleObjectMap);
    }
    
    public void AfterUndelete(List<SObject> newSampleObjectList, Map<Id, SObject> newSampleObjectMap){
        updateAccountFields(newSampleObjectMap);
    }
    
    //AIRP: Calculates the Accounts Primary DSP Count
    public void updateAccountFields(Map<Id, sObject> dspMap){
        Set<Id> accountIds = new Set<Id>();
        
        for(sObject tempContact : dspMap.values()){
            accountIds.add((Id)tempContact.get('Account__c'));
        }
        
        Map<Id, Account> relatedAccounts = new Map<Id, Account>([select Id, Primary_DSP_Contacts__c, (select Id from DSPs__r where Is_Primary__c = true), (select Id from Contacts where Is_Primary__c = true and RecordType.DeveloperName = 'Distributor_Personnel') from Account where Id in :accountIds]);
        
        for(Account relatedAccount : relatedAccounts.values()){
            Integer tempDSPCount = 0;
            
            for(DSP__c relatedDSP : relatedAccount.DSPs__r){
                tempDSPCount++;
            }
            
            for(Contact relatedContact : relatedAccount.Contacts){
                tempDSPCount++;
            }            
            
            relatedAccount.Primary_DSP_Contacts__c = tempDSPCount;
        }

        List<Database.SaveResult> results = Database.update(relatedAccounts.values(), false);
        
        for(Integer x = 0; x < results.size(); x++){
            if(!results[x].isSuccess()){
                for(Database.Error error : results[x].getErrors()){
                    for(DSP__c childDSP : relatedAccounts.values()[x].DSPs__r){
                        if(dspMap.containsKey(childDSP.Id)){
                            DSP__c tempDSP = (DSP__c)dspMap.get(childDSP.Id);
                            tempDSP.addError(error.getMessage());
                        }
                    }
                }
            }
        }
    }
}