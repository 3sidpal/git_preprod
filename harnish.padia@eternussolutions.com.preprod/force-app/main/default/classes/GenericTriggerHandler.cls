/*
 * Description - This class is handler class for Trigger operations on Contact Report, Service Level, MOM, Trade Visit, Trade Check Objects
 *
 * Version        Date            Author            Description
 * 1.0            25/09/18        Eternus Solutions     Initial Draft
 */
public class GenericTriggerHandler {

    // This method populates Activity Status on Task on basis of End Date Time of Task.
    public void populateActivityStatus(Map<Id, SObject> mapOldRecords, List<SObject> lstRecords) {
        List<String> lstEformIds = new List<String>();
        List<Id> lstTaskIds = new List<Id>();

        for (Sobject records: lstRecords) {
            if(mapOldRecords != null) {
                if ((mapOldRecords.get(String.valueOf(records.get('Id')))).get('Status__c') != 'Submitted' && records.get('Status__c') == 'Submitted') {
                    lstEformIds.add((String)records.get('eForm__c'));
                }
            } else if(records.get('Status__c') == 'Submitted') {
                lstEformIds.add((String)records.get('eForm__c'));
            }
        }

        if (lstEformIds.size() > 0) {
            List<Task> lstTask = [SELECT Id
                                       , Activity_Status__c
                                       , EndDateTime__c
                                       , Account_Configuration__c
                                    FROM Task
                                   WHERE Eform__c IN :lstEformIds
                                     AND EndDateTime__c != null
                                 ];

            List<Task> taskListToUpdate = new List<Task>();

            for (Task objTask : lstTask) {
                DateTime objDate = Datetime.newInstance(objTask.EndDateTime__c.date(), Time.newInstance(23,59,0,0));
                if (System.now() > objDate || objTask.Activity_Status__c == 'Incomplete') {
                    taskListToUpdate.add(new Task(Id = objTask.Id, Activity_Status__c = 'Delayed', Account_Configuration__c = objTask.Account_Configuration__c));
                } else if (System.now() < objDate && (objTask.Activity_Status__c == null || objTask.Activity_Status__c == 'Open') ) {
                    taskListToUpdate.add(new Task(Id = objTask.Id, Activity_Status__c = 'Complete', Account_Configuration__c = objTask.Account_Configuration__c));
                }
            }

            if(!taskListToUpdate.isEmpty()) {
                StaticResources.byPassTaskTrigger = true;
                update taskListToUpdate;
                new TaskTriggerHandler().updateAccountConfigStatus(taskListToUpdate);
                StaticResources.byPassTaskTrigger = false;
            }

            System.debug('--genericTaskUpdate--> ' + lstTask);
        }
    }

    // This method populates Sync Date Time and Submitted Date Time.
    public void populateSyncDateSubmittedDate (Map<Id, SObject> lstoldMap, Map<Id, SObject> lstnewMap) {
        for (Id  objRecord : lstnewMap.keySet()) {
            if (lstoldMap.get(objRecord).get('Sync_Date_Time__c') == null && lstoldMap.get(objRecord).get('Status__c') == null && (String)lstnewMap.get(objRecord).get('Status__c') == 'Saved') {
                SObject objInstance = lstnewMap.get(objRecord);
                objInstance.put('Sync_Date_Time__c', System.now());
            }
            System.debug('--> '+lstoldMap.get(objRecord).get('Submitted_Date_Time__c'));
            System.debug('--> '+lstnewMap.get(objRecord).get('Submitted_Date_Time__c'));
            if (lstoldMap.get(objRecord).get('Submitted_Date_Time__c') != null) {
                SObject objInstance = lstnewMap.get(objRecord);
                objInstance.put('Submitted_Date_Time__c', lstoldMap.get(objRecord).get('Submitted_Date_Time__c'));
            }
            else if (lstoldMap.get(objRecord).get('Submitted_Date_Time__c') == null && (String)lstnewMap.get(objRecord).get('Status__c') == 'Submitted') {
                SObject objInstance = lstnewMap.get(objRecord);
                objInstance.put('Submitted_Date_Time__c', System.now());
            }
        }
    }
}