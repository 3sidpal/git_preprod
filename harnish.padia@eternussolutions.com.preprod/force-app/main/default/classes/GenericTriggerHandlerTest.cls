@isTest
public class GenericTriggerHandlerTest {

    @testSetup
    private static void init() {
        DateTime objDate = Datetime.newInstance(Date.newInstance(2016, 12, 9), Time.newInstance(23,59,0,0));
        List<Task> taskList = DIL_TestDataFactory.getTasks(1, false);
        taskList[0].Work_Type__c = 'Field Work';
        taskList[0].EndDateTime__c = objDate;
        insert taskList;
    }

    @isTest
    public static void checkpopulateActivityStatusForContactReport() {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Contact_Report__c objContactReport = new Contact_Report__c(EForm__c = lstTask[0].EForm__c, Status__c = 'Submitted');
            insert objContactReport;
        Test.stopTest();
        System.assert(objContactReport.Status__c == 'Submitted');

        List<Task> lstTasks = [SELECT Activity_Status__c FROM Task where Id =: lstTask[0].Id ];
        System.assert(lstTasks.size() > 0);
        System.assert(lstTasks[0].Activity_Status__c == 'Delayed');
    }

    @isTest
    public static void checkpopulateActivityStatusForMOM() {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            MOM__c objMom = new MOM__c(EForm__c = lstTask[0].EForm__c, Status__c = 'Submitted');
            insert objMom;
        Test.stopTest();
        System.assert(objMom.Status__c == 'Submitted');

        List<Task> lstTasks = [SELECT Activity_Status__c FROM Task where Id =: lstTask[0].Id ];
        System.assert(lstTasks.size() > 0);
        System.assert(lstTasks[0].Activity_Status__c == 'Delayed');
    }

    @isTest
    public static void checkpopulateActivityStatusForServiceLevel() {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Service_Level__c objServiceLevel = new Service_Level__c(EForm__c = lstTask[0].EForm__c, Status__c = 'Submitted');
            insert objServiceLevel;
        Test.stopTest();
        System.assert(objServiceLevel.Status__c == 'Submitted');

        List<Task> lstTasks = [SELECT Activity_Status__c FROM Task where Id =: lstTask[0].Id ];
        System.assert(lstTasks.size() > 0);
        System.assert(lstTasks[0].Activity_Status__c == 'Delayed');
    }

    @isTest
    public static void checkpopulateActivityStatusForTradeCheck() {
        List<Task> lstTask = [SELECT eForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Trade_Audit__c objTradeCheck= new Trade_Audit__c(eForm__c = lstTask[0].eForm__c, Status__c = 'Submitted');
            insert objTradeCheck;
        Test.stopTest();
        System.assert(objTradeCheck.Status__c == 'Submitted');

        List<Task> lstTasks = [SELECT Activity_Status__c FROM Task where Id =: lstTask[0].Id ];
        System.assert(lstTasks.size() > 0);
        System.assert(lstTasks[0].Activity_Status__c == 'Delayed');
    }

    @isTest
    public static void checkpopulateActivityStatusForTradeVisit() {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Trade_Visit__c objTradeVisit = new Trade_Visit__c(EForm__c = lstTask[0].EForm__c, Status__c = 'Submitted');
            insert objTradeVisit;
        Test.stopTest();
        System.assert(objTradeVisit.Status__c == 'Submitted');

        List<Task> lstTasks = [SELECT Activity_Status__c FROM Task where Id =: lstTask[0].Id ];
        System.assert(lstTasks.size() > 0);
        System.assert(lstTasks[0].Activity_Status__c == 'Delayed');
    }

    @isTest
    public static void checkpopulationOfSyncDate() {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Contact_Report__c objContactReport = new Contact_Report__c(EForm__c = lstTask[0].EForm__c);
            insert objContactReport;
            objContactReport.Status__c = 'Saved';
            update objContactReport;
        Test.stopTest();
        objContactReport = [SELECT Id, Sync_Date_Time__c, Submitted_Date_Time__c,Status__c FROM Contact_Report__c WHERE Id =: objContactReport.Id];
        System.assert(objContactReport.Status__c == 'Saved');
        System.assert(objContactReport.Sync_Date_Time__c != null);
    }

    @isTest
    public static void checkpopulationOfSubmittedDateForContactReport () {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Contact_Report__c objContactReport = new Contact_Report__c(EForm__c = lstTask[0].EForm__c, Status__c= 'Saved');
            insert objContactReport;
            objContactReport.Status__c = 'Submitted';
            objContactReport.Submitted_Date_Time__c = System.now();
            update objContactReport;
            objContactReport.Conforme__c = 'Test';
            update objContactReport;
        Test.stopTest();
        System.assert(objContactReport.Status__c == 'Submitted');
        objContactReport = [SELECT Id, Sync_Date_Time__c, Submitted_Date_Time__c,Status__c FROM Contact_Report__c WHERE Id =: objContactReport.Id];

    }

    @isTest
    public static void checkpopulationOfSubmittedDateForMOM () {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            MOM__c objContactReport = new MOM__c(EForm__c = lstTask[0].EForm__c, Status__c= 'Saved');
            insert objContactReport;
            objContactReport.Status__c = 'Submitted';
            objContactReport.Submitted_Date_Time__c = System.now();
            update objContactReport;
        Test.stopTest();
        objContactReport = [SELECT Id, Sync_Date_Time__c, Submitted_Date_Time__c,Status__c FROM MOM__c WHERE Id =: objContactReport.Id];
        System.assert(objContactReport.Status__c == 'Submitted');
    }

    @isTest
    public static void checkpopulationOfSubmittedDateForServiceLevel () {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Service_Level__c objContactReport = new Service_Level__c(EForm__c = lstTask[0].EForm__c, Status__c= 'Saved');
            insert objContactReport;
            objContactReport.Status__c = 'Submitted';
            objContactReport.Submitted_Date_Time__c = System.now();
            update objContactReport;
        Test.stopTest();
        objContactReport = [SELECT Id, Sync_Date_Time__c, Submitted_Date_Time__c,Status__c FROM Service_Level__c WHERE Id =: objContactReport.Id];
        System.assert(objContactReport.Status__c == 'Submitted');
    }

    @isTest
    public static void checkpopulationOfSubmittedDateForTradeCheck () {
        List<Task> lstTask = [SELECT eForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Trade_Audit__c objContactReport = new Trade_Audit__c(eForm__c = lstTask[0].EForm__c, Status__c= 'Saved');
            insert objContactReport;
            objContactReport.Status__c = 'Submitted';
            objContactReport.Submitted_Date_Time__c = System.now();
            update objContactReport;
        Test.stopTest();
        objContactReport = [SELECT Id, Sync_Date_Time__c, Submitted_Date_Time__c,Status__c FROM Trade_Audit__c WHERE Id =: objContactReport.Id];
        System.assert(objContactReport.Status__c == 'Submitted');
    }

    @isTest
    public static void checkpopulationOfSubmittedDateForTradeVisit () {
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];

        Test.startTest();
            Trade_Visit__c objContactReport = new Trade_Visit__c(EForm__c = lstTask[0].EForm__c, Status__c= 'Saved');
            insert objContactReport;
            objContactReport.Status__c = 'Submitted';
            objContactReport.Submitted_Date_Time__c = System.now();
            update objContactReport;
        Test.stopTest();
        objContactReport = [SELECT Id, Sync_Date_Time__c, Submitted_Date_Time__c,Status__c FROM Trade_Visit__c WHERE Id =: objContactReport.Id];
        System.assert(objContactReport.Status__c == 'Submitted');
    }
}