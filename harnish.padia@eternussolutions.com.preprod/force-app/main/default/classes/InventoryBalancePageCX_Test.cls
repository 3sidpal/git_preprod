@isTest
private class InventoryBalancePageCX_Test {

    @testSetup static void setupData(){
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Id distributorRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Account distributorAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            General_Trade__c = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT01'
            );
        insert distributorAccount;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;

        Warehouse__c wr = new Warehouse__c();
        wr.Account__c = distributorAccount.Id;
        wr.Address__c = distributorAccount.Name + distributorAccount.Id;
        wr.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        insert wr;
        distributorAccount.Warehouse__c = wr.Id;
        update distributorAccount;

        List<Account> accountForInsertList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Sample Grocery2';
        acc.RecordTypeId = customerRecordTypeId;
        acc.Sales_Organization__c  = 'Feeds';
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.Distributor__c = distributorAccount.Id;
        acc.Warehouse__c = wr.Id;
        acc.AccountNumber = 'ACT21';
        accountForInsertList.add(acc);

        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery3';
        acc2.RecordTypeId = customerRecordTypeId;
        acc2.Sales_Organization__c  = 'Feeds';
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = distributorAccount.Id;
        acc2.Warehouse__c = wr.Id;
        acc2.AccountNumber = 'ACT31';
        accountForInsertList.add(acc2);

        insert accountForInsertList;

        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
            );
        insert settings;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111222333';
        parentProductList.add(prodParent1);

        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '333444555';
        parentProductList.add(prodParent2);

        if(parentProductList.size()>0){
            System.debug('\n\n\nPARENT PRODUCT LIST : ' + parentProductList + '\n\n\n');
            insert parentProductList;
        }
        UOM__c uom = new UOM__c();
        uom.Name = 'TESTUOM';
        uom.SAP_Code__c = '11223344';
        insert uom;

        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1,
                UOM__c = uom.Id
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1,
                UOM__c = uom.Id
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
        Id distributorRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Account acct = new Account();
        acct.Name = 'Sample Grocery4';
        acct.Sales_Organization__c  = 'Feeds';
        acct.BU_Feeds__c = true;
        acct.Branded__c = true;
        acct.GFS__c = true;
        acct.BU_Poultry__c = true;
        acct.Market__c = marketList.get(0).Id;
        acct.Warehouse__c = wr.Id;
        acct.AccountNumber = 'ACT41';
        insert acct;

        Inventory__c inventoryRecord = new Inventory__c();
        inventoryRecord.Account__c = distributorAccount.Id;
        inventoryRecord.Last_Update__c = Date.today();
        inventoryRecord.Warehouse1__c = wr.Id;
        insert inventoryRecord;

        Inventory_Item__c invItem = new Inventory_Item__c();
        invItem.Inventory__c = inventoryRecord.Id;
        invItem.Product__c = parentProductList.get(0).Id;
        invItem.Quantity__c = 100;
        insert invItem;


        Inventory_Line__c invLine = new Inventory_Line__c();
        invLine.Inventory_Item__c = invItem.Id;
        invLine.UOM__c = ConversionforInsertList.get(0).Id;
        invLine.Depleted__c = false;
        insert invLine;


    }

    private static testMethod void test() {
        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id,Warehouse1__c,Account__c FROM Inventory__c WHERE Account__c = : accountId ];
        Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
            cx.saveInventories();
        Test.stopTest();
    }

    private static testMethod void test2() {
        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id,Warehouse1__c,Account__c FROM Inventory__c WHERE Account__c = : accountId ];
        Custom_Product__c prodRecord = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductGFS'];
        Test.startTest();
        Test.setCurrentPageReference(Page.InventoryBalancePage);
        System.currentPageReference().getParameters().put('xProductId', prodRecord.Id);
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.PageSize = 1;
            cx.filterByName = 'ParentProductGFS';
            cx.filterBySKU = '333444555';
            cx.filterByBU = 'GFS';

            cx.searchProduct();
            cx.nextInventoryList();
            cx.previousInventoryList();
            cx.AddLineRows();
            cx.saveInventories();
        Test.stopTest();
    }
    private static testMethod void test3() {
        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id,Warehouse1__c,Account__c FROM Inventory__c WHERE Account__c = : accountId ];
        Custom_Product__c prodRecord = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductGFS'];
        System.Test.startTest();
        Test.setCurrentPageReference(Page.InventoryBalancePage);
        System.currentPageReference().getParameters().put('xProductId', prodRecord.Id);
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.PageSize = 1;
            cx.filterByName = 'ParentProduct';
            cx.searchProduct();
            cx.nextInventoryList();
            cx.previousInventoryList();
            cx.AddLineRows();
            cx.saveInventories();
        System.Test.stopTest();
    }
    private static testMethod void test4() {
        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id,Warehouse1__c,Account__c FROM Inventory__c WHERE Account__c = : accountId ];
        Custom_Product__c prodRecord = [SELECT Id FROM Custom_Product__c WHERE Name = : 'ParentProductFeeds'];
        System.Test.startTest();
        Test.setCurrentPageReference(Page.InventoryBalancePage);
        System.currentPageReference().getParameters().put('xProductId', prodRecord.Id);
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.PageSize = 1;
            cx.filterByName = 'ParentProduct';
            cx.searchProduct();
            cx.nextInventoryList();
            cx.previousInventoryList();
            cx.removeLine();
            cx.saveInventories();
        System.Test.stopTest();
    }


    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test5() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];
        invRecord.Warehouse1__c = null;
        Inventory__c invRecord2 = invRecord.clone(false, true, false, false);
        insert invRecord2;

        System.Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
            cx.saveInventories();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('Feeds', cx.filterByBU);
    }

    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test6() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];
        Inventory__c invRecord2 = invRecord.clone(false, true, false, false);
        insert invRecord2;

        System.Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
            cx.saveInventories();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('Feeds', cx.filterByBU);
    }


    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test7() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];
        invRecord.Warehouse1__c = null;

        System.Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
            cx.saveInventories();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('Feeds', cx.filterByBU);
    }

    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test8() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];
        delete invRecord;

        System.Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('Feeds', cx.filterByBU);
    }

    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test9() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];
        invRecord.Warehouse1__c = null;
        delete invRecord;

        System.Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('Feeds', cx.filterByBU);
    }

    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test10() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];

        System.Test.startTest();
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.inventoryItemLineMap = new Map<id,InventoryBalancePageCX.InventoryWrapper>();
            cx.filterByName = 'ParentProductFeeds';
            cx.filterByBU = 'Feeds';
            cx.filterBySKU = '111222333';
            cx.searchProduct();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('Feeds', cx.filterByBU);
    }

    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void test11() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];

        System.Test.startTest();
            Test.setCurrentPageReference(Page.InventoryBalancePage);
            System.currentPageReference().getParameters().put('saveProcess', 'SaveOnly');
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.currentInventoryRec.Id = null;
            cx.saveInventories();
        System.Test.stopTest();

        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals(null, cx.filterBySKU);
        System.assertEquals(null, cx.filterByName);
        System.assertEquals(null, cx.filterByBU);
    }

    // Changes on 08 May 2019 for enhancement of test coverage.
    public static testMethod void searchProductTest1() {

        Id accountId = [SELECT Id FROM Account WHERE Name = 'TestDistributor'].Id;
        Inventory__c invRecord = [SELECT Id, Warehouse1__c, Account__c FROM Inventory__c WHERE Account__c = :accountId];
        Inventory_Line__c invLine = [SELECT Id,Inventory_Item__r.Product__c,Inventory_Item__r.Product__r.Name,So_No__c,Inventory_Item__r.Inventory__r.Account__c FROM Inventory_Line__c];
        invLine.So_No__c = 'Test So_No';
        update invLine;


        System.Test.startTest();
            Test.setCurrentPageReference(Page.InventoryBalancePage);
            System.currentPageReference().getParameters().put('saveProcess', 'SaveOnly');
            ApexPages.StandardController con = new ApexPages.StandardController(invRecord);
            InventoryBalancePageCX cx = new InventoryBalancePageCX(con);
            cx.filterBYSONumber = 'Test So_No';
            cx.filterByName = 'ParentProductFeeds';
            cx.filterBySKU = '111222333';
            cx.filterByBrand = 'Test Brand';
            cx.filterByCategory = invLine.Id;
            cx.filterByBU = 'GFS';
            cx.searchProduct();
        System.Test.stopTest();


        System.assertNotEquals(null, cx.currentInventoryRec);
        System.assertEquals('SaveOnly', cx.saveOnlyTxt);
        System.assertEquals('SaveAndNew', cx.saveAndNewTxt);
        System.assertEquals('111222333', cx.filterBySKU);
        System.assertEquals('ParentProductFeeds', cx.filterByName);
        System.assertEquals('GFS', cx.filterByBU);
    }

}