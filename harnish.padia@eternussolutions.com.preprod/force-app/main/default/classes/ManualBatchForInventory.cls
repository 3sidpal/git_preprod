global class ManualBatchForInventory
{    
    webservice static void callSnapshotBatchJob() // you can pass parameters
    { 
         Database.executeBatch(new batchInventorySnapshot (), 1000);
    }
    
    webservice static void callDepletedBatchJob() // you can pass parameters
    { 
         Database.executeBatch(new  batchDepleteInventoryLineItem(), 1000);
    }
    
    webservice static void callBatchJob() // you can pass parameters
    { 
         Database.executeBatch(new  batchUpsateInventoryLineItem(), 1000);
    }
    
}