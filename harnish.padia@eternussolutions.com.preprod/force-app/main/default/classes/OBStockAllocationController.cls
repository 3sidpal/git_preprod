public class OBStockAllocationController {
    
    
    public Map<Id,Decimal> mapStockAllocationIdToAmountLeft = new Map<Id,Decimal>();
    public List<Account_Product_Allocation__c> listAccountProductAllocation =
        new List<Account_Product_Allocation__c>();
    public Map<String, Integer> mapServingPlantCodeToNumberOfAccounts = new Map<String, Integer>();
    public Map<Id,Stock_Allocation__c> mapStockAllocationsToBeUpdated =
        new Map<Id, Stock_Allocation__c>();
    public Map<String,Decimal> mapConversionValues;
    public List<Order_Item__c> listOrderItemsToBeUpdated = new List<Order_Item__c>();
    public List<Order__c> listOrdersToBeUpdated = new List<Order__c>();
    public List<Account_Product_Allocation__c> listAccountProductAllocationToBeCreated =
        new List<Account_Product_Allocation__c>();
    public Map<Id,Stock_Allocation__c> mapProductIdToMapStockAllocation;
    
    
    /**
     * method to create Product Id to map of Stock Allocations map
     */
    public Map<Id,Stock_Allocation__c> createProductToStockAllocationsMap(List<Order_Item__c> listOrderItems,
        Map<String,String> mapPlantCodes, Account accInst) {
            
        Set<String> setMaterialCode = new Set<String>();
        Set<String> setServingPlantCode = new Set<String>();
        Map<String, Id> mapMaterialCodeToProductId = new Map<String, Id>();
        Map<Id,Stock_Allocation__c> mapProductIdToMapStockAllocation = new Map<Id,Stock_Allocation__c>();
        
        String combinationKey;
        for(Order_Item__c objOrderItem: listOrderItems) {
            combinationKey = '';
            if(objOrderItem.Product__c != null && String.isNotBlank(objOrderItem.Product__r.Loading_Group__c) &&
                accInst != null && String.isNotBlank(accInst.Shipping_Condition__c)) {
                    combinationKey = (objOrderItem.Product__r.Loading_Group__c + '_' +
                        accInst.Shipping_Condition__c).toLowerCase();
            }
            if(objOrderItem.Product__r.SKU_Code__c != null) {
                setMaterialCode.add(objOrderItem.Product__r.SKU_Code__c);
            }
            if(mapPlantCodes.get(combinationKey) != null) {
                setServingPlantCode.add(mapPlantCodes.get(combinationKey));
            }
            if(objOrderItem.Product__c != null && objOrderItem.Product__r.SKU_Code__c != null) {
                mapMaterialCodeToProductId.put(objOrderItem.Product__r.SKU_Code__c, objOrderItem.Product__c);
            }
        }

        if(setMaterialCode != null && setServingPlantCode != null) {

             List<Stock_Allocation__c> stockAllocation =[Select Id, Material_Code__c,Product__c,
                                                             Quantity__c, Quantity_Allocated__c, Quantity_Left__c,
                                                              Cap_Size__c, Serving_Plant_Code__c,
                                                             Start_Date__c, End_Date__c,
                                                             Re_Allocation_Limit__c, Percentage_for_Re_Allocation__c
                                                        From Stock_Allocation__c
                                                        Where Material_Code__c IN: setMaterialCode
                                                        AND Serving_Plant_Code__c IN: setServingPlantCode
                                                        ORDER BY CreatedDate ASC];
            for(Stock_Allocation__c stockAllocationObj : stockAllocation) {
                mapProductIdToMapStockAllocation.put(stockAllocationObj.Product__c, stockAllocationObj);
                mapStockAllocationIdToAmountLeft.put(stockAllocationObj.Id, stockAllocationObj.Quantity_Left__c);
            }
            
        } // end of if
        return mapProductIdToMapStockAllocation;
    }
    
    /**
     * method to populate Account Product allocation map
     */
    public void populateAccountProductAllocation(List<Order_Item__c> listNewOrderItems, Map<String,String> mapPlantCodes,
        Account accInst) {
        Set<Id> setProductId = new Set<Id>();
        for(Order_Item__c objOrderItem: listNewOrderItems) {
            setProductId.add(objOrderItem.Product__c);
        }
        listAccountProductAllocation = [
            Select Id, Account__c, Allocated_Quantity__c,
                Allocation_Date__c, Custom_Product__c,
                Product_Type__c, Stock_Allocation__c, Key__c
            From Account_Product_Allocation__c
            Where Custom_Product__c IN: setProductId AND
                  Account__c =: accInst.Id
        ];
    }
    
    
    /**
     * method to fetch Plant Codes
     */
    public static void fetchPlantCodes(List<Order_Item__c>listOrderItems, Map<String,String> mapPlantCodes,
        Account accInst) {
        system.debug('====orderItem=='+Json.serializePretty(listOrderItems[0]));
        String combinationKey;
        for(Order_Item__c orderItem : listOrderItems) {
             combinationKey = '';
            if(orderItem.Product__c != null && String.isNotBlank(orderItem.Product__r.Loading_Group__c) &&
                accInst != null && String.isNotBlank(accInst.Shipping_Condition__c)) {
                    combinationKey = orderItem.Product__r.Loading_Group__c + '_' +
                        accInst.Shipping_Condition__c;
                    system.debug('====combinationKey==='+combinationKey);
                    if(!mapPlantCodes.containsKey(combinationKey)) {
                        mapPlantCodes.put(combinationKey.toLowerCase(), null);
                    }
                }
        }
        if(mapPlantCodes.isEmpty() == false) {
            for(Stock_Allocation_Plant_Determination__c plantDetermination : [SELECT Combination_Key__c,
                                                                    Plant__c
                                                             FROM Stock_Allocation_Plant_Determination__c
                                                             WHERE Combination_Key__c IN :mapPlantCodes.keySet() AND
                                                                   Combination_Key__c != Null AND
                                                                   Plant__c != null]) {
                if(mapPlantCodes.get(plantDetermination.Combination_Key__c.toLowerCase()) == null) {
                    mapPlantCodes.put(plantDetermination.Combination_Key__c.toLowerCase(),plantDetermination.Plant__c);
                }
            }
        }
        system.debug('====mapPlantCodes=='+mapPlantCodes);
    }
    
    
    public Boolean conversionAndAvailability(Conversion__c inventoryConversion, Conversion__c orderItemConversion,
        Decimal inventoryQuantity, Decimal orderQuantity) {
        system.debug('===conversionAndAvailability===');
        Decimal inventoryQuantityInKg = inventoryQuantity * inventoryConversion.KG_Conversion_Rate__c;
        system.debug('====orderQuantity==='+orderQuantity);
        system.debug('====orderItemConversion==='+orderItemConversion);
        if(orderQuantity == null) {
            orderQuantity = 0;
        }
        Decimal orderQuantityInKg = orderQuantity * orderItemConversion.KG_Conversion_Rate__c;
        if(orderQuantityInKg <= inventoryQuantityInKg) {
            return true;
        }
        return false;
    }
    
    
    /**
     * method to process Order Items
     */
    public void processOrderItems(
        List<Order_Item__c> listNewOrderItems,
        Map<String,String> validationErrors, 
        Account accInst,
        Map<String,Map<String,Conversion__c>> mapConversion
    ) {
        
        //collected the plant codes
        Map<String,String> mapPlantCodes = new Map<String,String>();
        String combinationKey;
        for(Order_Item__c objOrderItem: listNewOrderItems) {
            combinationKey = '';
            if(objOrderItem.Product__c != null && String.isNotBlank(objOrderItem.Product__r.Loading_Group__c) &&
                accInst != null && String.isNotBlank(accInst.Shipping_Condition__c)) {
                    combinationKey = (objOrderItem.Product__r.Loading_Group__c + '_' +
                        accInst.Shipping_Condition__c).toLowerCase();
            }

            if(String.isNotBlank(objOrderItem.Order_Form__r.Order_Serving_Plant__c)) {
                /*objOrderItem.Order_Form__r.Account__r.Default_Serving_Plant__c =
                    objOrderItem.Order_Form__r.Order_Serving_Plant__c;*/
                    mapPlantCodes.put(combinationKey, objOrderItem.Order_Form__r.Order_Serving_Plant__c);
            }

            if(objOrderItem.Order_Quantity_Remaining__c != null &&
                objOrderItem.Order_Quantity_Remaining__c > 0
            ) {
                objOrderItem.Order_Quantity__c = objOrderItem.Order_Quantity_Remaining__c;
            }
            else if(objOrderItem.Product__c != null && objOrderItem.Product__r.Catch_Weight__c == true) {
                objOrderItem.Order_Quantity__c = objOrderItem.Weight_Quantity__c;
            }
        }
        system.debug('=====mapPlantCodes b4====='+Json.serializePretty(mapPlantCodes));
        fetchPlantCodes(listNewOrderItems, mapPlantCodes, accInst);
        system.debug('=====mapPlantCodes after====='+Json.serializePretty(mapPlantCodes));
        
        mapProductIdToMapStockAllocation =
            createProductToStockAllocationsMap(listNewOrderItems, mapPlantCodes, accInst);
        system.debug('=====mapProductIdToMapStockAllocation====='+Json.serializePretty(mapProductIdToMapStockAllocation));
        
        populateAccountProductAllocation(listNewOrderItems, mapPlantCodes, accInst);
        system.debug('======listAccountProductAllocation==='+listAccountProductAllocation);
        
        
        placeOrder(listNewOrderItems, mapProductIdToMapStockAllocation, mapPlantCodes, validationErrors, accInst, mapConversion);
    }
    /************************************************************************************************/
    
    /**
     * method to verify Stock availability and place order
     */
    public void placeOrder(
        List<Order_Item__c> listOrderItems,
        Map<Id,Stock_Allocation__c> mapProductIdToMapStockAllocation,
        Map<String,String> mapPlantCodes,
        Map<String,String> validationErrors,
        Account accInst,
        Map<String,Map<String,Conversion__c>> mapConversion
    ) {
        system.debug('=======mapConversion==='+Json.serializePretty(mapConversion));
        for(Order_Item__c objOrderItem: listOrderItems) {
            Decimal requiredQty = objOrderItem.Order_Quantity__c;
            system.debug('=====objOrderItem====='+objOrderItem);
            if(mapProductIdToMapStockAllocation.containsKey(objOrderItem.Product__c) ) {
                Stock_Allocation__c stockAllocation = mapProductIdToMapStockAllocation.get(objOrderItem.Product__c);
                
                if ((stockAllocation.Start_Date__c == null &&
                        stockAllocation.End_Date__c == null) ||
                        (stockAllocation.Start_Date__c != null &&
                        stockAllocation.End_Date__c != null &&
                        Date.today() >= stockAllocation.Start_Date__c &&
                        Date.today() <=  stockAllocation.End_Date__c)
                    ) {
                        //checkStockAvailability(objOrderItem, 'Primary', stockAllocation, mapPlantCodes, validationErrors, accInst);
                        validate(objOrderItem, stockAllocation, mapPlantCodes, validationErrors, accInst, mapConversion.get(objOrderItem.Product__c));
                    }
            }
            else {
                validationErrors.put(objOrderItem.Product__c, 'No Stocks available');
            }
        } // end of inner for
    }
    
    public void validate(Order_Item__c objOrderItem,
        Stock_Allocation__c objStockAllocation,
        Map<String,String> mapPlantCodes,
        Map<String,String> validationErrors,
        Account accInst,
        Map<String,Conversion__c> mapConversion) {
            System.debug('===validate==');
            String errorMessage = '';
            system.debug('====mapConversion======'+Json.serializePretty(mapConversion));
            if(mapConversion.get('StockUOM') != null && mapConversion.get('OrderItemUOM') != null ) {
               Boolean isAvailable = conversionAndAvailability(mapConversion.get('StockUOM'), mapConversion.get('OrderItemUOM'),
                    objStockAllocation.Quantity_Left__c, objOrderItem.Order_Quantity__c);
                   system.debug('===isAvailable====='+isAvailable);
               if(!isAvailable) {
                   errorMessage += 'Insufficient available quantity.';
               }
            }
            else {
                errorMessage += 'Invalid UOM';
            }
            
            if(errorMessage != 'Invalid UOM') {
                
                if(objStockAllocation.Cap_Size__c != null && (mapConversion.get('OrderItemUOM').KG_Conversion_Rate__c * objOrderItem.Order_Quantity__c) > (mapConversion.get('StockUOM').KG_Conversion_Rate__c * objStockAllocation.Cap_Size__c)) {
                    errorMessage += '\n Cap limit exceeded.';
                }
                
                Decimal intTotalQuantityAlloacted = 0;
                Decimal intTotalOverallConsumed = 0;
                Decimal intPoolQuantity = 0;
                Integer intCurrentMonth = 1;
                
                if (accInst.Allocation_Limit__c != null) {
                    if(objStockAllocation.Start_Date__c != null) {
                        Integer i = 1;
                        while(true) {
                            intCurrentMonth = i;
                            if(Date.today() < objStockAllocation.Start_Date__c.addMonths(i)) {
                                break;
                            }
                            i++;
                        }
                    }
                    system.debug('==intCurrentMonth===='+intCurrentMonth);
                    for(Account_Product_Allocation__c objAccountProductAlloaction: listAccountProductAllocation) {
                        if(objOrderItem.Product__c == objAccountProductAlloaction.Custom_Product__c &&
                            objAccountProductAlloaction.Product_Type__c == 'Primary' &&
                            objStockAllocation.Start_Date__c != null &&
                            objStockAllocation.End_Date__c != null &&
                            objAccountProductAlloaction.Allocation_Date__c >= objStockAllocation.Start_Date__c
                        ) {
                            if (accInst.Id == objAccountProductAlloaction.Account__c &&
                                objAccountProductAlloaction.Allocation_Date__c <= objStockAllocation.End_Date__c
                            ) {
                                intTotalQuantityAlloacted += objAccountProductAlloaction.Allocated_Quantity__c;
                            }
                        }
                    }
                   system.debug('=======intTotalQuantityAlloacted=>'+intTotalQuantityAlloacted+'||||intTotalOverallConsumed=>'+intTotalOverallConsumed);
                    intTotalQuantityAlloacted = intTotalQuantityAlloacted * mapConversion.get('StockUOM').KG_Conversion_Rate__c;
                    Decimal  limitRemaining = ((accInst.Allocation_Limit__c * mapConversion.get('StockUOM').KG_Conversion_Rate__c) * intCurrentMonth) - intTotalQuantityAlloacted;
                    intPoolQuantity = (limitRemaining *
                        ((accInst.Percentage_for_Re_Allocation__c != null ? accInst.Percentage_for_Re_Allocation__c : 100) /100));
                   if(objOrderItem.Order_Quantity__c > intPoolQuantity) {
                       errorMessage += '\n Allocation limit exausted.';
                   }
                }
            }
            System.debug(objOrderItem.Product__r.Name+'===errorMessage=='+errorMessage);
            if(String.isBlank(errorMessage)) {
                errorMessage = 'Available';
            }
            validationErrors.put(objOrderItem.Product__c, errorMessage);
    }
}