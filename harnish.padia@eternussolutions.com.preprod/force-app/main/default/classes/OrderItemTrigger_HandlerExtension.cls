/**
    * AUTHOR        : EUGENEBASIANOMUTYA
    * DESCRIPTION   : HANDLES OTHER FUNCTIONS AND METHODS OF ORDER AND ORDER ITEMS
    * HISTORY       : 
                    : MAY.06.2017 - CREATED.
                    : Update: 06.08.2017
                    : John Michael Mahaguay
                    : UpdateOrderFileCounter_FromLineItem - change parameter from Order List to Order Item List
                    : Add Total_Order_Net_Amount__c, Total_Invoice_Net_Amount__c for updating whenever order line item was updated
**/


public with sharing class OrderItemTrigger_HandlerExtension {

    //Update the File Counter everytime the Order is Updated
    public static void UpdateOrderFileCounter_FromLineItem(List<Order_Item__c> newOrderItemList){
    
        if(!TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE){
            list<Order__c> lOrder = new list<Order__c>();
            //
            Set<Id> orderId = new Set<Id>();
            Decimal orderNetAmnt=0.00, invoiceNetAmnt=0.00;
            for(Order_Item__c oi: newOrderItemList){
                orderId.add(oi.Order_Form__c);
            }
            for(Order__c ord : [SELECT File_Counter__c, Total_Order_Net_Amount__c, Total_Invoice_Net_Amount__c FROM Order__c WHERE Id IN: orderId]){
                orderNetAmnt = 0.00;
                invoiceNetAmnt = 0.00;
                if(ord.File_Counter__c == NULL){
                    ord.File_Counter__c = 1;
                }else{
                    ord.File_Counter__c = ord.File_Counter__c + 1;
                }
                // for(Order_Item__c oi : newOrderItemList){
                //     if(oi.Order_form__c == ord.Id){
                //         orderNetAmnt += oi.Order_Net_Total__c == null ? 0 : oi.Order_Net_Total__c;
                //         invoiceNetAmnt += oi.Invoice_Net_Amount__c == null ? 0 : oi.Invoice_Net_Amount__c;
                //     }
                // }
                // ord.Total_Order_Net_Amount__c = orderNetAmnt;
                // ord.Total_Invoice_Net_Amount__c = invoiceNetAmnt;

                lOrder.add(ord);
            }
            if(lOrder.size() >0){
                TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE = true;
                update lOrder;
            }
        }
    }
    
    //Update the File Counter everytime the Order is Updated
    public static void UpdateOrderFileCounter(List<Order__c> newOrderList){
        if(!TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE){
            for(Order__c ord : newOrderList){
                if(ord.File_Counter__c == NULL){
                    ord.File_Counter__c = 1;
                }else{
                    ord.File_Counter__c = ord.File_Counter__c + 1;
                }
            }
            
            
        }
    }
   
   //public static boolean ExcempProfile(){
    //   boolean bExcempt=false;
        /*       
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName=='Dataloader'){
            bExcempt= true;
        }
        */
        
        // 00e28000000NkquAAC - Dataloader
        // 00e280000017sDtAAI - System Admin
//         if('00e28000000NkquAAC'==userinfo.getProfileId() || '00e280000017sDtAAI'==userinfo.getProfileId()){
//             bExcempt= true;
//         }
        
//         return bExcempt;
        
//   }

}