@isTest
private class OrderReturnPageController_Test {

    @isTest static void constCallTestMethod(){
        Order__c orderObj = new Order__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(orderObj);
        OrderReturnPageController testObj = new OrderReturnPageController(sc);
        System.assertEquals(testObj.draftOrder, FALSE);
    }

}