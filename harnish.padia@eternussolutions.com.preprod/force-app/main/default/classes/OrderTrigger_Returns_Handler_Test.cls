@isTest
public with sharing class OrderTrigger_Returns_Handler_Test {
      @testSetup static void setupdata(){
        
        // Create New BU_Mapping
        BU_Mapping__c buMappingObj = new BU_Mapping__c(Name= 'GFS', Master_BU__c= '');
        insert buMappingObj;
        BU_Mapping__c buMappingObj1 = new BU_Mapping__c(Name= 'branded', Master_BU__c= '');
        insert buMappingObj1;

        // Inserting Custom Setting Data (Sales_Org_Division__c)
        List<Sales_Org_Division__c> salesOrgDivisionList = new List<Sales_Org_Division__c>();
        salesOrgDivisionList.add(new Sales_Org_Division__c(Name = 'Poultry', Division__c = '20', Code__c = '11A0',Business_Unit__c = 'POULTRY'));
        salesOrgDivisionList.add(new Sales_Org_Division__c(Name = 'MEAT', Division__c = '20', Code__c = '11B0',Business_Unit__c = 'MEATS'));
        salesOrgDivisionList.add(new Sales_Org_Division__c(Name = 'GFS', Division__c = '10', Code__c = '11D0',Business_Unit__c = 'GFS'));
        insert salesOrgDivisionList;

        
        // Create New RDDCutOff
        List<RDDCutOff__c> rddCutOffList = new List<RDDCutOff__c>();
        String profileName = [SELECT Name FROM Profile WHERE Id =: UserInfo.getProfileId()].Name;
        rddCutOffList.add(new RDDCutOff__c(Name= 'RDD CutOff 1', Business_Unit__c = 'gfs', Profile__c= profileName, Time__c = '18:00',Days__c = 2,Cutoff_Day__c = 1));
        rddCutOffList.add(new RDDCutOff__c(Name= 'RDD CutOff 2',Business_Unit__c='', Time__c = '18:00',Days__c = 2,Cutoff_Day__c = 1));
        // rddCutOffList.add(new RDDCutOff__c(Name= 'RDD CutOff 3',Business_Unit__c = '', Time__c = '18:00',Days__c = 2,Cutoff_Day__c = 1));
        insert rddCutOffList;

        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'National List Price',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            SIF_AllowEditAndDelete__c = true,
            Check_PO_Number_Dup__c = true
            );
        insert settings;
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id distributorCustomerId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = 'Sample Grocery1 AVS';
        acc.Sales_Organization__c  = 'Branded';
        acc.General_Trade__c  = true;
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.AccountNumber = '123412344';
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        acc.Area__c = 'VISAYAS';
        insert acc;

        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery2 AVS';
        acc2.Sales_Organization__c  = 'Branded';
        acc2.General_Trade__c  = true;
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.AccountNumber = '1234123';
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = acc.Id;
        acc2.RecordTypeId = distributorCustomerId;
        acc2.Area__c = 'VISAYAS';
        insert acc2;

        Warehouse__c wr = new Warehouse__c();
        wr.Account__c = acc.Id;
        wr.Warehouse_No__c = acc2.Id;
        insert wr;
        acc2.Warehouse__c = wr.Id; 
        update acc2;       
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();

        //CREATE SAS- Contact
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact SASvar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast', AccountId = acc.Id);
        insert SASvar;

        //AARP: Create New DSP
        Id dspRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact dspVar = new Contact (RecordTypeId=dspRecordTypeId, FirstName = 'xyzSecond1', LastName = 'XyZSecond1', AccountId = acc.Id);
        insert dspVar;

        List<Order__c> orderList = new List<Order__c>();
        Order__c orderRecord = new Order__c(
            Account__c = acc2.Id,
            RecordTypeId = actualSalesRecordTypeId,
            Invoice_Date__c = System.today().addDays(-3),
            Invoice_No__c = '1111111',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id,
            Sales_Org__c = '11D0-GFS'
        );
        orderList.add(orderRecord);

        // Insert Purchased Order Record
        Id purchaseOrderRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        Order__c purchaseOrderRecord = new Order__c(
            Account__c = acc2.Id,
            RecordTypeId = purchaseOrderRecordTypeId,
            PO_No__c = 'Test PO',
            Invoice_Date__c = System.today().addDays(-3),
            Invoice_No__c = '1111111',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id,
            Sales_Org__c = '11D0-GFS',
            requested_Delivery_Date__c = System.Today().addDays(3),
            Product_Category_Name__c = 'REFRIGERATED MEATS'
        );
        orderList.add(purchaseOrderRecord);

        // Insert Return Order Form Record
        Id returnRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId();
        Order__c returnOrderRecord = new Order__c(
            Account__c = acc2.Id,
            RecordTypeId = returnRecordTypeId,
            RTV_No__c = 'Test RTV',
            Invoice_Date__c = System.today().addDays(-3),
            Invoice_No__c = '1111111',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id,
            Sales_Org__c = '11D0-GFS',
            Sales_Order_Number__c = 'TestSO'
        );
        orderList.add(returnOrderRecord);
        insert orderList;
        Test.setCreatedDate(orderRecord.Id,System.today().addDays(-3));
    }

     /* Added this functionality to enhance the code coverage of class OrderTrigger_Returns_Handler */
    private static testMethod void updateSIFViaInvoiceTest() {
        Id returnOrderFormRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId();
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        Account acc2 = [SELECT Id FROM Account WHERE Name = 'Sample Grocery1 AVS'];
        Test.startTest();
        //Create New DSP
        Id dspRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact dspVar = new Contact (RecordTypeId = dspRecordTypeId, FirstName = 'ABC', LastName = 'DEF',AccountId = acc2.Id);
        insert dspVar;

        //Create New SAS
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact sasVar = new Contact(RecordTypeId = SASrecordTypeId, FirstName = 'PQR', LastName = 'STU',AccountId = acc2.Id);
        insert sasVar;

        List<Order__c> orderRecordList = new List<Order__c>();
        Order__c orderRecordObject = new Order__c();
        orderRecordObject.Account__c = acc2.Id;
        orderRecordObject.RecordTypeId = returnOrderFormRecordTypeId;
        orderRecordObject.Invoice_Date__c = System.today().addDays(-3);
        orderRecordObject.Invoice_No__c = 'INV0111';
        orderRecordObject.SAS__c = sasVar.Id;
        orderRecordObject.DSP__c = dspVar.Id;
        orderRecordList.add(orderRecordObject);

        orderRecordObject.Account__c = acc2.Id;
        orderRecordObject.RecordTypeId = actualSalesRecordTypeId;
        orderRecordObject.Invoice_Date__c = System.today().addDays(-3);
        orderRecordObject.Invoice_No__c = 'INV0112';
        orderRecordObject.SAS__c = sasVar.Id;
        orderRecordObject.DSP__c = dspVar.Id;
        orderRecordList.add(orderRecordObject);
        insert orderRecordObject;

        Test.stopTest();
        Order__c orderObj = new Order__c();
        orderObj = [SELECT ID, Sales_Information_Form__c FROM Order__c WHERE ID = :orderRecordObject.Id];
        System.assertEquals(NULL,orderObj.Sales_Information_Form__c);
    }
}