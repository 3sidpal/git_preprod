@isTest
public with sharing class OrderUtils_Test {
    @testSetup static void setupdata(){

        TestDataFactory tdf = new TestDataFactory();
        List<Distribution_Channel__c> distributionChannelList = TestDataFactory.createDistributionChannels();
        List<Custom_Product__c> prodList = tdf.createTestProducts(2);

        //  Record Type Ids
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id distributorCustomerId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Id sasReportTypeId = Schema.sObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Id orderRectType = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        Id distRectType = Schema.sObjectType.Pricing_Condition__c.getRecordTypeInfosByName().get('Discount').getRecordTypeId();

        //  Insert Markets
        List<Market__c> marketList = new List<Market__c>();
        marketList.add(new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
        ));
        marketList.add(new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
        ));
        marketList.add(new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
        ));
        insert marketList;

        // Insert Accounts
        Account acc = new Account(
            Name = 'Sample Grocery1 AVS',
            Sales_Organization__c  = 'Branded',
            General_Trade__c  = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            AccountNumber = '123412344',
            Market__c = marketList.get(0).Id,
            RecordTypeId = distributorAccountId,
            Area__c = 'VISAYAS'
        );
        insert acc;

        Account acc2 = new Account(
            Name = 'Sample Grocery2 AVS',
            Sales_Organization__c  = 'Branded',
            General_Trade__c  = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            AccountNumber = '1234123',
            Market__c = marketList.get(0).Id,
            Distributor__c = acc.Id,
            RecordTypeId = distributorAccountId,
            Area__c = 'VISAYAS',
            Shipping_Condition__c = 'Test'
        );
        insert acc2;

        // Insert Contact
        Contact contactObj = new Contact (
            FirstName='Sample-FirstName',
            LastName='Sample-LastName',
            AccountId=acc2.Id,
            RecordTypeId=sasReportTypeId, 
            Active__c=true
        );
        insert contactObj;

        // Insert Ship_Sold_To__c
        Ship_Sold_To__c shipSoldToObj = new Ship_Sold_To__c (
            Name='Ship Sold Record - ',
            AccountChild__c=acc.Id,
            AccountMaster__c=acc2.Id,
            Ship_Sold_To__c='SH',
            Dataloader_ID__c='1111290520180705'
        );
        insert shipSoldToObj;

        // Insert Category
        Category__c newCat = new Category__c(
            Name = 'ANIMAL',
            Business_Unit__c = 'Feeds',
            SAP_Code__c = 'ANIMAL'
        );
        insert newCat;

        //INSERT SALES ORG DIVISION
        List<Sales_Org_Division__c> salesOrgDivision = new List<Sales_Org_Division__c>();
        salesOrgDivision.add(new Sales_Org_Division__c(
            Name = '14F0-MAGNOLIA-10',
            Business_Unit__c = 'MAGNOLIA',
            Code__c = '14F0',
            Description__c = 'Branded',
            Division__c = '10'
        ));
        salesOrgDivision.add(new Sales_Org_Division__c(
            Name = '11B0-MEATS-20',
            Business_Unit__c = 'MEATS',
            Code__c = '11B0',
            Description__c = 'Agro/Commodity',
            Division__c = '20'
        ));
        salesOrgDivision.add(new Sales_Org_Division__c(
            Name = '11D0-GFS-10',
            Business_Unit__c = 'GFS',
            Code__c = '11D0',
            Division__c = '10'
        ));
        salesOrgDivision.add(new Sales_Org_Division__c(
            Name = '11D0-GFS-20',
            Business_Unit__c = 'GFS',
            Code__c = '11D0',
            Division__c = '20'
        ));  
        salesOrgDivision.add(new Sales_Org_Division__c(
            Name = '11C0-FEEDS-20',
            Business_Unit__c = 'FEEDS',
            Code__c = '11C0',
            Description__c = 'Agro/Commodity',
            Division__c = '20'
        ));
        salesOrgDivision.add(new Sales_Org_Division__c(
            Name = '11A0-POULTRY-20',
            Business_Unit__c = 'POULTRY',
            Code__c = '11A0',
            Description__c = 'Agro/Commodity',
            Division__c = '20'
        ));
        insert salesOrgDivision;

        //  Insert Order
        
        Order__c ord = new Order__c(
            PO_No__c = '000111',
            RecordTypeId = orderRectType,
            Account__c = acc2.Id,
            SAS__c = contactObj.Id,
            Ship_Sold_To__c = shipSoldToObj.Id,
            Product_Category__c = newCat.id,
            Sales_Org__c = '11D0-GFS',
            Order_Type__c = 'ZOR'
        );
        insert ord;

        
        
        //  Insert Pricing Condition
        Pricing_Condition__c prcCond = new Pricing_Condition__c(
            Name = 'Test Pricing Condition',
            RecordTypeId = distRectType
        );
        insert prcCond;

        //  Insert Loading Group
        Loading_Group__c loadGroupA = new Loading_Group__c(
            Name = 'A',
            Grouping__c = 'A'
        );
        insert loadGroupA;

        prodList[0].Loading_Group__c = loadGroupA.Id;
        update prodList[0];

        //  Insert Plant Determination
        Plant_Determination__c objPlantDetermination = new Plant_Determination__c(
            Account__c = acc2.Id,
            CustomProduct__c = prodList[0].Id,
            Product_Status__c = 'Available',
            Sales_Org__c = '11D0',
            Order_Type__c = 'ZOR',
            Shipping_Condition__c = 'Test',
            Loading_Group__c = loadGroupA.Id
        );
        insert objPlantDetermination;

        //  Insert Pricing Condition Data
        Pricing_Condition_Data__c pCondData = new Pricing_Condition_Data__c(
            Account__c = acc2.Id,
            Selling_Account__c = acc.Id,
            Pricing_Condition__c = prcCond.Id,
            Product__c = prodList[0].Id,
            Active__c = True,
            Effective_Date__c = System.Today() + 1,
            Price__c = 100
        );
        insert pCondData;
    }

    public static testMethod void testDistributionChannels() {
        Test.startTest();
        Account accRecord = [SELECT Id 
                               FROM Account 
                              WHERE Name = 'Sample Grocery2 AVS'];
        List<SelectOption> selectOptionList = OrderUtils.getDistributionChannels(accRecord.Id);
        Test.stopTest();
        System.assert(selectOptionList != null);
    }

    public static testMethod void testSalesOrgs() {

        Map<String, List<String>> categoryBusinessUnitMap = new Map<String, List<String>>();
        Category__c categoryRec = [SELECT Id 
                                     FROM Category__c 
                                    WHERE Name = 'ANIMAL'];
        List<String> businessUnitsList = new List<String>();
        for(Sales_Org_Division__c salesOrgObj : [SELECT Id, 
                                                        Business_Unit__c 
                                                   FROM Sales_Org_Division__c]) {
            businessUnitsList.add(salesOrgObj.Business_Unit__c);
        }
        categoryBusinessUnitMap.put(categoryRec.Id,businessUnitsList);
        Test.startTest();
        Order__c orderRec = [SELECT Id, 
                                    Product_Category__c 
                               FROM Order__c 
                              WHERE PO_No__c = '000111'];
        List<SelectOption> selectOptionList = OrderUtils.getSalesOrgs(orderRec, categoryBusinessUnitMap);
        Test.stopTest();
        System.assert(selectOptionList != null);
    }

    public static testMethod void testProductCatBasedFromBU() {
        Sales_Org_Division__c salesOrgObj = [SELECT Id, 
                                                    Business_Unit__c 
                                               FROM Sales_Org_Division__c 
                                              WHERE Business_Unit__c = 'POULTRY'];
        Test.startTest();
        List<Category__c> categoryList = OrderUtils.getProductCatBasedFromBU('POULTRY');
        Test.stopTest();
        System.assert(categoryList != null);
    }

    public static testMethod void testNationalPricing() {
        Account accRecord = [SELECT Id 
                               FROM Account 
                              WHERE Name = 'Sample Grocery2 AVS'];
        List<String> prodIdList = new List<String>();
        for(Pricing_Condition__c prodObj : [SELECT Id, 
                                                   Name FROM 
                                                   Pricing_Condition__c 
                                             WHERE Name = 'Test Pricing Condition']) {
            prodIdList.add(prodObj.Id);
        }
        Test.startTest();
        Map<String, Double> nationalPricingMap = OrderUtils.getNationalPricing(accRecord.Id, prodIdList);
        Test.stopTest();
        System.assert(nationalPricingMap != null);
    }

    public static testMethod void testProductPrices() {
        Account accRecord = [SELECT Id 
                               FROM Account 
                              WHERE Name = 'Sample Grocery2 AVS'];
        Set<Id> prodIdSet = new Set<Id>();
        for(Pricing_Condition__c prodObj : [SELECT Id, 
                                                   Name 
                                              FROM Pricing_Condition__c 
                                             WHERE Name = 'Test Pricing Condition']) {
            prodIdSet.add(prodObj.Id);
        }
        Test.startTest();
        Map<String, Map<Id, Double>> productPricesMap = OrderUtils.getProductPrices(accRecord.Id, prodIdSet);
        Test.stopTest();
        System.assert(productPricesMap != null);
    }

    public static testMethod void testDeterminationIdentifier() {
        Order__c orderRec = [SELECT Id, 
                                    Account__c, 
                                    Sales_Org__c, 
                                    Distribtution_Channel__c 
                               FROM Order__c 
                              WHERE PO_No__c = '000111' ];
        List<Custom_Product__c> customProductList = new List<Custom_Product__c>();
        Map<Order__c,List<Custom_Product__c>> orderProductMap = new Map<Order__c,List<Custom_Product__c>>();
        for(Custom_Product__c customProdObj : [SELECT Id, 
                                                      Name 
                                                 FROM Custom_Product__c]) {
            customProductList.add(customProdObj);
        }
        orderProductMap.put(orderRec,customProductList);
        Test.startTest();
        Map<Id,Custom_Product__c> plantdeterminationMap = OrderUtils.plantDeterminationIdentifier(orderProductMap);
        Test.stopTest();
        System.assert(plantdeterminationMap != null);
    }

    public static testMethod void testreturnPlantDetermination() {
        Order__c orderRec = [SELECT Id, 
                                    Account__c, 
                                    Sales_Org__c, 
                                    Distribtution_Channel__c, 
                                    Order_Type__c 
                               FROM Order__c 
                              WHERE PO_No__c = '000111' ];
        List<Custom_Product__c> customProductList = new List<Custom_Product__c>();
        Map<Order__c,List<Custom_Product__c>> orderProductMap = new Map<Order__c,List<Custom_Product__c>>();
        for(Custom_Product__c customProdObj : [SELECT Id, 
                                                      Name, 
                                                      Loading_Group__c 
                                                 FROM Custom_Product__c]) {
            customProductList.add(customProdObj);
        }
        orderProductMap.put(orderRec,customProductList);
        Test.startTest();
        List<Plant_Determination__c> platDeterminationList = OrderUtils.returnPlantDetermination(orderProductMap);
        Test.stopTest();
        System.assert(platDeterminationList == null);
    }

    public static testMethod void testOrderType() {
        Order__c orderRec = [SELECT Id, 
                                    Account__c, 
                                    Sales_Org__c, 
                                    Distribtution_Channel__c, 
                                    Order_Type__c 
                               FROM Order__c 
                              WHERE PO_No__c = '000111' ];
        Account accRecord = [SELECT Id 
                               FROM Account 
                              WHERE Name = 'Sample Grocery2 AVS'];
        Category__c categoryRec = [SELECT Id 
                                     FROM Category__c 
                                    WHERE Name = 'ANIMAL'];
        Test.startTest();
        OrderUtils.getOrderType(orderRec, accRecord.Id, categoryRec.Id);
        Test.stopTest();
        Order__c orderUpdated = [SELECT Id, 
                                        Order_Type__c 
                                   FROM Order__c 
                                  WHERE PO_No__c = '000111' ];
        System.assertEquals(orderUpdated.Order_Type__c , 'ZOR');
    }

    public static testMethod void testSystemNow() {
        Test.startTest();
        DateTime dt = OrderUtils.getSystemNow();
        Test.stopTest();
        System.assertEquals(dt, DateTime.newInstance(2018, 1, 31, 12, 8, 16));
    }
}