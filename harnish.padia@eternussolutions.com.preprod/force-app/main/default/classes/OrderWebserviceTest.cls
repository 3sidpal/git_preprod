@isTest
public class OrderWebserviceTest {


    public static void createCustomSettings() {

        List<RDDCutOff__c> rddCutOffList = new List<RDDCutOff__c>();

        RDDCutOff__c rddCutOff0 = new RDDCutOff__c(
            Name = 'Branded',
            Days__c = 2
        );

        RDDCutOff__c rddCutOff1 = new RDDCutOff__c(
            Name = 'POULTRY',
            Days__c = 2,
            Time__c = '13:00'
        );

        rddCutOffList.add(rddCutOff0);
        rddCutOffList.add(rddCutOff1);

        Insert rddCutOffList;

        List<BU_Mapping__c> buMapping = new List<BU_Mapping__c>();

        BU_Mapping__c brandedMapping = new BU_Mapping__c();

        brandedMapping.Name = 'MAGNOLIA';
        brandedMapping.Account_Field_Name__c = 'Branded__c';

        buMapping.add(brandedMapping);

        BU_Mapping__c poultryMapping = new BU_Mapping__c();

        poultryMapping.Name = 'MEATS';
        poultryMapping.Account_Field_Name__c = 'BU_Poultry__c';

        buMapping.add(poultryMapping);

        BU_Mapping__c gfsMapping = new BU_Mapping__c();

        gfsMapping.Name = 'GFS';
        gfsMapping.Account_Field_Name__c = 'GFS__c';

        buMapping.add(gfsMapping);

        BU_Mapping__c feedsMapping = new BU_Mapping__c();

        feedsMapping.Name = 'FEEDS';
        feedsMapping.Account_Field_Name__c = 'BU_Feeds__c';

        buMapping.add(feedsMapping);

        insert buMapping;

        List<Sales_Org_Division__c> salesOrgDivision = new List<Sales_Org_Division__c>();

        Sales_Org_Division__c brandedSalesOrg = new Sales_Org_Division__c();

        brandedSalesOrg.Name = '14F0-MAGNOLIA-10';
        brandedSalesOrg.Business_Unit__c = 'MAGNOLIA';
        brandedSalesOrg.Code__c = '14F0';
        brandedSalesOrg.Description__c = 'Branded';
        brandedSalesOrg.Division__c = '10';

        salesOrgDivision.add(brandedSalesOrg);

        Sales_Org_Division__c poultrySalesOrg = new Sales_Org_Division__c();

        poultrySalesOrg.Name = '11B0-MEATS-20';
        poultrySalesOrg.Business_Unit__c = 'MEATS';
        poultrySalesOrg.Code__c = '11B0';
        poultrySalesOrg.Description__c = 'Agro/Commodity';
        poultrySalesOrg.Division__c = '20';

        salesOrgDivision.add(poultrySalesOrg);

        Sales_Org_Division__c gfsSalesOrg = new Sales_Org_Division__c();

        gfsSalesOrg.Name = '11D0-GFS-10';
        gfsSalesOrg.Business_Unit__c = 'GFS';
        gfsSalesOrg.Code__c = '11D0';
        gfsSalesOrg.Division__c = '10';

        salesOrgDivision.add(gfsSalesOrg);

        Sales_Org_Division__c gfsSalesOrg2 = new Sales_Org_Division__c();

        gfsSalesOrg2.Name = '11D0-GFS-20';
        gfsSalesOrg2.Business_Unit__c = 'GFS';
        gfsSalesOrg2.Code__c = '11D0';
        gfsSalesOrg2.Division__c = '20';

        salesOrgDivision.add(gfsSalesOrg2);

        Sales_Org_Division__c feedsSalesOrg = new Sales_Org_Division__c();

        feedsSalesOrg.Name = '11C0-FEEDS-20';
        feedsSalesOrg.Business_Unit__c = 'FEEDS';
        feedsSalesOrg.Code__c = '11C0';
        feedsSalesOrg.Description__c = 'Agro/Commodity';
        feedsSalesOrg.Division__c = '20';

        salesOrgDivision.add(feedsSalesOrg);

        insert salesOrgDivision;
    }

    public static Order__c setupdata(){

        createCustomSettings();

        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'National List Price',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        Market__c poultryMarket2 = new Market__c(
            Name = 'POULTRY',
            Active__c = true,
            Market_Code__c = '04'
            );
        marketList.add(poultryMarket2);
        insert marketList;
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true,
            Account_CreateWarehouseForDistributor__c = true,
            Account_CreatePricingCondition__c = true,
            Account_PricingConditionName__c = 'National List Price',
            SIF_AllowEditAndDelete__c = false
            );
        insert settings;
        Id distributorAccountId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id distributorCustomerId = Schema.sobjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();


        /*
        Added on 08/03/2018
        */




        Account acc = new Account();
        acc.Name = 'Sample Grocery1';
        acc.Sales_Organization__c  = 'Branded';
        acc.General_Trade__c  = true;
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.AccountNumber = '123412344';
        acc.Market__c = marketList.get(0).Id;
        acc.RecordTypeId = distributorAccountId;
        insert acc;

        Warehouse__c warehouseObj = new Warehouse__c();
        warehouseObj.Account__c = acc.Id;
        warehouseObj.Address__c = 'test Address';
        warehouseObj.Warehouse_No__c = '001';
        insert warehouseObj;


        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery2';
        acc2.Sales_Organization__c  = 'Branded';
        acc2.General_Trade__c  = true;
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.AccountNumber = '1234123';
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = acc.Id;
        acc2.RecordTypeId = distributorCustomerId;
        acc2.Warehouse__c = warehouseObj.Id;
        insert acc2;
        Id actualSalesRecordTypeId = Schema.sObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();

         //CREATE SAS- Contact
        Id SASrecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        Contact SASvar = new Contact(RecordTypeId=SASrecordTypeId, FirstName = 'xyzFirst', LastName = 'XyZLast', AccountId = acc.Id);
        insert SASvar;

        //AARP: Create New DSP
        Id dspRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        Contact dspVar = new Contact (RecordTypeId=dspRecordTypeId, FirstName = 'xyzSecond1', LastName = 'XyZSecond1', AccountId = acc.Id);
        insert dspVar;

        Order__c orderRecord = new Order__c(
            Account__c = acc2.Id,
            RecordTypeId = actualSalesRecordTypeId,
            Invoice_Date__c = System.today().addDays(-3),
            Invoice_No__c = '1111111',
            PO_No__c   = 'bar@19526',
            SAS__c = SASvar.Id,
            DSP__c = DSPvar.Id
            );
        insert orderRecord;


        /////////////////////////////////Product////////////////////////////////////////////
        /*Id pricebookId = Test.getStandardPricebookId();
        Product2 prod = new Product2(
             Name = 'Product X',
             ProductCode = 'Pro-X',
             isActive = true
        );
        insert prod;

        PricebookEntry pbEntry = new PricebookEntry(
             Pricebook2Id = pricebookId,
             Product2Id = prod.Id,
             UnitPrice = 100.00,
             IsActive = true
        );
        insert pbEntry;*/


        UOM__c uomObj = new UOM__c();
        uomObj.Name = 'KG';
        insert uomObj;


        Custom_Product__c productObj = new Custom_Product__c();
        productObj.Name = 'Test Product';
        productObj.SKU_Code__c = 'TestSkuCode';
        productObj.SellingUOM__c = uomObj.Id;
        insert productObj;

        ///////////////////////////////////////////////////////////////////////////////////

        Conversion__c conversionObj = new Conversion__c();
        conversionObj.name = 'KG';
        conversionObj.UOM__c = uomObj.Id;
        conversionObj.Product__c = productObj.Id;
        conversionObj.Numerator__c = 1;
        conversionObj.Denominator__c = 1;
        insert conversionObj;

        Order_Item__c orderItemObj = new Order_Item__c();
        orderItemObj.Order_Form__c = orderRecord.Id;
        orderItemObj.Product__c    = productObj.Id;
        orderItemObj.Order_Price__c = 60;
        orderItemObj.Invoice_Quantity__c = 0;
        orderItemObj.Order_Quantity__c = 1;
        orderItemObj.Conversion__c = conversionObj.Id;
        insert orderItemObj;

        return orderRecord;

    }


    public static testMethod void OrderWebserviceTestPositive() {

        Order__c orderObj = setupdata();

        Order__c orderObj2 = [SELECT Id, Account__c, RecordTypeId, Invoice_Date__c, Invoice_No__c, PO_No__c, SAS__r.Name, DSP__c,
                            (SELECT Id, Order_Form__c, Product__c, Order_Price__c FROM Order_Items__r)
                             FROM Order__c];

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Order_Item__c' AND NAME ='Purchase Order'];


        String jsonstr=        '{'+
        '   "order": {'+
        '       "Account__c": "'+orderObj2.Account__c+'",'+
        '       "Account_Name__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Account_Taxable__c": "",'+
        '       "Contested__c": "",'+
        '       "Created_in_Pulsar__c": "",'+
        '       "CreatedDate": "",'+
        '       "Delete_Only__c": "",'+
        '       "Delivery_Time__c": "12:00 AM - 12:00 AM",'+
        '       "Delivery_Time_From__c": "12:00 AM",'+
        '       "Delivery_Time_To__c": "12:00 AM",'+
        '       "Distribtution_Channel__c": "30 - General Trade",'+
        '       "Distributor__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Division__c": "10 - Branded",'+
        '       "Employees_Category__c": "",'+
        '       "Export_and_Delete__c": "",'+
        '       "Invoice_No__c": "",'+
        '       "IsDeleted": "",'+
        '       "LastModifiedDate": "",'+
        '       "Monthly_order__c": "",'+
        '       "Name": "",'+
        '       "Order_Reference_No__c": "",'+
        '       "Paid__c": "",'+
        '       "Invoice_No__c": "InvoiceTest1",'+
        '       "PO_Date__c": "2018-06-25",'+
        '       "PO_No__c": "po@ma134",'+
        '       "Product_Category_Name__c": "REFRIGERATED MEATS",'+
        '       "Rehabilitated__c": "",'+
        '       "Remark__c": "",'+
        '       "Remarks__c": "",'+
        '       "Invoice_Date__c": "2021-06-26",'+
        '       "Requested_Delivery_Date__c": "2020-06-26",'+
        '       "Order_Booking_Date__c": "2020-05-26",'+
        '       "Returned_Check__c": "",'+
        '       "Sales_Org__c": "12G0 - PHC",'+
        '       "SAP_Transaction_Type__c": "",'+
        '       "SAS__c": "'+orderObj2.SAS__c+'",'+
        '       "SAS_Name__c": "'+orderObj2.SAS__r.Name+'",'+
        '       "DSP__c": "'+orderObj2.DSP__c+'",'+
        '       "Status__c": "Draft",'+
        '       "XML_Upload__c": "",'+
        '       "Sync_Down__C": "",'+
        '       "SAP_Billing_Doc__c": "",'+
        '       "SAP_DR_No__c": "",'+
        '       "SAP_Error_Log__c": "",'+
        '       "SAP_Sales_Order__c": "",'+
        '       "Shipping_Instructions2__c": "",'+
        '       "Total_Estimated_Amount__c": "2392.05",'+
        '       "Total_Order_Quantity__c": "15",'+
        '       "Total_Order_KG__c": "15.00",'+
        '       "Offline_Created_Date__c": "2018-06-25T06:55:10.179+0530",'+
        '       "Customer_Group_2__c": "13 - Wet Market Dealer",'+
        '       "Soup_Entry_Id__c": "174"'+
        '   },'+
        '   "orderItems": ['+
        '       {'+
        '           "attributes": {'+
        '               "type": "Order_Item__c"'+
        '           },'+
        '           "RecordTypeId": "'+rt.Id+'",'+
        '           "Product__c": "'+orderObj2.Order_Items__r.get(0).Product__c+'",'+
        '           "Order_Form__c": "",'+
        '           "Order_Quantity__c": "1.0",'+
        '           "In_KG__c": "1.0",'+
        '           "Price__c": "",'+
        '           "Estimated_Amount__c": "168.75"'+
        '       }'+
        '   '+
        '   ]'+
        '}';


        /*Order__c orderDelObj = new Order__c();
        orderDelObj.Id = orderObj2.Id;

        System.debug('--se--' + JSON.serialize(orderObj2));

        orderObj2.Id = null;

        orderObj2.Order_Items__r.get(0).Id = null;

        System.debug('--wee--' + JSON.serialize(orderObj2));*/

        delete orderObj2;


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/OrderWebservice';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonstr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            WebServiceResponse.OrderResponse resultRes = OrderWebservice.doPost();
        System.Test.stopTest();


        //System.assertEquals(true, resultRes.success);



    }

    public static testMethod void OrderWebserviceTestOrderInsertFail() {

        Order__c orderObj = setupdata();

        Order__c orderObj2 = [SELECT Id, Account__c, RecordTypeId, Invoice_Date__c, Invoice_No__c, PO_No__c, SAS__r.Name, DSP__c,
                            (SELECT Id, Order_Form__c, Product__c, Order_Price__c FROM Order_Items__r)
                             FROM Order__c];

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Order_Item__c' AND NAME ='Purchase Order'];



        String jsonstr=        '{'+
        '   "order": {'+
        '       "Account__c": "1'+orderObj2.Account__c+'",'+
        '       "Account_Name__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Account_Taxable__c": "",'+
        '       "Contested__c": "",'+
        '       "Created_in_Pulsar__c": "",'+
        '       "CreatedDate": "",'+
        '       "Delete_Only__c": "",'+
        '       "Delivery_Time__c": "12:00 AM - 12:00 AM",'+
        '       "Delivery_Time_From__c": "12:00 AM",'+
        '       "Delivery_Time_To__c": "12:00 AM",'+
        '       "Distribtution_Channel__c": "30 - General Trade",'+
        '       "Distributor__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Division__c": "10 - Branded",'+
        '       "Employees_Category__c": "",'+
        '       "Export_and_Delete__c": "",'+
        '       "Invoice_No__c": "",'+
        '       "IsDeleted": "",'+
        '       "LastModifiedDate": "",'+
        '       "Monthly_order__c": "",'+
        '       "Name": "",'+
        '       "Order_Reference_No__c": "",'+
        '       "Paid__c": "",'+
        '       "Invoice_No__c": "InvoiceTest1",'+
        '       "PO_Date__c": "2018-06-25",'+
        '       "PO_No__c": "po@ma134",'+
        '       "Product_Category_Name__c": "REFRIGERATED MEATS",'+
        '       "Rehabilitated__c": "",'+
        '       "Remark__c": "",'+
        '       "Remarks__c": "",'+
        '       "Invoice_Date__c": "2021-06-26",'+
        '       "Requested_Delivery_Date__c": "2020-06-26",'+
        '       "Order_Booking_Date__c": "2020-05-26",'+
        '       "Returned_Check__c": "",'+
        '       "Sales_Org__c": "12G0 - PHC",'+
        '       "SAP_Transaction_Type__c": "",'+
        '       "SAS__c": "'+orderObj2.SAS__c+'",'+
        '       "SAS_Name__c": "'+orderObj2.SAS__r.Name+'",'+
        '       "DSP__c": "'+orderObj2.DSP__c+'",'+
        '       "Status__c": "Draft",'+
        '       "XML_Upload__c": "",'+
        '       "Sync_Down__C": "",'+
        '       "SAP_Billing_Doc__c": "",'+
        '       "SAP_DR_No__c": "",'+
        '       "SAP_Error_Log__c": "",'+
        '       "SAP_Sales_Order__c": "",'+
        '       "Shipping_Instructions2__c": "",'+
        '       "Total_Estimated_Amount__c": "2392.05",'+
        '       "Total_Order_Quantity__c": "15",'+
        '       "Total_Order_KG__c": "15.00",'+
        '       "Offline_Created_Date__c": "2018-06-25T06:55:10.179+0530",'+
        '       "Customer_Group_2__c": "13 - Wet Market Dealer",'+
        '       "Soup_Entry_Id__c": "174"'+
        '   },'+
        '   "orderItems": ['+
        '       {'+
        '           "attributes": {'+
        '               "type": "Order_Item__c"'+
        '           },'+
        '           "RecordTypeId": "'+rt.Id+'",'+
        '           "Product__c": "'+orderObj2.Order_Items__r.get(0).Product__c+'",'+
        '           "Order_Form__c": "",'+
        '           "Order_Quantity__c": "1.0",'+
        '           "In_KG__c": "1.0",'+
        '           "Price__c": "",'+
        '           "Estimated_Amount__c": "168.75"'+
        '       }'+
        '   '+
        '   ]'+
        '}';


        /*Order__c orderDelObj = new Order__c();
        orderDelObj.Id = orderObj2.Id;

        System.debug('--se--' + JSON.serialize(orderObj2));

        orderObj2.Id = null;

        orderObj2.Order_Items__r.get(0).Id = null;

        System.debug('--wee--' + JSON.serialize(orderObj2));*/

        delete orderObj2;


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/OrderWebservice';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonstr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            WebServiceResponse.OrderResponse resultRes = OrderWebservice.doPost();
        System.Test.stopTest();


        System.assertEquals(false, resultRes.success);



    }


    public static testMethod void OrderWebserviceTestOrderItemInsertFail() {

        Order__c orderObj = setupdata();

        Order__c orderObj2 = [SELECT Id, Account__c, RecordTypeId, Invoice_Date__c, Invoice_No__c, PO_No__c, SAS__r.Name, DSP__c,
                            (SELECT Id, Order_Form__c, Product__c, Order_Price__c FROM Order_Items__r)
                             FROM Order__c];

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Order_Item__c' AND NAME ='Purchase Order'];



        String jsonstr=        '{'+
        '   "order": {'+
        '       "Account__c": "'+orderObj2.Account__c+'",'+
        '       "Account_Name__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Account_Taxable__c": "",'+
        '       "Contested__c": "",'+
        '       "Created_in_Pulsar__c": "",'+
        '       "CreatedDate": "",'+
        '       "Delete_Only__c": "",'+
        '       "Delivery_Time__c": "12:00 AM - 12:00 AM",'+
        '       "Delivery_Time_From__c": "12:00 AM",'+
        '       "Delivery_Time_To__c": "12:00 AM",'+
        '       "Distribtution_Channel__c": "30 - General Trade",'+
        '       "Distributor__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Division__c": "10 - Branded",'+
        '       "Employees_Category__c": "",'+
        '       "Export_and_Delete__c": "",'+
        '       "Invoice_No__c": "",'+
        '       "IsDeleted": "",'+
        '       "LastModifiedDate": "",'+
        '       "Monthly_order__c": "",'+
        '       "Name": "",'+
        '       "Order_Reference_No__c": "",'+
        '       "Paid__c": "",'+
        '       "Invoice_No__c": "InvoiceTest1",'+
        '       "PO_Date__c": "2018-06-25",'+
        '       "PO_No__c": "po@ma134",'+
        '       "Product_Category_Name__c": "REFRIGERATED MEATS",'+
        '       "Rehabilitated__c": "",'+
        '       "Remark__c": "",'+
        '       "Remarks__c": "",'+
        '       "Invoice_Date__c": "2021-06-26",'+
        '       "Requested_Delivery_Date__c": "2020-06-26",'+
        '       "Order_Booking_Date__c": "2020-05-26",'+
        '       "Returned_Check__c": "",'+
        '       "Sales_Org__c": "12G0 - PHC",'+
        '       "SAP_Transaction_Type__c": "",'+
        '       "SAS__c": "'+orderObj2.SAS__c+'",'+
        '       "SAS_Name__c": "'+orderObj2.SAS__r.Name+'",'+
        '       "DSP__c": "'+orderObj2.DSP__c+'",'+
        '       "Status__c": "Draft",'+
        '       "XML_Upload__c": "",'+
        '       "Sync_Down__C": "",'+
        '       "SAP_Billing_Doc__c": "",'+
        '       "SAP_DR_No__c": "",'+
        '       "SAP_Error_Log__c": "",'+
        '       "SAP_Sales_Order__c": "",'+
        '       "Shipping_Instructions2__c": "",'+
        '       "Total_Estimated_Amount__c": "2392.05",'+
        '       "Total_Order_Quantity__c": "15",'+
        '       "Total_Order_KG__c": "15.00",'+
        '       "Offline_Created_Date__c": "2018-06-25T06:55:10.179+0530",'+
        '       "Customer_Group_2__c": "13 - Wet Market Dealer",'+
        '       "Soup_Entry_Id__c": "174"'+
        '   },'+
        '   "orderItems": ['+
        '       {'+
        '           "attributes": {'+
        '               "type": "Order_Item__c"'+
        '           },'+
        '           "RecordTypeId": "'+rt.Id+'gg'+'",'+
        '           "Product__c": "'+orderObj2.Order_Items__r.get(0).Product__c+'",'+
        '           "Order_Form__c": "",'+
        '           "Order_Quantity__c": "1.0",'+
        '           "In_KG__c": "1.0",'+
        '           "Price__c": "",'+
        '           "Estimated_Amount__c": "168.75"'+
        '       }'+
        '   '+
        '   ]'+
        '}';


        /*Order__c orderDelObj = new Order__c();
        orderDelObj.Id = orderObj2.Id;

        System.debug('--se--' + JSON.serialize(orderObj2));

        orderObj2.Id = null;

        orderObj2.Order_Items__r.get(0).Id = null;

        System.debug('--wee--' + JSON.serialize(orderObj2));*/

        delete orderObj2;


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/OrderWebservice';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonstr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            WebServiceResponse.OrderResponse resultRes = OrderWebservice.doPost();
        System.Test.stopTest();


        System.assertEquals(false, resultRes.success);



    }


    public static testMethod void OrderWebserviceTestDuplicate() {

        Order__c orderObj = setupdata();

        Order__c orderObj2 = [SELECT Id, Account__c, RecordTypeId, Invoice_Date__c, Invoice_No__c, PO_No__c, SAS__r.Name, DSP__c,
                            (SELECT Id, Order_Form__c, Product__c, Order_Price__c FROM Order_Items__r)
                             FROM Order__c];

        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Order_Item__c' AND NAME ='Purchase Order'];

        System.debug('--dup--' + orderObj2);



        String jsonstr=        '{'+
        '   "order": {'+
        '       "Account__c": "'+orderObj2.Account__c+'",'+
        '       "Account_Name__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Account_Taxable__c": "",'+
        '       "Contested__c": "",'+
        '       "Created_in_Pulsar__c": "",'+
        '       "CreatedDate": "",'+
        '       "Delete_Only__c": "",'+
        '       "Delivery_Time__c": "12:00 AM - 12:00 AM",'+
        '       "Delivery_Time_From__c": "12:00 AM",'+
        '       "Delivery_Time_To__c": "12:00 AM",'+
        '       "Distribtution_Channel__c": "30 - General Trade",'+
        '       "Distributor__c": "AMY URGINO SARI-SARI STORE",'+
        '       "Division__c": "10 - Branded",'+
        '       "Employees_Category__c": "",'+
        '       "Export_and_Delete__c": "",'+
        '       "Invoice_No__c": "",'+
        '       "IsDeleted": "",'+
        '       "LastModifiedDate": "",'+
        '       "Monthly_order__c": "",'+
        '       "Name": "",'+
        '       "Order_Reference_No__c": "",'+
        '       "Paid__c": "",'+
        '       "Invoice_No__c": "InvoiceTest1",'+
        '       "PO_Date__c": "2018-06-25",'+
        '       "PO_No__c": "bar@19526",'+
        '       "Product_Category_Name__c": "REFRIGERATED MEATS",'+
        '       "Rehabilitated__c": "",'+
        '       "Remark__c": "",'+
        '       "Remarks__c": "",'+
        '       "Invoice_Date__c": "2021-06-26",'+
        '       "Requested_Delivery_Date__c": "2020-06-26",'+
        '       "Order_Booking_Date__c": "2020-05-26",'+
        '       "Returned_Check__c": "",'+
        '       "Sales_Org__c": "12G0 - PHC",'+
        '       "SAP_Transaction_Type__c": "",'+
        '       "SAS__c": "'+orderObj2.SAS__c+'",'+
        '       "SAS_Name__c": "'+orderObj2.SAS__r.Name+'",'+
        '       "DSP__c": "'+orderObj2.DSP__c+'",'+
        '       "Status__c": "Draft",'+
        '       "XML_Upload__c": "",'+
        '       "Sync_Down__C": "",'+
        '       "SAP_Billing_Doc__c": "",'+
        '       "SAP_DR_No__c": "",'+
        '       "SAP_Error_Log__c": "",'+
        '       "SAP_Sales_Order__c": "",'+
        '       "Shipping_Instructions2__c": "",'+
        '       "Total_Estimated_Amount__c": "2392.05",'+
        '       "Total_Order_Quantity__c": "15",'+
        '       "Total_Order_KG__c": "15.00",'+
        '       "Offline_Created_Date__c": "2018-06-25T06:55:10.179+0530",'+
        '       "Customer_Group_2__c": "13 - Wet Market Dealer",'+
        '       "Soup_Entry_Id__c": "174"'+
        '   },'+
        '   "orderItems": ['+
        '       {'+
        '           "attributes": {'+
        '               "type": "Order_Item__c"'+
        '           },'+
        '           "RecordTypeId": "'+rt.Id+'",'+
        '           "Product__c": "'+orderObj2.Order_Items__r.get(0).Product__c+'",'+
        '           "Order_Form__c": "",'+
        '           "Order_Quantity__c": "1.0",'+
        '           "In_KG__c": "1.0",'+
        '           "Price__c": "",'+
        '           "Estimated_Amount__c": "168.75"'+
        '       }'+
        '   '+
        '   ]'+
        '}';


        /*Order__c orderDelObj = new Order__c();
        orderDelObj.Id = orderObj2.Id;

        System.debug('--se--' + JSON.serialize(orderObj2));

        orderObj2.Id = null;

        orderObj2.Order_Items__r.get(0).Id = null;

        System.debug('--wee--' + JSON.serialize(orderObj2));*/



        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/OrderWebservice';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonstr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            WebServiceResponse.OrderResponse resultRes = OrderWebservice.doPost();
        System.Test.stopTest();


        System.assertEquals(false, resultRes.success);



    }



    public static testMethod void OrderWebserviceTestGetMethod() {

        setupdata();


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/OrderWebservice';  //Request URL
        req.httpMethod = 'GET'; //HTTP Request Type

        System.Test.startTest();
            List<WebServiceResponse.ProductResponse> resultRes = OrderWebservice.doGet();
        System.Test.stopTest();


        System.assertEquals('TEST PRODUCT', resultRes.get(0).name);

    }

}