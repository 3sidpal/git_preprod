public class PricingConditionDataTrigger_Handler {
    
    public static void onBeforeInsert(List<Pricing_Condition_Data__c>newPCDList){
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        if(settings!=null){
            if(settings.PCD_AttachedToNationalPricing__c == true){
                if(String.isNotBlank(settings.DataloaderSFDCId__c)){
                    Id currentUser = UserInfo.getUserId();
                    if(currentUser == settings.DataloaderSFDCId__c){
                        //GET NATIONAL LIST PRICE CONDITION 
                        if(String.isNotBlank(settings.PCD_PricingConditionName__c)){
                            List<Pricing_Condition__c> pcdList = new List<Pricing_Condition__c>();
                            for(Pricing_Condition__c pc : [SELECT Id FROM Pricing_Condition__c WHERE Name = : settings.PCD_PricingConditionName__c LIMIT 1]){
                                pcdList.add(pc);
                            }
                            if(pcdList.size()>0){
                                for(Pricing_Condition_Data__c pcd : newPCDList){
                                    pcd.Pricing_Condition__c = pcdList.get(0).Id;
                                }    
                            }
                                
                        }
                    }
                }
            }
            
            if(settings.PCD_AssignConversionRecord__c == true){
                List<Pricing_Condition_Data__c> newPricingConditionData = new List<Pricing_Condition_Data__c>();
                Map<Id,Custom_Product__c> customProductMap = new Map<Id,Custom_Product__c>();
                Map<String, Conversion__c> conversionMapByDloaderId = new Map<String, Conversion__c>();
                Set<id> productIdSet = new Set<id>();
                Set<String> skuUomCodeSet = new Set<String>();
                for(Pricing_Condition_Data__c pcd : newPCDList){
                    if(pcd.SAP_uom__c!=null && String.isNotBlank(pcd.SAP_uom__c)){
                        newPricingConditionData.add(pcd);
                        productIdSet.add(pcd.product__c);   
                    }
                }
                
                for(Custom_Product__c prod : [SELECT Id, SKU_Code__c  FROM Custom_product__c WHERE Id IN:productIdSet]){
                    customProductMap.put(prod.Id, prod);
                }
                
                for(Pricing_Condition_Data__c pcd2 : newPricingConditionData ){
                    String currentUomName = pcd2.SAP_uom__c;
                    if(customProductMap.containsKey(pcd2.Product__c)){
                        skuUomCOdeSet.add(customProductMap.get(pcd2.product__c).SKU_Code__c+'/'+currentUomName);
                    }
                }
                
                for(conversion__c con : [SELECT id, Dataloader_ID__c FROM conversion__c WHERE Dataloader_ID__c IN: skuUomCOdeSet]){
                    conversionMapByDloaderId.put(con.Dataloader_ID__c,con);
                }
                
                for(Pricing_Condition_Data__c pcd3 : newPricingConditionData){
                    String currentUomName = pcd3.SAP_uom__c;
                    String currentDataloaderId;
                    if(customProductMap.containsKey(pcd3.product__c)){
                        currentDataloaderId = customProductMap.get(pcd3.product__c).SKU_Code__c+'/'+currentUomName;
                        if(conversionMapByDloaderId.containsKey(currentDataloaderId)){
                            pcd3.UOM_1__c = conversionMapByDloaderId.get(currentDataloaderId).id;
                        }else{
                            //return error for the record.
                            pcd3.addError('No UOM (Conversion) record exist/match. ');
                        }
                    }
                    pcd3.SAP_uom__c=null;
                }
            }
        }
    }
    
    
    public static void onBeforeUpdate(List<Pricing_Condition_Data__c> newPCDList , Map<Id,Pricing_Condition_Data__c> oldPCDMap){
        TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        if(settings.PCD_AssignConversionRecord__c == true){
                List<Pricing_Condition_Data__c> newPricingConditionData = new List<Pricing_Condition_Data__c>();
                Map<Id,Custom_Product__c> customProductMap = new Map<Id,Custom_Product__c>();
                Map<String, Conversion__c> conversionMapByDloaderId = new Map<String, Conversion__c>();
                Set<id> productIdSet = new Set<id>();
                Set<String> skuUomCodeSet = new Set<String>();
                for(Pricing_Condition_Data__c pcd : newPCDList){
                    if(pcd.SAP_uom__c!=null && String.isNotBlank(pcd.SAP_uom__c)){
                        newPricingConditionData.add(pcd);
                        productIdSet.add(pcd.product__c);   
                    }
                }
                
                for(Custom_Product__c prod : [SELECT Id, SKU_Code__c  FROM Custom_product__c WHERE Id IN:productIdSet]){
                    customProductMap.put(prod.Id, prod);
                }
                
                for(Pricing_Condition_Data__c pcd2 : newPricingConditionData ){
                    String currentUomName = pcd2.SAP_uom__c;
                    if(customProductMap.containsKey(pcd2.Product__c)){
                        skuUomCOdeSet.add(customProductMap.get(pcd2.product__c).SKU_Code__c+'/'+currentUomName);
                    }
                }
                
                for(conversion__c con : [SELECT id, Dataloader_ID__c FROM conversion__c WHERE Dataloader_ID__c IN: skuUomCOdeSet]){
                    conversionMapByDloaderId.put(con.Dataloader_ID__c,con);
                }
                
                for(Pricing_Condition_Data__c pcd3 : newPricingConditionData){
                    String currentUomName = pcd3.SAP_uom__c;
                    String currentDataloaderId;
                    if(customProductMap.containsKey(pcd3.product__c)){
                        currentDataloaderId = customProductMap.get(pcd3.product__c).SKU_Code__c+'/'+currentUomName;
                        if(conversionMapByDloaderId.containsKey(currentDataloaderId)){
                            pcd3.UOM_1__c = conversionMapByDloaderId.get(currentDataloaderId).id;
                        }else{
                            //return error for the record.
                            pcd3.addError('No UOM (Conversion) record exist/match. ');
                        }
                    }
                    pcd3.SAP_uom__c=null;
                }
            }
    }
}