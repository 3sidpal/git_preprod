@isTest
private class PricingConditionDataTrigger_Test {
    @testSetup static void setupdata(){
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            PCD_AttachedToNationalPricing__c = true,
            DataloaderSFDCId__c = UserInfo.getUserId(),
            PCD_PricingConditionName__c = 'NATIONAL',
            PCD_AssignConversionRecord__c = true
            );
        insert settings;
        List<Pricing_Condition__c> pricingConditionList = new List<Pricing_Condition__c>();
        List<Market__c> marketList = new List<Market__c>();
        List<Custom_Product__c> parentProductList = new List<Custom_Product__c>();
        Id distributorRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        Pricing_Condition__c nationalPricing = new Pricing_Condition__c(
            Name = 'NATIONAL',
            SAP_Code__c = '12345'
            );
        pricingConditionList.add(nationalPricing);
        Pricing_Condition__c regionalPricing = new Pricing_Condition__c(
            Name = 'REGIONAL',
            SAP_Code__c = '89798'
            );
        pricingConditionList.add(regionalPricing);
        Pricing_Condition__c segmentPricing = new Pricing_Condition__c(
            Name = 'SEGMENT',
            SAP_Code__c = '098098'
            );
        pricingConditionList.add(segmentPricing);
        insert pricingConditionList;
        Account distributorAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = distributorRecordTypeId,
            Sales_Organization__c  = 'Feeds',
            General_Trade__c  = true,
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'TestDistributor11'
            );
        insert distributorAccount;
        Market__c meatMarket = new Market__c(
            Name = 'Meats',
            Active__c = true,
            Market_Code__c = '01'
            );
        marketList.add(meatMarket);
        Market__c gfsMarket = new Market__c(
            Name = 'GFS',
            Active__c = true,
            Market_Code__c = '02'
            );
        marketList.add(gfsMarket);
        Market__c poultryMarket = new Market__c(
            Name = 'Poultry',
            Active__c = true,
            Market_Code__c = '03'
            );
        marketList.add(poultryMarket);
        insert marketList;

        Warehouse__c wr = new Warehouse__c();
        wr.Account__c = distributorAccount.Id;
        wr.Address__c = distributorAccount.Name + distributorAccount.Id;
        wr.Warehouse_No__c = String.valueOf(distributorAccount.Id) + String.valueOf(System.now().millisecond());
        insert wr;
        distributorAccount.Warehouse__c = wr.Id;
        update distributorAccount;
        
        List<Account> accountForInsertList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Sample Grocery2';
        acc.RecordTypeId = customerRecordTypeId;
        acc.Sales_Organization__c  = 'Feeds';
        acc.General_Trade__c  = true;
        acc.BU_Feeds__c = true;
        acc.Branded__c = true;
        acc.GFS__c = true;
        acc.BU_Poultry__c = true;
        acc.Market__c = marketList.get(0).Id;
        acc.Distributor__c = distributorAccount.Id;
        acc.Warehouse__c = wr.Id;
        acc.AccountNumber = 'ACT21';
        accountForInsertList.add(acc);
        
        Account acc2 = new Account();
        acc2.Name = 'Sample Grocery3';
        acc2.RecordTypeId = customerRecordTypeId;
        acc2.Sales_Organization__c  = 'Feeds';
        acc2.General_Trade__c  = true;
        acc2.BU_Feeds__c = true;
        acc2.Branded__c = true;
        acc2.GFS__c = true;
        acc2.BU_Poultry__c = true;
        acc2.Market__c = marketList.get(0).Id;
        acc2.Distributor__c = distributorAccount.Id;
        acc2.Warehouse__c = wr.Id;
        acc2.AccountNumber = 'ACT31';
        accountForInsertList.add(acc2);
        insert accountForInsertList;
        Custom_Product__c prodParent1 = new Custom_Product__c();
        prodParent1.Name = 'ParentProductFeeds';
        prodParent1.Business_Unit__c = 'Feeds';
        prodParent1.Feeds__c = true;
        prodParent1.Market__c = marketList.get(0).Id;
        prodParent1.SKU_Code__c = '111222333';
        parentProductList.add(prodParent1);
        
        Custom_Product__c prodParent2 = new Custom_Product__c();
        prodParent2.Name = 'ParentProductGFS';
        prodParent2.Business_Unit__c = 'GFS';
        prodParent2.Feeds__c = true;
        prodParent2.Market__c = marketList.get(1).Id;
        prodParent2.SKU_Code__c = '333444555';
        parentProductList.add(prodParent2);
        
        if(parentProductList.size()>0){
            System.debug('\n\n\nPARENT PRODUCT LIST : ' + parentProductList + '\n\n\n');
            insert parentProductList;
        }
        List<Conversion__c> ConversionforInsertList = new List<Conversion__c>();
        for(Custom_Product__c customProduct : parentProductList){
            Conversion__c conversionRecord = new Conversion__c(
                Name = 'KG',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1,
                Dataloader_ID__c = customProduct.SKU_Code__c+'/KG'
                );
            ConversionforInsertList.add(conversionRecord);
            Conversion__c conversionRecordPC = new Conversion__c(
                Name = 'PC',
                Product__c = customProduct.Id,
                Numerator__c = 1,
                Denominator__c = 1,
                Dataloader_ID__c = customProduct.SKU_Code__c+'/PC'
                );
            ConversionforInsertList.add(conversionRecordPC);
        }
        if(ConversionforInsertList.size()>0){
            insert ConversionforInsertList;
        }
    }
    private static testMethod void test() {
        Conversion__c conv = [SELECT Id,Name,Product__c FROM Conversion__c LIMIT 1];
        Test.startTest();
            Pricing_Condition_Data__c pcd = new Pricing_Condition_Data__c(
                UOM_1__c = conv.Id,
                SAP_uom__c = 'KG',
                Product__c = conv.Product__c,
                Active__c = true,
                Effective_Date__c = Date.today()
                );
                System.debug('\n\n\nPRICING CONDITION DATA : ' + pcd + '\n\n\n');
            insert pcd;
        Test.stopTest();
    }
    
    private static testMethod void testUpdate() {
        Conversion__c conv = [SELECT Id,Name,Product__c FROM Conversion__c LIMIT 1];
        Test.startTest();
            Pricing_Condition_Data__c pcd = new Pricing_Condition_Data__c(
                UOM_1__c = conv.Id,
                SAP_uom__c = 'KG',
                Product__c = conv.Product__c,
                Active__c = true,
                Effective_Date__c = Date.today()
                );
                System.debug('\n\n\nPRICING CONDITION DATA : ' + pcd + '\n\n\n');
            insert pcd;
            
            pcd.SAP_uom__c = 'KG';
            update pcd;
        Test.stopTest();
    }


}