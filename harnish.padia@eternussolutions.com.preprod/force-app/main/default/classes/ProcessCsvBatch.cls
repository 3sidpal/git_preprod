public class ProcessCsvBatch implements Database.batchable<string>, Database.stateful {

    public Upload_Job__c uploadJob;
    public List<ContentVersion> contVerLst;
    public List<String> headersLst;
    public static final string CR_LF = '\r\n';
    public List<String> errorReasonCsv;
    public List<String>successIds;
    public Boolean processParents;
    public Boolean contVersionId;
    public Map<String,String> primaryStockallocationMap;
    
    public ProcessCsvBatch() {}
    public ProcessCsvBatch(String recId, String uploadJobId, Boolean processParents, List<String> errorReasonCsv, Map<String,String> primaryStockallocationMap) {
        system.debug('======Heap limit======'+Limits.getHeapSize());
        if(primaryStockallocationMap == null) {
            primaryStockallocationMap = new Map<String,String>();
        }
        this.primaryStockallocationMap = primaryStockallocationMap;
        
        uploadJob = [SELECT Object_Name__c FROM Upload_Job__c WHERE Id = :uploadJobId];
        contVerLst = [SELECT ContentDocumentId,
                             ContentDocument.Title,
                             ContentSize,
                             PathOnClient,
                             FileExtension,
                             VersionData
                      FROM ContentVersion
                      WHERE Id = :recId];
        if(errorReasonCsv == null) {
            errorReasonCsv = new List<String>();
        }
        this.errorReasonCsv = errorReasonCsv;
        successIds = new List<String>();
        headersLst = new List<String>();
        this.processParents = processParents;
        system.debug('======Heap limit======'+Limits.getHeapSize());
    }
    public Iterable<string> start(Database.batchableContext batchableContext) {
        system.debug('======Heap limit======'+Limits.getHeapSize());
        return new  CSVIterator(contVerLst[0].VersionData.toString(), CR_LF, headersLst);
    }
    
    public  void execute(Database.BatchableContext batchableContext, List<String> scope) {
            ProcessCsvBatchHandler.processRecords(scope,headersLst,errorReasonCsv,
                uploadJob.Object_Name__c, processParents, primaryStockallocationMap);
    }
    
    public void finish(Database.BatchableContext batchableContext) {
        system.debug('====finish===');
        List<Upload_Job__c> uploadJobLst = [SELECT Status__c FROM Upload_Job__c WHERE Id = :uploadJob.Id];
        if(uploadJobLst.isEmpty() == false) {
            
            if(processParents != null && processParents == true && uploadJob.Object_Name__c == 'Stock_Allocation__c') {
                Database.executeBatch(new ProcessCsvBatch(contVerLst[0].Id, uploadJob.Id, false, errorReasonCsv, primaryStockallocationMap));
            }
            else {
	            Map<String,Map<String,String>> uploadJobStatus = new Map<String,Map<String,String>>();
	            if(!String.isBlank(uploadJobLst[0].Status__c)){
	                uploadJobStatus = (Map<String,Map<String,String>>)Json.deserialize(uploadJobLst[0].Status__c, Map<String,Map<String,String>>.class);
	            }
	            if(!errorReasonCsv.isEmpty()) {
	                ContentVersion conVer = new ContentVersion();
	                conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
	                conVer.PathOnClient = contVerLst[0].ContentDocument.Title+' Error.csv';
	                conVer.Title = contVerLst[0].ContentDocument.Title+' Error';
	                conVer.VersionData = Blob.valueOf(errorReasonCsv[0]); 
	                insert conVer;
	                
	                // First get the content document Id from ContentVersion
	                Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
	                 
	                //Create ContentDocumentLink
	                ContentDocumentLink cDe = new ContentDocumentLink();
	                cDe.ContentDocumentId = conDoc;
	                cDe.LinkedEntityId = uploadJob.Id;
	                cDe.ShareType = 'I';
	                cDe.Visibility = 'AllUsers';
	                insert cDe;
	
	                uploadJobStatus.put(contVerLst[0].Id, new Map<String,String>{'Status'=>'Failure',
	                    'ErrorCSV'=>conVer.Id});
	            }
	            else {
	                uploadJobStatus.put(contVerLst[0].Id, new Map<String,String>{'Status'=>'Success'});
	            }
	            uploadJobLst[0].Status__c = Json.serializePretty(uploadJobStatus);
	            update uploadJobLst;
            }
        }
    }
}