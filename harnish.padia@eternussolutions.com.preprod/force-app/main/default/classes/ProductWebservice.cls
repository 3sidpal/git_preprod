@RestResource(urlMapping='/CustomProducts/*')
global with sharing class ProductWebservice {

    @HttpGet
    global static List<WebServiceResponse.ProductResponse> doGet() {
        List<WebServiceResponse.ProductResponse> prodList = new List<WebServiceResponse.ProductResponse>();
        List<Id> productIds = new List<Id>();
        List<Id> uomIds = new List<Id>();
        Profile micProfile = null;
        String MIC_PROFILE_NAME = 'MIC Direct';
        String MIC_PRODUCT_CATEGORY = 'ICE CREAM';
        Boolean isMICUser = false;
        Category__c micProdCategory = null;
		Set<Id> productIdSet = new Set<Id>();
        RestRequest req = RestContext.request;
        String userId = req.params.get('userId');
        
        try
        {
            micProfile = [select Id from Profile where Name = :MIC_PROFILE_NAME];
            micProdCategory = [select Id, Name from Category__c where Name =:MIC_PRODUCT_CATEGORY];
            User user = [select Id, ProfileId from User where Id = :userId];
            if(micProfile.Id == user.ProfileId)
            {
                isMICUser = true;
            }
        }catch(Exception e){
        }
        
        Map<Id, Custom_Product__c> customProdMap = new Map<Id, Custom_Product__c>([SELECT Id, SellingUOM__c ,Branded_UOM__c, SellingUOM__r.Name, Branded_UOM__r.KG_Conversion_Rate__c, SKU_Code__c,  Name, Category1__c FROM Custom_Product__c where SellingUOM__c in (Select Id from UOM__c)]);
        for(Conversion__c conv : [SELECT Id, Name, Product__c, UOM__c , KG_Conversion_Rate__c, CS_Conversion_Rate__c FROM Conversion__c where Product__c in :customProdMap.keySet()]) 
        {
            Custom_Product__c prod = customProdMap.get(conv.Product__c);
            if(productIdSet.contains(prod.Id)){
                continue;
            }
            if(isMICUser && micProdCategory != null && micProdCategory.Id == prod.Category1__c && conv.Name == 'GAL')
            {
                prodList.add(new WebServiceResponse.ProductResponse(prod.Id, prod.SellingUOM__r.Name, conv.KG_Conversion_Rate__c, conv.CS_Conversion_Rate__c, prod.SKU_Code__c, prod.Name, prod.Category1__c));
                //continue;
                productIdSet.add(prod.Id);
            }
            else if(conv.UOM__c == customProdMap.get(conv.Product__c).SellingUOM__c)
            {
                //prodList.add(new WebServiceResponse.ProductResponse(prod.Id, prod.SellingUOM__r.Name, conv.KG_Conversion_Rate__c, prod.SKU_Code__c, prod.Name, prod.Category1__c));
                prodList.add(new WebServiceResponse.ProductResponse(prod.Id, prod.SellingUOM__r.Name, conv.KG_Conversion_Rate__c, conv.CS_Conversion_Rate__c, prod.SKU_Code__c, prod.Name, prod.Category1__c));
                productIdSet.add(prod.Id);
            }
            
        }
        return prodList;
    }

    /*@HttpPost
    global static WebServiceResponse.OrderItemResponse doPost(String orderItemstr) {

        System.debug('orderItemstr :'+orderItemstr);
        List<Object> jsonObjectList = (List<Object>)JSON.deserializeUntyped(orderItemstr);
        List<Order_Item__c> orderItems = new List<Order_Item__c>();
        List<Order_Item__c> orderItemNewList = new List<Order_Item__c>();
        System.debug('jsonObjectList :'+jsonObjectList);
        String orderId = '';
        String errorMessage = '';
        WebServiceResponse.OrderItemResponse orderResponse = null;
        for(Object obj :jsonObjectList){
            String jsonStr = JSON.serialize(obj);
            System.debug('jsonStr :'+jsonStr);
            Order_Item__c sObj = (Order_Item__c)JSON.deserialize(jsonStr, Order_Item__c.class);
            orderId = sObj.Order_Form__c;
            orderItems.add(sObj);
        }
        System.debug('orderItems'+orderItems);
        System.debug('orderId'+orderId);
        if(isDuplicatePO(orderId)){

            System.debug('In duplicate PO'+orderId);
            //Check for alredy inserted records
            Set<Id> orderItemIds = getExistingOrderItems(orderId);

            if(orderItemIds != null && orderItemIds.size() == orderItems.size()){
                System.debug('orderItemIds'+orderItemIds);
                orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(orderItemIds), true, new List<String>());
                return orderResponse;
            }
            errorMessage = 'Duplicate PO number!!';
            System.debug('duplicate :'+errorMessage);
            orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(), false, new List<String>{errorMessage});
            return orderResponse;
        } else { // Added else part on 23-06-2018

            /*Set<Id> orderItemIds = getExistingOrderItems(orderId);
            Map<Id, Order_Item__c> orderItemIdsMap = getExistingOrderItemsMap(orderId);
            System.debug('Anydatatype_msg' + orderItemIdsMap);
            System.debug('orderItemIds'+orderItemIds);
            System.debug('orderItemIdsout'+orderItemIds.size() + '----' + orderItems.size());
            if(orderItemIds != null && orderItemIds.size() == orderItems.size()){
                System.debug('orderItemIdsin'+orderItemIds);
                orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(orderItemIds), true, new List<String>());
                return orderResponse;
            }


            for(Order_Item__c orderItemItr : orderItems) {

                System.debug('--70--' + orderItemItr.Product__c);
                System.debug('--71--' + orderItemIdsMap);
                if(!orderItemIdsMap.containsKey(orderItemItr.Product__c)) {

                    orderItemNewList.add(orderItemItr);

                } else {

                    orderItemNewList.add(orderItemIdsMap.get(orderItemItr.Product__c));

                }

            }

            deleteOrderItems(orderId);




        }
        /*
        List<API_Key__c> apiKeyList = API_Key__c.getall().values();
        String apiKey = '';
        errorMessage = 'Incorrect Signature. Pls ask admin to check correct API key.';
        if(apiKeyList == null){
            orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(), false, new List<String>{errorMessage});
            return orderResponse;
        }
        apiKey = apiKeyList[0].Name;

        RestRequest req = RestContext.request;
        String hash = req.requestURI.substring(req.requestURI.indexOf('hash=')+5);
        String orderData = '';
        if(orderItems == null){
            System.debug('orderItems :'+orderItems);
            orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(), false, new List<String>{errorMessage});
            return orderResponse;
        }
        orderData = String.valueOf(jsonObjectList);
        System.debug('orderData :'+orderData);
        Blob data = crypto.generateMac('HmacSHA256',Blob.valueOf(orderItemstr), Blob.valueOf(apiKey));
        String generatedHash = EncodingUtil.base64Encode(data);
        System.debug('generatedHash :'+generatedHash);
        System.debug('hash :'+hash);
        if(!hash.equals(generatedHash)){
            System.debug('hash not match');
            orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(), false, new List<String>{errorMessage});
            return orderResponse;
        }

        try {

            System.debug('----113--' + orderItems);
            upsert orderItems;

            //upsert orderItemNewList;
            System.debug('----120--' + orderItems);
            /*if(String.isNotBlank(orderId)){
                try{
                    Order__c orderRecord = new Order__c(Id=orderId,Status__c ='Submitted');
                    update orderRecord;
                }
                catch(Exception ex){
                }
            }
        }
        catch(Exception ex) {
            System.debug('exception :'+ex);
            errorMessage = ex.getMessage();
            orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(), false, new List<String>{errorMessage});
            return orderResponse;
        }
        Map<Id, sObject> orderMap = new Map<Id, sObject>(orderItems);
        orderResponse = new WebServiceResponse.OrderItemResponse(new List<Id>(orderMap.keySet()), true, new List<String>());
        System.debug('--145--' + orderResponse);
        return orderResponse;
    }

    private static Set<Id> getExistingOrderItems(String orderId){
        Set<Id> orderItemIds = new Set<Id>();
        try{
            Order__c order = [select Id, (select Id, Product__c, Order_Form__c from Order_Items__r limit 50000) from Order__c where Id =:orderId];
            if(order.Order_Items__r != null && order.Order_Items__r.size() > 0){
                Map<Id, Order_Item__c> orderItemMap = new Map<Id, Order_Item__c>(order.Order_Items__r);
                orderItemIds.addAll(orderItemMap.keySet());
            }
        }catch(Exception e){
            system.debug('exception getExistingOrderItems :'+e);
        }
        return orderItemIds;
    }

    private static Map<Id, Order_Item__c> getExistingOrderItemsMap(String orderId){
        Map<Id, Order_Item__c> orderItemIdsMap = new Map<Id, Order_Item__c>();
        try {
            Order__c order = [select Id, (select Id, Product__c, Order_Form__c from Order_Items__r limit 50000) from Order__c where Id =:orderId];

            System.debug('--155--' + order + '---' + order.Order_Items__r);

            if(order.Order_Items__r != null && order.Order_Items__r.size() > 0){
                for(Order_Item__c orderItemItr :  order.Order_Items__r) {
                    orderItemIdsMap.put(orderItemItr.Product__c, orderItemItr);
                }
            }
        } catch(Exception e) {
            system.debug('exception getExistingOrderItemsMap :'+e);
        }
        System.debug('--165--' + orderItemIdsMap);
        return orderItemIdsMap;
    }


    private static void deleteOrderItems(String orderId) {

        List<Order_Item__c> orderItemDeleteList = new List<Order_Item__c>();

        try {
            Order__c order = [select Id, (select Id, Product__c, Order_Form__c from Order_Items__r limit 50000) from Order__c where Id =:orderId];

            if(order.Order_Items__r != null && order.Order_Items__r.size() > 0){
                for(Order_Item__c orderItemItr :  order.Order_Items__r) {
                    orderItemDeleteList.add(orderItemItr);
                }
            }

            delete orderItemDeleteList;
        } catch(Exception e) {
            system.debug('exception deleteOrderItems :'+e);
        }
    }


    private static boolean isDuplicatePO(String orderId){
        boolean hasDuplicatePO = false;
        try{
            Order__c order = [select Id, PO_No__c, Account__c from Order__c where Id =:orderId];
            System.debug(' order :'+order);
            List<Order__c> orderList = [select Id from Order__c
                                        where PO_No__c = :order.PO_No__c
                                        and   Account__c = :order.Account__c
                                        and    Id != :orderId];
            system.debug('orderList :'+orderList);
            if(orderList.size() > 0){
                hasDuplicatePO = true;
            }
        }catch(Exception e){
            system.debug('Exception :'+e);
            hasDuplicatePO = false;
        }
        return hasDuplicatePO;
    }*/
}