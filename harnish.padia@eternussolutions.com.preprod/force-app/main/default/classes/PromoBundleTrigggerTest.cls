@isTest
public class PromoBundleTrigggerTest {

    public static testmethod void triggerTest() {
        TestDataFactory objTestDataFactory = new TestDataFactory();

        List<Custom_Product__c> prodList =  objTestDataFactory.createTestUOMProducts(20, TRUE, FALSE);

        List<Account> DistributorAccobj = TestDataFactory.createAccounts(1,'Distributor/ Direct',true);

        Ship_Sold_To__c spTo = new Ship_Sold_To__c(
            AccountMaster__c = DistributorAccobj[0].Id,
            AccountChild__c = DistributorAccobj[0].Id,
            Ship_Sold_To__c = 'SP',
            Dataloader_ID__c = DistributorAccobj[0].Id+'SP2'
        );
        Insert spTo;

        TriggerFlagControl__c settings = new TriggerFlagControl__c(
        OrderItem_PlantDetermination__c = TRUE,
        orderItemOldTrigger__c = FALSE
        );
        insert settings;

        Validation_Rules__c validationRule = new Validation_Rules__c(
        Order_RequestedDeliveryDateLessPODate__c = true
        );
        insert validationRule;

        List<Order__c > orderList = objTestDataFactory.createTestOrder(10,DistributorAccobj[0].id);
        orderList[0].Ship_Sold_To__c = spTo.Id;
        orderList[0].requested_Delivery_Date__c = system.today()+2;
        orderList[0].Invoice_Date__c = system.today()+3;
        orderList[0].PO_Date__c = system.today()+3;
        orderList[1].Ship_Sold_To__c = NULL;
        orderList[1].requested_Delivery_Date__c = system.today()+2;
        orderList[1].Invoice_Date__c = system.today()+3;
        orderList[1].PO_Date__c = system.today()+3;
        update orderList;

        List<Order_Item__c> orderItemList = objTestDataFactory.createTestUOMOrderItem(prodList,orderList[0].id, 'CS');


        List<Conversion__c> convList = [SELECT Id, Product__c FROM Conversion__c];

        Promotion__c promotionObj        = new Promotion__c();
        promotionObj.Custom_Product__c   = convList.get(0).Product__c;
        promotionObj.Promotion_Name__c   = 'Test Promotion';
        promotionObj.Start_Date__c       = Date.today();
        promotionObj.Trigger_Quantity__c = 1;
        promotionObj.Conversion__c       = convList.get(0).Id;
        promotionObj.Valid_Until__c      = Date.today();
        insert promotionObj;

        Bundle__c bundleObj         = new Bundle__c();
        bundleObj.Bundled_Price__c  = 14;
        bundleObj.Custom_Product__c = convList.get(0).Product__c;
        bundleObj.Description__c    = 'Test Description ';
        bundleObj.Promotion__c      = promotionObj.Id;
        bundleObj.Quantity__c       = 18;
        bundleObj.Conversion__c     = convList.get(0).Id;
        insert bundleObj;

        List<Promotion__c> CurrentValidPromotions  = [SELECT id, Custom_Product__c, custom_product__r.name,Trigger_Quantity__c,Conversion__r.Conversion_Rate__c,
                    (SELECT id, Bundled_Price__c, Custom_Product__c, Quantity__c, Conversion__c,
                            Promotion__r.Conversion__r.Conversion_Rate__c, Promotion__r.Trigger_Quantity__c
                    FROM Bundles__r)
            FROM Promotion__c
            WHERE Valid_Until__c >= TODAY];

        for(Order_Item__c objorderItem : orderItemList) {
            objorderItem.Id = null;
            objorderItem.Estimated_Amount__c = 10;
            objorderItem.Order_Quantity__c = 1;
        }
        insert orderItemList;

        System.assertEquals(null, CurrentValidPromotions.size());

    }
}