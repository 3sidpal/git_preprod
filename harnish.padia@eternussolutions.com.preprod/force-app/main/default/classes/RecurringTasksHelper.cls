public class RecurringTasksHelper {

    public static list<RecurringTasksController.AccountTaskWrapper> accountTaskWrapperList;
    public static list<RecurringTasksController.DailyHoursWrapper> dailyHoursWrapperList;

    // Method to create multiple tasks based on selected days
    public static Map<String,List<sObject>> weeklyTaskCreation(List<RecurringTasksController.AccountTaskWrapper> taskWrapperList, String userIdStr, String monthStr, String yearStr) {
        String currBU;
        User currUserRec = [SELECT Id, Business_Unit__c FROM User WHERE Id =: UserInfo.getUserID()];
        if(currUserRec.Business_Unit__c == 'GFS') {
            currBU = 'GFS';
        }
        if(currUserRec.Business_Unit__c == 'Feeds' || currUserRec.Business_Unit__c == 'Poultry') {
            currBU = 'Agro';
        }
        Integer strMonth = getMonthNo(monthStr);
        accountTaskWrapperList = taskWrapperList;
        Date monthStartDate = Date.newInstance(Integer.valueOf(yearStr), strMonth, 01);
        Date monthEndDate = Date.newInstance(Integer.valueOf(yearStr), strMonth, 01).addMonths(1).toStartOfMonth().addDays(-1);
        Integer daysAdded = getWeekStartDay(DateTime.newInstance(monthStartDate, Time.newInstance(0, 0, 0, 0)).format('EEEE').toUpperCase());
        Map<String,List<sObject>> sobjectMap = new Map<String,List<sObject>>();
        List<Task> tasktoCreateList = new List<Task>();

        List<Account_Configuration__c> accConfigtoCreateList = new List<Account_Configuration__c>();

        Task wrapperTask = new Task();
        wrapperTask.Recurring_Task_Identifier__c = EncodingUtil.convertToHex(crypto.generateAesKey(256));

        Account_Configuration__c accConfig = new Account_Configuration__c();
        accConfig.Recurring_Configuration_Identifier__c = EncodingUtil.convertToHex(crypto.generateAesKey(256));

        for(RecurringTasksController.AccountTaskWrapper wrapperObj : accountTaskWrapperList) {
            if((wrapperObj.task.Work_Type != 'Leave' && currBU != 'Agro') || (wrapperObj.task.Work_Type != 'Leave' && currBU == 'Agro' && wrapperObj.task.numberOfAccounts == '')) {
                wrapperTask.Subject = wrapperObj.activity;
                if(wrapperObj.task.accountName != null && wrapperObj.task.accountName != '' && wrapperObj.activity != '') {
                    wrapperTask.Title__c = wrapperObj.task.accountName + '-' +wrapperObj.activity;
                } 
                else if(wrapperObj.account != null && !String.isBlank(wrapperObj.account.Id)) {
                    wrapperTask.Title__c = wrapperObj.account.Name + '-' +wrapperObj.activity;
                }                
                else {
                    wrapperTask.Title__c = wrapperObj.activity;
                }
                wrapperTask.Type_of_Activity__c = wrapperObj.activityType;
                wrapperTask.Related_Account__c = wrapperObj.account.Id;
            }
            else if(wrapperObj.task.Work_Type == 'Field work' && currBU == 'Agro') {
                if(wrapperObj.task.numberOfAccounts != '') {
                    accConfig.Number_of_Accounts__c = Integer.valueOf(wrapperObj.task.numberOfAccounts);
                    accConfig.Title__c = 'Multiple Accounts ' + wrapperObj.task.numberOfAccounts;
                    accConfig.User__c = UserInfo.getUserID();
                    accConfig.Work_Type__c = wrapperObj.task.Work_Type;
                    accConfig.Status__c = 'New';
                    accConfig.Recurring_Task__c = TRUE;
                    accConfig.City__c = wrapperObj.task.taskCityId;
                    accConfig.Province__c = wrapperObj.task.taskProvinceId;
                    accConfig.Account_Name__c = wrapperObj.task.accountName;
                }
                else {
                    if(!String.isBlank(wrapperObj.account.Id)) {
                        wrapperTask.Related_Account__c = wrapperObj.account.Id;
                    }
                    else if(!String.isBlank(wrapperObj.Task.accountName)) {
                        wrapperTask.Account_Name__c = wrapperObj.Task.accountName;
                    }
                    wrapperTask.Subject = wrapperObj.task.accountName;
                    wrapperTask.Title__c = wrapperObj.task.accountName + '-' + wrapperObj.activity;
                }
            }
            else {
                if(wrapperObj.task.leaveType == 'Half Day') {
                    wrapperTask.Leave_Type__c = 'Half Day';
                    wrapperTask.Half_day_leave_type__c = wrapperObj.task.halfDayLeaveType;
                    wrapperTask.Title__c = 'Half Day Leave';
                    wrapperTask.Subject = 'Half Day Leave';
                    wrapperTask.Type_of_Activity__c = 'Half Day Leave';
                }
                else{
                    wrapperTask.Leave_Type__c = 'Full Day';
                    wrapperTask.Title__c = 'Full Day Leave';
                    wrapperTask.Subject = 'Full Day Leave';
                    wrapperTask.Type_of_Activity__c = 'Full Day Leave';

                }
                System.debug('Leave Reason :: '+wrapperObj.Task.LeaveReason);

                wrapperTask.Leave_Reason__c = wrapperObj.Task.LeaveReason;
            }
            wrapperTask.User__c = userIdStr;
            if((wrapperObj.Task.targetRevenue != '' || wrapperObj.Task.targetRevenue != '0') && wrapperObj.Task.targetRevenue != null) {
                wrapperTask.Target_Revenue__c = Integer.valueOf(wrapperObj.Task.targetRevenue);
            }
            if(wrapperObj.task.Work_Type == 'Field work' && currBU != 'Agro') {
                if(String.isBlank(wrapperObj.account.Id) && String.isBlank(wrapperObj.Task.accountName)) {
                    wrapperTask.Title__c = wrapperObj.activity;
                }
                else if(!String.isBlank(wrapperObj.account.Id)) {
                    wrapperTask.Related_Account__c = wrapperObj.account.Id;
                    wrapperTask.Title__c = wrapperObj.Account.Name +' - '+wrapperObj.activity;
                }
                else if(!String.isBlank(wrapperObj.Task.accountName)) {
                    wrapperTask.Account_Name__c = wrapperObj.Task.accountName;
                    wrapperTask.Title__c = wrapperObj.Task.accountName +' - '+wrapperObj.activity;
                }

                if((wrapperObj.Task.taskCustomerGroup).toUpperCase() == 'OTHERS') {
                    wrapperTask.Title__c = wrapperObj.activity;
                }
            }

            wrapperTask.Work_Type__c = wrapperObj.task.Work_Type;
            wrapperTask.Description = wrapperObj.task.Description;
            wrapperTask.City__c = wrapperObj.task.taskCityId;
            wrapperTask.Province__c = wrapperObj.task.taskProvinceId;
            wrapperTask.Customer_Group__c = wrapperObj.task.taskCustomerGroupId;

            if(wrapperObj.account.Week_1_Route != null && wrapperObj.account.Week_1_Route != '}') {

                List<String> days = wrapperObj.account.Week_1_Route.split(';');
                if(wrapperObj.task.Work_Type == 'Leave') {
                    wrapperObj.week1StartTime = wrapperObj.leaveStartTime;
                    wrapperObj.week1EndTime = wrapperObj.leaveEndTime;
                }
                else{
                    wrapperObj.week1StartTime = wrapperObj.week1StartTime;
                    wrapperObj.week1EndTime = wrapperObj.week1EndTime;
                }

                for(Date d = monthStartDate; d < monthStartDate.addDays(daysAdded); d = d.addDays(1)) {
                    if(wrapperObj.task.numberOfAccounts != '' && wrapperObj.task.numberOfAccounts != '0' && wrapperObj.task.numberOfAccounts != null) {
                        Account_Configuration__c accConfigObj = multipleConfigurations(days, d, wrapperObj.week1StartTime, wrapperObj.week1EndTime, Integer.valueOf(wrapperObj.task.numberOfAccounts));
                        if(accConfigObj != null) {
                            accConfigObj = configPopulate(accConfigObj, accConfig);
                            accConfigtoCreateList.add(accConfigObj);
                        }
                    }
                    else {
                        Task taskObj = multipleTasks(days, d, wrapperObj.week1StartTime, wrapperObj.week1EndTime);

                        if(taskObj != null) {
                            taskObj = taskPopulate(taskObj, wrapperTask);
                            tasktoCreateList.add(taskObj);
                        }
                    }

                }

            }
            if(wrapperObj.account.Week_2_Route != null  && wrapperObj.account.Week_2_Route != '}') {
                List<String> days = wrapperObj.account.Week_2_Route.split(';');
                if(wrapperObj.task.Work_Type == 'Leave') {
                    wrapperObj.week2StartTime = wrapperObj.leaveStartTime;
                    wrapperObj.week2EndTime = wrapperObj.leaveEndTime;
                }
                else{
                    wrapperObj.week2StartTime = wrapperObj.week2StartTime;
                    wrapperObj.week2EndTime = wrapperObj.week2EndTime;
                }
                for(Date d = monthStartDate.addDays(daysAdded); d < monthStartDate.addDays(daysAdded + 7); d = d.addDays(1)) {
                    if(wrapperObj.task.numberOfAccounts != '' && wrapperObj.task.numberOfAccounts != '0' && wrapperObj.task.numberOfAccounts != null) {
                        Account_Configuration__c accConfigObj = multipleConfigurations(days, d, wrapperObj.week2StartTime, wrapperObj.week2EndTime, Integer.valueOf(wrapperObj.task.numberOfAccounts));
                        if(accConfigObj != null) {
                            accConfigObj = configPopulate(accConfigObj, accConfig);
                            accConfigtoCreateList.add(accConfigObj);
                        }
                    }
                    else {
                        Task taskObj = multipleTasks(days, d, wrapperObj.week2StartTime, wrapperObj.week2EndTime);
                        if(taskObj != null) {
                            taskObj = taskPopulate(taskObj, wrapperTask);
                            tasktoCreateList.add(taskObj);
                        }
                    }
                }
            }
            if(wrapperObj.account.Week_3_Route != null && wrapperObj.account.Week_3_Route != '}') {

                List<String> days = wrapperObj.account.Week_3_Route.split(';');
                if(wrapperObj.task.Work_Type == 'Leave') {
                    wrapperObj.week3StartTime = wrapperObj.leaveStartTime;
                    wrapperObj.week3EndTime = wrapperObj.leaveEndTime;
                }
                else{
                    wrapperObj.week3StartTime = wrapperObj.week3StartTime;
                    wrapperObj.week3EndTime = wrapperObj.week3EndTime;
                }
                for(Date d = monthStartDate.addDays(daysAdded + 7); d < monthStartDate.addDays(daysAdded + 14); d = d.addDays(1)) {
                    if(wrapperObj.task.numberOfAccounts != '' && wrapperObj.task.numberOfAccounts != '0' && wrapperObj.task.numberOfAccounts != null) {
                        Account_Configuration__c accConfigObj = multipleConfigurations(days, d, wrapperObj.week3StartTime, wrapperObj.week3EndTime, Integer.valueOf(wrapperObj.task.numberOfAccounts));
                        if(accConfigObj != null) {
                            accConfigObj = configPopulate(accConfigObj, accConfig);
                            accConfigtoCreateList.add(accConfigObj);
                        }
                    }
                    else {
                        Task taskObj = multipleTasks(days, d, wrapperObj.week3StartTime, wrapperObj.week3EndTime);

                        if(taskObj != null) {
                            taskObj = taskPopulate(taskObj, wrapperTask);
                            tasktoCreateList.add(taskObj);
                        }
                    }
                }
            }
            if(wrapperObj.account.Week_4_Route != null && wrapperObj.account.Week_4_Route != '}') {

                List<String> days = wrapperObj.account.Week_4_Route.split(';');
                Date monthEnd;
                if(wrapperObj.task.Work_Type == 'Leave') {
                    wrapperObj.week4StartTime = wrapperObj.leaveStartTime;
                    wrapperObj.week4EndTime = wrapperObj.leaveEndTime;
                }
                else{
                    wrapperObj.week4StartTime = wrapperObj.week4StartTime;
                    wrapperObj.week4EndTime = wrapperObj.week4EndTime;
                }
                for(Date d = monthStartDate.addDays(daysAdded + 14); d < monthStartDate.addDays(daysAdded + 21); d = d.addDays(1)) {
                    if(wrapperObj.task.numberOfAccounts != '' && wrapperObj.task.numberOfAccounts != '0' && wrapperObj.task.numberOfAccounts != null) {
                        Account_Configuration__c accConfigObj = multipleConfigurations(days, d, wrapperObj.week4StartTime, wrapperObj.week4EndTime, Integer.valueOf(wrapperObj.task.numberOfAccounts));
                        if(accConfigObj != null) {
                            accConfigObj = configPopulate(accConfigObj, accConfig);
                            accConfigtoCreateList.add(accConfigObj);
                        }
                    }
                    else {
                        Task taskObj = multipleTasks(days, d, wrapperObj.week4StartTime, wrapperObj.week4EndTime);
                        if(taskObj != null) {
                            taskObj = taskPopulate(taskObj, wrapperTask);
                            tasktoCreateList.add(taskObj);
                        }
                    }
                }
            }

            if(wrapperObj.account.Week_5_Route != null && wrapperObj.account.Week_5_Route != '}') {

                List<String> days = wrapperObj.account.Week_5_Route.split(';');
                if(wrapperObj.task.Work_Type == 'Leave') {
                    wrapperObj.week5StartTime = wrapperObj.leaveStartTime;
                    wrapperObj.week5EndTime = wrapperObj.leaveEndTime;
                }
                else{
                    wrapperObj.week5StartTime = wrapperObj.week5StartTime;
                    wrapperObj.week5EndTime = wrapperObj.week5EndTime;
                }
                for(Date d = monthStartDate.addDays(daysAdded + 21); d < monthStartDate.addDays(daysAdded + 28); d = d.addDays(1)) {
                    if(wrapperObj.task.numberOfAccounts != '' && wrapperObj.task.numberOfAccounts != '0' && wrapperObj.task.numberOfAccounts != null) {
                        Account_Configuration__c accConfigObj = multipleConfigurations(days, d, wrapperObj.week5StartTime, wrapperObj.week5EndTime, Integer.valueOf(wrapperObj.task.numberOfAccounts));
                        if(accConfigObj != null) {
                            accConfigObj = configPopulate(accConfigObj, accConfig);
                            accConfigtoCreateList.add(accConfigObj);
                        }
                    }
                    else {
                        Task taskObj = multipleTasks(days, d, wrapperObj.week5StartTime, wrapperObj.week5EndTime);
                        if(taskObj != null) {
                            taskObj = taskPopulate(taskObj, wrapperTask);
                            tasktoCreateList.add(taskObj);
                        }
                    }
                }
            }

            if(wrapperObj.account.Week_6_Route != null && wrapperObj.account.Week_6_Route != '}') {

                List<String> days = wrapperObj.account.Week_6_Route.split(';');
                if(wrapperObj.task.Work_Type == 'Leave') {
                    wrapperObj.week6StartTime = wrapperObj.leaveStartTime;
                    wrapperObj.week6EndTime = wrapperObj.leaveEndTime;
                }
                else{
                    wrapperObj.week6StartTime = wrapperObj.week6StartTime;
                    wrapperObj.week6EndTime = wrapperObj.week6EndTime;
                }
                for(Date d = monthStartDate.addDays(daysAdded + 28); d < monthStartDate.addDays(daysAdded + 35); d = d.addDays(1)) {
                    if(wrapperObj.task.numberOfAccounts != '' && wrapperObj.task.numberOfAccounts != '0' && wrapperObj.task.numberOfAccounts != null) {
                        Account_Configuration__c accConfigObj = multipleConfigurations(days, d, wrapperObj.week6StartTime, wrapperObj.week6EndTime, Integer.valueOf(wrapperObj.task.numberOfAccounts));
                        if(accConfigObj != null) {
                            accConfigObj = configPopulate(accConfigObj, accConfig);
                            accConfigtoCreateList.add(accConfigObj);
                        }
                    }
                    else {
                        Task taskObj = multipleTasks(days, d, wrapperObj.week6StartTime, wrapperObj.week6EndTime);
                        if(taskObj != null) {
                            taskObj = taskPopulate(taskObj, wrapperTask);
                            tasktoCreateList.add(taskObj);
                        }
                    }
                }
            }
        }
        if(accConfigtoCreateList != null && !accConfigtoCreateList.isEmpty()) {
            //insert accConfigtoCreateList;
        }
        sobjectMap.put('Task',tasktoCreateList);
        sobjectMap.put('Account Configuration',accConfigtoCreateList);
        return sobjectMap;

    }

    // Task record field population
    public static Account_Configuration__c configPopulate(Account_Configuration__c configObj, Account_Configuration__c accConfig) {
        configObj.Name = accConfig.Title__c;
        configObj.Recurring_Configuration_Identifier__c = accConfig.Recurring_Configuration_Identifier__c;
        configObj.Subject__c = accConfig.Subject__c;
        configObj.Status__c = 'New';
        configObj.Work_Type__c = accConfig.Work_Type__c;
        configObj.User__c = accConfig.User__c;
        configObj.Account_Name__c = accConfig.Title__c;
        configObj.City__c = accConfig.City__c;
        configObj.Province__c = accConfig.Province__c;
        configObj.Recurring_Task__c = true;
        configObj.Title__c = accConfig.Title__c;
        return configObj;
    }

    public static Account_Configuration__c multipleConfigurations(List<String> days, Date d, Time weekStartTime, Time weekEndTime, Integer numberOfAccounts) {
        Account_Configuration__c accConfigObj;
        if(weekStartTime != null && weekEndTime != null) {

            for(String day : days) {
                Datetime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
                if(dt.format('EEEE').toUpperCase() == day.replace('{','').replace('}','').toUpperCase()) {
                    accConfigObj = new Account_Configuration__c();
                    DateTime tempSDT = DateTime.newInstanceGMT(System.today(),weekStartTime);
                    DateTime tempEDT = DateTime.newInstanceGMT(System.today(),weekEndTime);
                    accConfigObj.StartDateTime__c = DateTime.newInstance(d, tempSDT.time());
                    accConfigObj.EndDateTime__c = DateTime.newInstance(d, tempEDT.time());
                    accConfigObj.Number_of_Accounts__c = numberOfAccounts;
                }
            }
        }

        if(accConfigObj != null) {
            return accConfigObj;
        }
        else {
            return null;
        }
    }

    // Task record field population
    public static Task taskPopulate(Task taskObj, Task wrapperTask) {
        taskobj.Recurring_Task_Identifier__c = wrapperTask.Recurring_Task_Identifier__c;
        taskObj.Subject = wrapperTask.Subject;
        taskObj.Status = 'Not Submitted';
        taskObj.Work_Type__c = wrapperTask.Work_Type__c;
        taskObj.User__c = wrapperTask.User__c;
        if(wrapperTask.Related_Account__c != null) {
            taskObj.Related_Account__c = wrapperTask.Related_Account__c;
        }
        else {
            taskObj.Account_Name__c = wrapperTask.Account_Name__c;
        }
        taskObj.Type_of_Activity__c = wrapperTask.Type_of_Activity__c;
        taskObj.Description = wrapperTask.Description;
        taskObj.City__c = wrapperTask.City__c;
        taskObj.Province__c = wrapperTask.Province__c;
        taskObj.Customer_Group__c = wrapperTask.Customer_Group__c;
        if(wrapperTask.Work_Type__c == 'Field work' && wrapperTask.Related_Account__c != null && wrapperTask.Related_Account__c != '') {
            taskObj.WhatId = wrapperTask.Related_Account__c;
        }
        if(wrapperTask.Leave_Type__c == 'Half Day') {
            taskObj.Leave_Type__c = 'Half Day';
            taskObj.Half_day_leave_type__c = wrapperTask.Half_day_leave_type__c;
        }
        else {
            taskObj.Leave_Type__c = 'Full Day';
        }
        taskObj.Recurring_Task__c = true;
        taskObj.Title__c = wrapperTask.Title__c;
        taskObj.Leave_Reason__c = wrapperTask.Leave_Reason__c;
        return taskObj;
    }

    // Method to add startTime and endTime to task
    public static Task  multipleTasks(List<String> days, Date d, Time weekStartTime, Time weekEndTime) {

        Task taskObj;
        if(weekStartTime != null && weekEndTime != null) {

            for(String day : days) {
                Datetime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
                if(dt.format('EEEE').toUpperCase() == day.replace('{','').replace('}','').toUpperCase()) {
                    taskObj = new Task();
                    DateTime tempSDT = DateTime.newInstanceGMT(System.today(),weekStartTime);
                    DateTime tempEDT = DateTime.newInstanceGMT(System.today(),weekEndTime);
                    taskObj.StartDateTime__c = DateTime.newInstance(d, tempSDT.time());
                    taskObj.EndDateTime__c = DateTime.newInstance(d, tempEDT.time());
                }
            }
        }

        if(taskObj != null) {
            return taskObj;
        }
        else {
            return null;
        }

    }



    public static list<RecurringTasksController.DailyHoursWrapper> dailyHoursInMonth(Integer daysInMonth,Integer year, String monthStr, Map<Date,Map<String,List<Decimal>>> taskHoursMap) {
        Integer month = getMonthNo(monthStr);
        dailyHoursWrapperList = new list<RecurringTasksController.DailyHoursWrapper>();
        for (Integer day = 1; day <= daysInMonth; day++) {

            RecurringTasksController.DailyHoursWrapper dailyHoursWrapperObj = new RecurringTasksController.DailyHoursWrapper();
            if(taskHoursMap.containsKey(Date.newInstance(year, month, day))) {
                Map<String,List<Decimal>> worktypeHoursMap = taskHoursMap.get(Date.newInstance(year, month, day));
                List<Decimal> wthourList = new List<Decimal>();
                for(String workType : worktypeHoursMap.keySet()) {
                    wthourList.addAll(worktypeHoursMap.get(workType));
                }
                List<Decimal> hourList = wthourList;
                Decimal hourValue = 0;

                for(Decimal hour : hourList) {
                    if(hour != null)
                    hourValue += hour;
                }

                dailyHoursWrapperObj.taskDate = String.valueOf(DateTime.newInstance(year, month, day).format('d'));
                dailyHoursWrapperObj.taskDay = String.valueOf(DateTime.newInstance(year, month, day).format('EEEE'));
                dailyHoursWrapperObj.taskHours = hourValue;
                dailyHoursWrapperList.add(dailyHoursWrapperObj);
            }
            else {
                dailyHoursWrapperObj.taskDate = String.valueOf(DateTime.newInstance(year, month, day).format('d'));
                dailyHoursWrapperObj.taskDay = String.valueOf(DateTime.newInstance(year, month, day).format('EEEE'));
                dailyHoursWrapperObj.taskHours = 0;
                dailyHoursWrapperList.add(dailyHoursWrapperObj);
            }
        }
        return dailyHoursWrapperList;
    }

    // Method to get the number of days to start new week
    public static Integer getWeekStartDay(String weekDay) {

        Integer addDays = 0;

        switch on weekDay {
            when 'SUNDAY' {
                addDays = 7;
            }
            when 'MONDAY' {
                addDays = 6;
            }
            when 'TUESDAY' {
                addDays = 5;
            }
            when 'WEDNESDAY' {
                addDays = 4;
            }
            when 'THURSDAY' {
                addDays = 3;
            }
            when 'FRIDAY' {
                addDays = 2;
            }
            when 'SATURDAY' {
                addDays = 1;
            }
            when else {
                addDays = 1;
            }
        }

        return addDays;

    }

    // Method to get the number of days to start new week
    public static Integer getMonthNo(String monthName) {

        Integer addDays = 0;

        switch on monthName {
            when 'January' {
                addDays = 1;
            }
            when 'February' {
                addDays = 2;
            }
            when 'March' {
                addDays = 3;
            }
            when 'April' {
                addDays = 4;
            }
            when 'May' {
                addDays = 5;
            }
            when 'June' {
                addDays = 6;
            }
            when 'July' {
                addDays = 7;
            }
            when 'August' {
                addDays = 8;
            }
            when 'September' {
                addDays = 9;
            }
            when 'October' {
                addDays = 10;
            }
            when 'November' {
                addDays = 11;
            }
            when 'December' {
                addDays = 12;
            }
            when else {
                addDays = 1;
            }
        }

        return addDays;

    }
}