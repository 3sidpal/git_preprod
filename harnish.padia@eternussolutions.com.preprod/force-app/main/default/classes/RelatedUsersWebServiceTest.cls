@isTest
private class RelatedUsersWebServiceTest {

	private static testMethod void testDoGetNegative() {
       RestRequest req = new RestRequest(); 
       RestResponse res = new RestResponse();
             
       req.requestURI = '/services/apexrest/getSubordinates';  //Request URL
       req.httpMethod = 'GET';//HTTP Request Type
       RestContext.request = req;
       RestContext.response= res;
       RelatedUsersWebService.doGet();
	}
	
	private static testMethod void testDoGetpPositive() {
            User objUser = [SELECT Id FROM User LIMIT 1];
        //for(User objUser : [SELECT Id FROM User LIMIT 100]){
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/getSubordinates/'+objUser.Id;  //Request URL
            req.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = req;
            RestContext.response= res;
            RelatedUsersWebService.doGet();
        //}
	}
	
	private static testMethod void testDoGetpPositiveTwo() {
	    Set<String> setMgrRoles = new Set<String>();
	    for(Manager_Role__mdt objMgrRol : [SELECT DeveloperName FROM Manager_Role__mdt]){
	        setMgrRoles.add(objMgrRol.DeveloperName);
	    }
	    User objUser = [SELECT Id 
	                        FROM User 
	                        WHERE UserRole.Name IN :setMgrRoles
	                        LIMIT 1 ];
            RestRequest req = new RestRequest(); 
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/getSubordinates/'+objUser.Id;  //Request URL
            req.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = req;
            RestContext.response= res;
            RelatedUsersWebService.doGet();
	}
}