public abstract class SMC_TestFactoryData
{
    // User Details 
    /*
    public static User businessUser(String alias, String emailAddress, String firstName, String lastName, Id profileId, String userTitle)
    {
        String uniqueUserName      = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        return new User(Alias             = alias, 
                        Email             = emailAddress,
                        EmailEncodingKey  = 'UTF-8', 
                        FirstName         = firstName, 
                        LastName          = lastName,
                        LanguageLocaleKey = 'en_US',
                        LocaleSidKey      = 'en_US', 
                        ProfileId         = profileId,
                        TimeZoneSidKey    = 'America/Los_Angeles',
                        title             = userTitle,
                        UserName          = uniqueUserName);
    }
    */
    
    // Creation of Business Unit
    public static List<Market__c> createBusinessUnit(String businessUnitName)
    {
        List<Market__c> newBU = new List<Market__c>();
        Market__c businessUnit = new Market__c(Name      = businessUnitName,
                                               Active__c = true);
       newBU.add(businessUnit);
       return newBU;
    }

    // Creation of Customer Group
    public static List<Chains__c> createCustomerGroup()
    {
        List<Chains__c> newChains = new List<Chains__c>();
        Chains__c customerGroup = new Chains__c(Name = '909');

        newChains.add(customerGroup);
        return newChains;
    }

    // Creation of Customer Group Level Budget with ASM Control
    public static List<ePAW_Budget2__c> createBudgetCGLevelControl(Id businessUnitId, Id prodCatId, Id chainsId, Id majorActivityId, Id asmUser)
    {
        List<ePAW_Budget2__c> newBudget = new List<ePAW_Budget2__c>();

        ePAW_Budget2__c budget = new ePAW_Budget2__c(Name                    = 'TEST BUDGET',
                                                    Business_Unit__c         = businessUnitId, // For Cross Reference: Business_Unit__c == Market__c
                                                    Product_Category__c      = prodCatId,
                                                    Customer_Group__c        = chainsId,
                                                    Major_Activity__c        = majorActivityId,
                                                    Area__c                  = 'NATIONAL',
                                                    Budget_Type__c           = 'PAW Form',
                                                    ASM_User__c              = asmUser,
                                                    Account_Charge__c        = 'Sales and Distribution Budget',
                                                    Initial_Budget_Amount__c = 1000000,
                                                    Budget_Floating__c       = 1000000,
                                                    Remaining_Budget__c      = 1000000,
                                                    Start_Date__c            = date.newInstance(2020, 1, 17),
                                                    End_Date__c              = date.newInstance(2020, 8, 17));
        newBudget.add(budget);
        return newBudget;
    }

    // Creation of Account (Distributor / Direct)
    public static List<Account> createDistributorDirectAccount(Id businessUnitId)
    {
        List<Account> newAccount = new List<Account>();
        Id RecordTypeIdAccount   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        Account accountDistributorDirect = new Account(Name          = 'TEST DISTRIBUTOR ACCOUNT',
                                                       Branded__c    = true,
                                                       Active__c     = true,
                                                       Area__c       = 'VISAYAS',
                                                       AccountNumber = '1234',
                                                       Market__c     = businessUnitId,
                                                       RecordTypeId  = RecordTypeIdAccount);
       newAccount.add(accountDistributorDirect);
       return newAccount;
    }
    
    // Creation of Product Category
    public static List<Category__c> createProductCategory(String categoryName)
    {
        List<Category__c> newProdCat = new List<Category__c>();
        Category__c productCategory  = new Category__c(Name = categoryName);
        newProdCat.add(productCategory);
        return newProdCat;
    }
    
    // Creation of Major Activity
    public static List<Activity_ePAW__c> createMajorActivity()
    {
        List<Activity_ePAW__c> newMajorActivity = new List<Activity_ePAW__c>();
        Activity_ePAW__c majorActivity = new Activity_ePAW__c(Name = 'DISPLAY ALLOWANCE (DA)');
        newMajorActivity.add(majorActivity);
        return newMajorActivity;
    }

    // Creation of Budget with ASM Control
    public static List<ePAW_Budget2__c> createBudgetControl(Id businessUnitId, Id prodCatId, Id accountId, Id majorActivityId, Id asmUser)
    {
        List<ePAW_Budget2__c> newBudget = new List<ePAW_Budget2__c>();

        ePAW_Budget2__c budget = new ePAW_Budget2__c(Name                    = 'TEST BUDGET',
                                                    Business_Unit__c         = businessUnitId, // For Cross Reference: Business_Unit__c == Market__c
                                                    Product_Category__c      = prodCatId,
                                                    Account__c               = accountId,
                                                    Major_Activity__c        = majorActivityId,
                                                    Area__c                  = 'VISAYAS',
                                                    ASM_User__c              = asmUser,
                                                    Budget_Type__c           = 'PAW Form',
                                                    Account_Charge__c        = 'Sales and Distribution Budget',
                                                    Initial_Budget_Amount__c = 1000000,
                                                    Budget_Floating__c       = 1000000,
                                                    Remaining_Budget__c      = 1000000,
                                                    Start_Date__c            = date.newInstance(2020, 1, 17),
                                                    End_Date__c              = date.newInstance(2020, 8, 17));
        newBudget.add(budget);
        return newBudget;
    }

    public static List<ePAW_Budget2__c> createBudgetControlMIC(Id businessUnitId, Id prodCatId, Id accountId, Id majorActivityId, Id asmUser)
    {
        List<ePAW_Budget2__c> newBudget = new List<ePAW_Budget2__c>();

        ePAW_Budget2__c budget = new ePAW_Budget2__c(Name                    = 'TEST BUDGET',
                                                    Business_Unit__c         = businessUnitId, // For Cross Reference: Business_Unit__c == Market__c
                                                    Product_Category__c      = prodCatId,
                                                    Account__c               = accountId,
                                                    Major_Activity__c        = majorActivityId,
                                                    Area__c                  = 'VISAYAS',
                                                    ASM_User__c              = asmUser,
                                                    Budget_Type__c           = 'PAW Form',
                                                    Account_Charge__c        = 'Sales and Distribution Budget',
                                                    Initial_Budget_Amount__c = 1000000,
                                                    Budget_Floating__c       = 1000000,
                                                    Remaining_Budget__c      = 1000000,
                                                    Start_Date__c            = date.newInstance(2020, 1, 17),
                                                    Sales_User_Profile_Applicability__c = 'MIC Direct',
                                                    End_Date__c              = date.newInstance(2020, 8, 17));
        newBudget.add(budget);
        return newBudget;
    }

    // Creation of Budget with ASM Control
    public static List<ePAW_Budget2__c> createAccountLevelBudgetControl(Id businessUnitId, Id prodCatId, Id accountId, Id majorActivityId, Id asmUser)
    {
        List<ePAW_Budget2__c> newBudget = new List<ePAW_Budget2__c>();

        ePAW_Budget2__c budget = new ePAW_Budget2__c(Name                    = 'TEST BUDGET',
                                                    Business_Unit__c         = businessUnitId, // For Cross Reference: Business_Unit__c == Market__c
                                                    Product_Category__c      = prodCatId,
                                                    Account__c               = accountId,
                                                    Major_Activity__c        = majorActivityId,
                                                    Area__c                  = 'VISAYAS',
                                                    ASM_User__c              = asmUser,
                                                    Budget_Type__c           = 'PAW Form',
                                                    Account_Charge__c        = 'Sales and Distribution Budget',
                                                    Initial_Budget_Amount__c = 1000000,
                                                    Budget_Floating__c       = 1000000,
                                                    Remaining_Budget__c      = 1000000,
                                                    Start_Date__c            = date.newInstance(2020, 1, 17),
                                                    End_Date__c              = date.newInstance(2020, 8, 17));
        newBudget.add(budget);
        return newBudget;
    }

    // Creation of ePAW Form
    public static List<ePAW_Form__c> createEpawForm(Id accountId)
    {
        List<ePAW_Form__c> newForm = new List<ePAW_Form__c>();
        ePAW_Form__c ePawForm = new ePAW_Form__c(BU_Picklist__c               = 'MAGNOLIA',
                                                 Category_Picklist__c         = 'ICE CREAM',
                                                 Budget_Name__c               = 'TesT BuDGEt',
                                                 Account_Charge__c            = 'Sales and Distribution Budget',
                                                 Major_Activity_Picklist__c   = 'DISPLAY ALLOWANCE (DA)',
                                                 Type_of_Activity_Picklist__c = 'DISPLAY ALLOWANCE',
                                                 Account__c                   = accountId,
                                                 Effectivity_Date_To__c       = date.newInstance(2020, 5, 17),
                                                 Effectivity_Date_From__c     = date.newInstance(2020, 2, 17));
        newForm.add(ePAWForm);
        return newForm;
    }
    
    // Creation of Mother ePAW Form
    public static List<ePAW_Form__c> createMotherEpawForm(Id accountId, Decimal RTAMother, Decimal RTAChild)
    {
        List<ePAW_Form__c> newForm = new List<ePAW_Form__c>();
        ePAW_Form__c ePawForm = new ePAW_Form__c(BU_Picklist__c               = 'MAGNOLIA',
                                                 Category_Picklist__c         = 'ICE CREAM',
                                                 Budget_Name__c               = 'TesT BuDGEt',
                                                 Account_Charge__c            = 'Sales and Distribution Budget',
                                                 Major_Activity_Picklist__c   = 'DISPLAY ALLOWANCE (DA)',
                                                 Type_of_Activity_Picklist__c = 'DISPLAY ALLOWANCE',
                                                 Parent_Requisition__c        = true,
                                                 Reporting_Total_Amount_Mother__c = RTAMother,
                                                 Reporting_Total_Amount_Child__c  = RTAChild,
                                                 Account__c                   = accountId,
                                                 Effectivity_Date_To__c       = date.newInstance(2020, 5, 17),
                                                 Effectivity_Date_From__c     = date.newInstance(2020, 2, 17));
        newForm.add(ePAWForm);
        return newForm;
    }
    
    // Creation of Child ePAW Form
    public static List<ePAW_Form__c> createChildEpawForm(Id accountId, Decimal RTAChild, String MotherReference, String BudgetName)
    {
        List<ePAW_Form__c> newForm = new List<ePAW_Form__c>();
        ePAW_Form__c ePawForm = new ePAW_Form__c(BU_Picklist__c               = 'MAGNOLIA',
                                                 Category_Picklist__c         = 'ICE CREAM',
                                                 Budget_Name__c               = BudgetName,
                                                 Account_Charge__c            = 'Sales and Distribution Budget',
                                                 Major_Activity_Picklist__c   = 'DISPLAY ALLOWANCE (DA)',
                                                 Type_of_Activity_Picklist__c = 'DISPLAY ALLOWANCE',
                                                 Parent_Requisition__c        = false,
                                                 Child_Requisition__c         = true,
                                                 Account__c                   = accountId,
                                                 Reporting_Total_Amount_Child__c = RTAChild,
                                                 Mother_ePAW_Reference__c     = MotherReference,
                                                 Effectivity_Date_To__c       = date.newInstance(2020, 5, 17),
                                                 Effectivity_Date_From__c     = date.newInstance(2020, 2, 17));
        newForm.add(ePAWForm);
        return newForm;
    }

    // Creation of ePAW Form Chains
    public static List<ePAW_Form__c> createEpawFormCG(Id customerGroupId)
    {
        List<ePAW_Form__c> newForm = new List<ePAW_Form__c>();
        ePAW_Form__c ePawForm = new ePAW_Form__c(BU_Picklist__c               = 'MAGNOLIA',
                                                 Category_Picklist__c         = 'ICE CREAM',
                                                 Budget_Name__c               = 'TesT BuDGEt',
                                                 Account_Charge__c            = 'Sales and Distribution Budget',
                                                 Major_Activity_Picklist__c   = 'DISPLAY ALLOWANCE (DA)',
                                                 Type_of_Activity_Picklist__c = 'DISPLAY ALLOWANCE',
                                                 Customer_Group__c            = customerGroupId,
                                                 Effectivity_Date_To__c       = date.newInstance(2020, 5, 17),
                                                 Effectivity_Date_From__c     = date.newInstance(2020, 2, 17));
        newForm.add(ePAWForm);
        return newForm;
    }
    
    /*
    public static List<Custom_Product_Group__c> createProductGroupPH3()
    {
        List<Custom_Product_Group__c> productGroup = new List<Custom_Product_Group__c>();
        Custom_Product_Group__c ph3 = new Custom_Product_Group__c(Name = 'FROZEN DELIGHTS (TRADED)');
        productGroup.add(ph3);
        return productGroup;
    }
    
    public static List<Brand__c> createBrandPH4()
    {
        List<Brand__c> brand = new List<Brand__c>();
        Brand__c ph4 = new Brand__c(Name = 'POP-A-CUP');
        brand.add(ph4);
        return brand;
    }
    
    public static List<Product_Subgroup__c> createProductSubgroupPH5()
    {
        List<Product_Subgroup__c> productSubgroup = new List<Product_Subgroup__c>();
        Product_Subgroup__c ph5 = new Product_Subgroup__c(Name = 'POP-A-CUP CHOCOLATE');
        productSubgroup.add(ph5);
        return productSubgroup;
    }
    
    public static List<Scale__c> createScalePH6()
    {
        List<Scale__c> scale = new List<Scale__c>();
        Scale__c ph6 = new Scale__c(Name = '150 ML');
        scale.add(ph6);
        return scale;
    }
    
    public static List<Custom_Product__c> customProduct(String productName, Id ph1, Id ph2, Id ph3, Id ph4, Id ph5, Id ph6, String ph7)
    {
        List<Custom_Product__c> generalize = new List<Custom_Product__c>();
        Custom_Product__c hierarchy = new Custom_Product__c(Name=productName, 
                                                            SKU_Code__c=ph7,
                                                            Market__c=ph1,
                                                            Category1__c=ph2,
                                                            Product_Group__c=ph3,
                                                            Brand__c=ph4,
                                                            Product_Subgroup__c=ph5,
                                                            Scale__c=ph6);
        generalize.add(hierarchy);
        return generalize;
    }
    */
}