@isTest
private class ServiceLevelSurveyPageControllerTest{
    @isTest
    static void testGetProductList() {
        Service_Level__c obj = new Service_Level__c();
        PageReference pageRef = Page.ServiceLevelSurveyPage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(obj);
        ServiceLevelSurveyPageController objController = new ServiceLevelSurveyPageController(sc);
        List<String> productList = new List<String>();
        Test.startTest();
            productList = objController.getProductList();
        Test.stopTest();
        System.assert(false == productList.isEmpty());
    }

    @isTest
    static void testGetExistingServiceLevel() {
        Id serviceId = DIL_TestDataFactory.getServiceLevel(1,true)[0].Id;
        DIL_TestDataFactory.getServiceLevelStandardDetails(serviceId, true);
        DIL_TestDataFactory.getProductDetailList(serviceId, true);

        Service_Level__c objService;
        Map<String, Service_Level_Standards_Details__c> stadardDetailsMap;
        ServiceLevelSurveyPageController.ProductDetailsWrapper wrapper;
        Test.startTest();
            objService = ServiceLevelSurveyPageController.getExistingServiceLevel(serviceId);
            wrapper = ServiceLevelSurveyPageController.fetchRelatedProductDetails(serviceId);
            stadardDetailsMap = ServiceLevelSurveyPageController.fetchRelatedStandardDetails(serviceId);
        Test.stopTest();
        System.assertNotEquals(new Service_Level__c(), objService);
        System.assertEquals(2, stadardDetailsMap.size());
        System.assertEquals(1, wrapper.productQuality.size());
        System.assertEquals(1, wrapper.displayQuality.size());
        System.assertEquals(1, wrapper.availablity.size());
    }

    @isTest
    static void testNegativegetExistingServiceLevel() {
        Service_Level__c objService;
        Test.startTest();
            objService = ServiceLevelSurveyPageController.getExistingServiceLevel(null);
        Test.stopTest();
        System.assertEquals(new Service_Level__c(), objService);
    }
}