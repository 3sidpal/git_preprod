public class TaskStateHandlerBatch implements Database.Batchable<sobject> {
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT VersionData,ContentDocumentId,Title FROM ContentVersion WHERE Title LIKE \'00T%\' ';
        
        Date lastThursday = Date.today().addDays(-4);
        Set<Id> setTaskIds = new Set<Id>();
        for(Task objTask : [Select Id From Task Where (Status = 'Edited After Approval' OR Status = 'Submitted' ) And LastModifiedDate = :lastThursday]){
            setTaskIds.add(objTask.Id);
        }
        Set<Id> setConDocIds = new Set<Id>();
        if(!setTaskIds.isempty()){
            for(ContentDocumentLink objCDL : [SELECT ContentDocumentId,LinkedEntityId,ContentDocument.LatestPublishedVersionId FROM ContentDocumentLink WHERE LinkedEntityId IN :setTaskIds AND LinkedEntity.type = 'Task']){
                setConDocIds.add(objCDL.ContentDocumentId);
            }
        }
        if(!setConDocIds.isEmpty()){
            query += ' AND ContentDocumentId In :setConDocIds';
        }
        
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<ContentVersion> lstConVer){
         
         List<Task> lstTasksToRevert = new List<Task>();
         Set<Id> setFilesToDeleteIds = new Set<Id>();
         Set<Id> setMonthIds = new Set<Id>();
         
         for(ContentVersion objCV : lstConVer){
             try{
                Task earlierApprovedTask = (Task)Json.deserialize(objCV.VersionData.toString(), Task.class);
                lstTasksToRevert.add(earlierApprovedTask); 
                setMonthIds.add(earlierApprovedTask.Month__c);
             }
             
             catch(Exception e){
                
             }
             
             setFilesToDeleteIds.add(objCV.ContentDocumentId);
         }
         
         Map<Id,Task> maptaskIdToTask = new Map<Id,Task>();
         
         for( Task objTask : lstTasksToRevert){
             maptaskIdToTask.put(objTask.Id,objTask);
         }
         
         lstTasksToRevert.clear();
         lstTasksToRevert.addAll(maptaskIdToTask.values());
         
         update lstTasksToRevert;
         delete [Select Id From ContentDocument Where Id IN :setFilesToDeleteIds];
         
         List<Month__c> lstMonthsUpdateStats = new List<Month__c>();
         for(Id monId : setMonthIds){
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
             req.setComments('Reverted to Earlier State');
             req.setAction('Approve');
             req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
             req.setWorkitemId(monId);
             try{
                Approval.ProcessResult result = Approval.process(req);
             }
             catch(Exception ex){
                
             }
             lstMonthsUpdateStats.add(new Month__c(Id = monId, Status__c = 'Approved'));
         }
         update lstMonthsUpdateStats;
    }

    public void finish(Database.BatchableContext BC){
        delete [Select Id From Task WHERE Created_After_Approval__c = True AND  (Status = 'Submitted' OR Status = 'Edited After Approval')];
    }
}