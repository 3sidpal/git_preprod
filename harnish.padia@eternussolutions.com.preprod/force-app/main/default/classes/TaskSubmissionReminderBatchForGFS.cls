/*
    Use the following code for running the class on 22nd of every month.
    
    TaskSubmissionReminderBatchForGFS schBatchGfs = new TaskSubmissionReminderBatchForGFS();
    String sch = '0 0 9 22 * ?';
    String jobID = system.schedule('Task Submission Reminder Batch For GFS', sch, schBatchGfs);
*/ 

public class TaskSubmissionReminderBatchForGFS Implements Database.Batchable <sObject>, Schedulable {
    
    public List<User> start(Database.BatchableContext bc) {
        
        DateTime currentTime = datetime.now().addMonths(1);
        String nextMonth = currentTime.format('MMMMM');
        String year = currentTime.format('YYYY');
        
        // Query to fetch all the month records which are submitted for approval
        String query = 'SELECT Id, Status__c, Month__c, Year__c, OwnerId FROM Month__c WHERE Submitted_for_Approval__c = TRUE AND '
                     + 'Month__c = \''+nextMonth+'\' AND Year__c = \''+year+'\'';
        
        List<Id> submissionDoneUserIds = new List<Id>();
        // Getting the Ids of User who have sent the Month record for Approval
        List<Month__c> monthList = Database.query(query);
        for(Month__c monthObj : monthList) {
            submissionDoneUserIds.add(monthObj.OwnerId);
        }
        List<User> userList = [SELECT Id, Email, FirstName FROM USER WHERE (Business_Unit__c = 'GFS' OR Business_Unit__c = 'Poultry' OR Business_Unit__c = 'Feeds') AND Profile.Name = 'Sales User' AND Id NOT IN : submissionDoneUserIds];
        //List<User> userList = [SELECT Id, Email, FirstName FROM USER WHERE Id = '0050l000001K1N9'];
        //return [SELECT Id, Email, FirstName FROM USER WHERE Id = '0050l000001K1N9'];
            System.debug('userListSize---->'+userList.size());
        if(Test.isRunningTest()){
            return [SELECT Id, Email, FirstName FROM User WHERE UserName LIKE 'smpfcTestUser@smpfc.com%'];
        }
        return userList;
    }
    
    public void execute(Database.BatchableContext bc, List<User> userList) {
        System.debug('userList------>'+userList);
        
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        
        DateTime currentTime = datetime.now().addMonths(1);
        String nextMonth = currentTime.format('MMMMM');
        system.debug('userList'+userList);
        for(User user : userList) {
            List<String> toAddresses = new List<String>{user.Email};
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSubject('Task Submission for Month '+nextMonth+' Pending');
            String messageBody = '<html>'
                            + '<body>Hello ' + user.FirstName + ',<br/>'
                            + 'You have not yet submitted your MCP for month '+nextMonth+' for approval. Kindly submit it ASAP'
                            + '<br/><br/><b>Thanks & Regards,</b><br/>Salesforce Admin</body></html>';
        }
        
        if(mailList != null) {
            System.debug('Sending mail');
            //Messaging.sendEmail(mailList);
        }
    
        
    }
    
    public void finish(Database.BatchableContext bc) {
        
    }
    
    public void execute(System.SchedulableContext sc) {
        Database.executeBatch(new TaskSubmissionReminderBatchForGFS());
    }
    
}