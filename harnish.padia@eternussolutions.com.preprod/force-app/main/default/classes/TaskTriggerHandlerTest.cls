@isTest
public class TaskTriggerHandlerTest {

	// Method to create Test Accounts
	public static List<Account> createTestAccount(Integer num) {
		List<Account> accountList = new List<Account>();
		for(Integer i=1; i<=num; i++) {
			accountList.add(new Account(Name='Test Account '+i, AccountNumber = String.valueOf(i)));
		}
		return accountList;
	}

	// Method to create test Month records
	public static List<Month__c> createMonth(Integer num, String month, String year) {
		List<Month__c> monthList = new List<Month__c>();
		for(Integer i=1; i<=num; i++) {
			monthList.add(new Month__c( Month__c=month
                                      , Year__c=year
                                      , Total_Work_Days__c = 5
                                      )
                         );
		}
		return monthList;
	}

    // Method to create test Month records
    public static List<Holiday__c> createHoliday(Integer num, Date holidayDate, String reason) {
        List<Holiday__c> holidayList = new List<Holiday__c>();
        for(Integer i=1; i<=num; i++) {
            holidayList.add(new Holiday__c( Reason_of_Holiday__c = reason
                                      , Date_of_Holiday__c = holidayDate
                                      )
                         );
        }
        return holidayList;
    }

    // Method to create test Account Configuration records
    public static List<Account_Configuration__c> createTestConfigurations(Integer num, Datetime startDate) {
        List<Account_Configuration__c> accConfigList = new List<Account_Configuration__c>();
        for(Integer i=0; i< num; i++) {
            accConfigList.add(new Account_Configuration__c( Number_of_Accounts__c = i+1
                                                          , StartDateTime__c = startDate.addDays(i)
                                                          , EndDateTime__c = startDate.addDays(i).addHours(8)
                                                          , User__c = UserInfo.getUserId()
                                                          , Title__c = 'Test Title'
                                                          , Subject__c = 'Test Subject'
                                                          , Status__c = 'New'
                                                          ));
        }
        return accConfigList;
    }

	// Method to create test Task Records
	public Static List<Task> createTasks( Integer num
                                        , String accId
                                        , DateTime startDate
                                        , Boolean checkin
                                        , Boolean checkout
                                        , String workType
                                        ) {
		List<Task> taskList = new List<Task>();
		for(Integer i=1; i<=num; i++) {
            Task taskObj = new Task();
            taskObj.Related_Account__c = accId;
            taskObj.StartDateTime__c = startDate.addDays(i);
            taskObj.EndDateTime__c = startDate.addDays(i).addHours(8);
            //taskObj.All_Day__c = true;
            taskObj.Title__c = 'Task '+i;
            taskObj.Status = 'In Progress';
            taskObj.User__c = UserInfo.getUserId();
            taskObj.Work_Type__C = workType;
            if(checkin)
                taskObj.Check_in__c = startDate.addDays(i).addMinutes(10);
            if(checkout)
                taskObj.Check_out__c = startDate.addDays(i).addMinutes(40);
            taskList.add(taskObj);
		}
		return taskList;
	}


    @isTest static void createMonthAndassignTask() {

		List<Account> accountList = createTestAccount(5);
		insert accountList;

        List<Holiday__c> holidayList = createHoliday(1, System.today().addDays(1), 'Test');
        insert holidayList;

        List<Task> taskListExisting = createTasks(5, accountList[0].Id, System.now(), true, true, 'Office work');
        insert taskListExisting;

		Test.startTest();
		List<Task> taskList = createTasks(5, accountList[0].Id, System.now(), true, true, 'Office work');
		insert taskList;

        List<Account_Configuration__c> accConfigList = createTestConfigurations(10, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigList;

		Test.stopTest();

		List<Task> taskWithMOnthList = [SELECT Id
                                             , Month__c
                                          FROM Task];
		System.assert(taskWithMOnthList[0].Month__c != null);
	}

	@isTest static void assignExistingMonth() {

		List<Account> accountList = createTestAccount(5);
		insert accountList;

		List<Month__c> monthList = createMonth(1, 'December', '2019');
		insert monthList;

		DateTime startDate = datetime.newInstance(2019, 12, 15, 12, 30, 0);
        List<Account_Configuration__c> accConfigList = createTestConfigurations(10, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigList;

		List<Task> taskList = createTasks(5, accountList[0].Id, startDate, true, false, 'Field work');

		Test.StartTest();
		insert taskList;
		Test.STopTest();

		List<Task> taskWithMOnthList = [SELECT Id
                                             , Month__c
                                          FROM Task
                                         WHERE Id IN: taskList];
		System.assert(taskWithMOnthList[0].Month__c == monthList[0].Id);

	}
    @isTest static void checkEformCreation() {

        List<Account> accountList = DIL_TestDataFactory.getAccounts(1,false);
        insert accountList;

        DateTime startDate = Datetime.newInstance(2019, 12, 15, 12, 30, 0);
        DateTime startDateLeave = Datetime.newInstance(2018, 09, 16, 12, 30, 0);
        Test.startTest();
            List<Task> taskList = DIL_TestDataFactory.getTasks(7, false);
            for (Task taskObj : taskList) {
                taskObj.Related_Account__c = accountList[0].Id;
                taskObj.StartDateTime__c = startDate;
                taskObj.EndDateTime__c = startDate.addHours(8);
                taskObj.Work_Type__c = 'Field work';
            }

            taskList[0].Forms_to_be_filled__c = 'MOM';
            taskList[1].Forms_to_be_filled__c = 'Contact Report';
            taskList[2].Forms_to_be_filled__c = 'Service Level Survey';
            taskList[3].Forms_to_be_filled__c = 'Trade Check';
            taskList[4].Forms_to_be_filled__c = 'Trade Visit';
            taskList[5].Forms_to_be_filled__c = 'Test';
            taskList[6].Forms_to_be_filled__c = 'Trade Audit';
            
            taskList[0].Type_of_Activity__c = 'BU Specific Activities';
            taskList[1].Type_of_Activity__c = 'BU Specific Activities';
            taskList[2].Type_of_Activity__c = 'BU Specific Activities';
            taskList[3].Type_of_Activity__c = 'BU Specific Activities';
            taskList[4].Type_of_Activity__c = 'BU Specific Activities';
            taskList[5].Type_of_Activity__c = 'BU Specific Activities';
            taskList[6].Type_of_Activity__c = 'BU Specific Activities';

            List<Task> lstTask = DIL_TestDataFactory.getTasks(1, false);
            lstTask[0].Related_Account__c = accountList[0].Id;
            lstTask[0].StartDateTime__c = startDate;
            lstTask[0].EndDateTime__c = startDate.addHours(8);
            lstTask[0].Work_Type__c = 'Office work';
            lstTask[0].Subject = 'Test Subject';
            lstTask[0].Type_of_Activity__c = 'Test Type of Activity';
            lstTask[0].Forms_to_be_filled__c = 'Test';
            taskList.addAll(lstTask);

            List<Task> lstLeaveTask = DIL_TestDataFactory.getTasks(1, false);
            lstLeaveTask[0].Related_Account__c = accountList[0].Id;
            lstLeaveTask[0].StartDateTime__c = startDate;
            lstLeaveTask[0].EndDateTime__c = startDate.addHours(8);
            lstLeaveTask[0].Work_Type__c = 'Leave';
            lstLeaveTask[0].Subject = 'Test Subject';
            lstLeaveTask[0].Type_of_Activity__c = 'Test Type of Activity';
            lstLeaveTask[0].Forms_to_be_filled__c = 'Test';
            taskList.addAll(lstLeaveTask);

            List<User> lstUsers = DIL_TestDataFactory.createBulkUsers(3, false);
            lstUsers[1].Business_Unit__c = 'GFS';
            lstUsers[2].Business_Unit__c = 'Feeds';
            insert lstUsers;

            lstUsers = [Select Id From User Where Id In:lstUsers And Business_Unit__c = 'Feeds'];
            for (Task objTask : taskList) {
                objTask.User__c = lstUsers[0].Id;
            }
            insert taskList;

        Test.stopTest();

        List<Task> lstTasks = [select Eform__c From Task Where Id =: taskList[0].Id];
        System.assert(lstTasks.size() > 0);

        for (Task objTask : lstTasks) {
            System.assert(objTask.Eform__c != null);
        }
    }

    @isTest static void checkBackupOnRecordUpdate(){

        Month__c objMonth = new Month__c();
        objMonth.Name = 'Test';
        objMonth.Status__c = 'Edited After Approval';
        objMonth.Year__c = '2018';
        objMonth.Month__c = 'December';
        insert objMonth;

        TaskTriggerHandler objTTH = new TaskTriggerHandler();
        TaskTriggerHandler.DAY_OF_WEEK = System.now().format('EEEE');

        List<Account> accountList = createTestAccount(1);
		insert accountList;

        List<Account_Configuration__c> accConfigList = createTestConfigurations(2, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigList;

        List<Task> taskList = createTasks(1, accountList[0].Id, System.now(), true, true, 'Office work');
		for(Task objTask : taskList){
            objTask.Month__c = objMonth.Id;
            objTask.Created_After_Approval__c = False;
            objTask.Status = 'Edited After Approval';
        }
		//Test.startTest();
		insert taskList;

		Map<Id,Task> mapIdTask = new Map<Id,Task>();
        for(Task objTask : taskList){
            mapIdTask.put(objTask.Id,objTask);
        }

        System.Test.startTest();
        objTTH.persistOldvaluesOnRecordUpdate(mapIdTask);
        System.Test.stopTest();
        //Test.stopTest();

        delete taskList;
    }

    @isTest static void accountConfigChangeTest(){

        TaskTriggerHandler objTTH = new TaskTriggerHandler();
        TaskTriggerHandler.DAY_OF_WEEK = System.now().format('EEEE');

        List<Account> accountList = createTestAccount(1);
        insert accountList;

        List<Account_Configuration__c> accConfigList = createTestConfigurations(2, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigList;

        List<Task> taskList = createTasks(1, accountList[0].Id, System.now(), true, true, 'Office work');
        for(Task objTask : taskList) {
            objTask.Created_After_Approval__c = False;
            objTask.Status = 'Edited After Approval';
            objTask.Account_Configuration__c = accConfigList.get(0).Id;
            objTask.Activity_Status__c = 'Incomplete';
        }
        Test.startTest();
        insert taskList;
        Test.stopTest();

        System.assertEquals('Incomplete', [SELECT Activity_Status__c FROM Account_Configuration__c WHERE Id = :accConfigList.get(0).Id].Activity_Status__c);
    }

    @isTest static void accountConfigChangeTest2(){

        TaskTriggerHandler objTTH = new TaskTriggerHandler();
        TaskTriggerHandler.DAY_OF_WEEK = System.now().format('EEEE');

        List<Account> accountList = createTestAccount(1);
        insert accountList;

        List<Account_Configuration__c> accConfigList = createTestConfigurations(2, System.now());
        for(Account_Configuration__c accConfigObj : accConfigList) {
            accConfigObj.Accounts_Created__c = 1;
        }
        insert accConfigList;

        List<Task> taskList = createTasks(1, accountList[0].Id, System.now(), true, true, 'Office work');
        for(Task objTask : taskList) {
            objTask.Created_After_Approval__c = False;
            objTask.Status = 'Edited After Approval';
            objTask.Account_Configuration__c = accConfigList.get(0).Id;
            objTask.Activity_Status__c = 'Delayed';
        }
        Test.startTest();
        insert taskList;
        //new TaskTriggerHandler().deleteOrphanFiles(new Map<Id,Task>());
        Test.stopTest();

        System.assertEquals('Delayed', [SELECT Activity_Status__c FROM Account_Configuration__c WHERE Id = :accConfigList.get(0).Id].Activity_Status__c);
    }
    
    
    public static void init() {

        List<Month__c> monthList = DIL_TestDataFactory.getMonth(1, true);
        Account_Configuration__c congigObj = new Account_Configuration__c( Number_of_Accounts__c = 3
                                                                          , StartDateTime__c = DateTime.now().addDays(-1)
                                                                          , EndDateTime__c =DateTime.now().addDays(-1).addHours(8)
                                                                          , User__c = UserInfo.getUserId()
                                                                          , Title__c = 'Test Title'
                                                                          , Subject__c = 'Test Subject'
                                                                          , Status__c = 'New'
                                                                          , Name = 'Multiple Accounts 3'
                                                                          , Month__c = monthList[0].Id
                                                                          );
        insert congigObj;
        
        List<Task> taskList = DIL_TestDataFactory.getTasks(1, false);
        taskList[0].Work_Type__c = 'Field Work';
        taskList[0].EndDateTime__c = System.now().addHours(2) - 1;
        taskList[0].StartDateTime__c = System.now() - 1;
        taskList[0].Activity_Status__c = 'Open';
        taskList[0].Type_Of_Activity__c = 'BU Specific Activities';
        taskList[0].Account_Configuration__c = congigObj.Id;
        taskList[0].Month__c = monthList[0].Id;
        insert taskList;
    }

    @isTest static void testIncompleteActivityStatusOrNot() {
        
        init();
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c, Month__c From Task];
        Set<Id> monthIdSet = new Set<Id>();
        for(Task taskObk : lstTask) {
            monthIdSet.add(taskObk.Month__c);
        }
        List<Month__c> monthList = new List<Month__c>();
        for(Month__c monthObj : [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdSet]) {
            monthObj.Status__c = 'Approved';
            monthList.add(monthObj);
        }

        update monthList;
        Test.startTest();
            Id batchJobId = Database.executeBatch(new BatchToUpdateIncompleteTasks(), 200);
        Test.stopTest();
        lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        System.assert(lstTask.size() > 0);
        System.debug('lstTask[0].Activity_Status__c :: '+lstTask[0].Activity_Status__c);
        System.assertEquals(lstTask[0].Activity_Status__c, 'Incomplete');
    }
    
    @isTest static void scheduleBatchTest() {
        init();
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c, Month__c From Task];
        Set<Id> monthIdSet = new Set<Id>();
        for(Task taskObk : lstTask) {
            monthIdSet.add(taskObk.Month__c);
        }
        List<Month__c> monthList = new List<Month__c>();
        for(Month__c monthObj : [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdSet]) {
            monthObj.Status__c = 'Approved';
            monthList.add(monthObj);
        }

        update monthList;
        Test.startTest();
            System.scheduleBatch(new BatchToUpdateIncompleteTasks(), 'BatchToUpdateIncompleteTasks', 5);
            new BatchToUpdateIncompleteTasks().execute(null);
        Test.stopTest();
        lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        System.assert(lstTask.size() > 0);
        System.debug('lstTask[0].Activity_Status__c :: '+lstTask[0].Activity_Status__c);
        System.assertEquals(lstTask[0].Activity_Status__c, 'Incomplete');
    }
    
    @isTest static void batchTestForMultipleAccount() {
        
        List<Month__c> monthList = DIL_TestDataFactory.getMonth(1, true);
        Account_Configuration__c congigObj = new Account_Configuration__c( Number_of_Accounts__c = 3
                                                                          , StartDateTime__c = DateTime.now().addDays(-1)
                                                                          , EndDateTime__c =DateTime.now().addDays(-1).addHours(8)
                                                                          , User__c = UserInfo.getUserId()
                                                                          , Title__c = 'Test Title'
                                                                          , Subject__c = 'Test Subject'
                                                                          , Status__c = 'New'
                                                                          , Name = 'Multiple Accounts 3'
                                                                          , Month__c = monthList[0].Id
                                                                          );
        insert congigObj;
        
        List<Task> taskList = DIL_TestDataFactory.getTasks(3, false);
        taskList[0].Work_Type__c = 'Field Work';
        taskList[0].EndDateTime__c = System.now().addHours(2) - 1;
        taskList[0].StartDateTime__c = System.now() - 1;
        taskList[0].Type_Of_Activity__c = 'BU Specific Activities';
        taskList[0].Activity_Status__c = 'Open';
        taskList[0].Account_Configuration__c = congigObj.Id;
        taskList[0].Month__c = monthList[0].Id;
        taskList[0].Account_Name__c = 'Test Account';
        
        
        taskList[1].Work_Type__c = 'Field Work';
        taskList[1].EndDateTime__c = System.now().addHours(2) - 1;
        taskList[1].StartDateTime__c = System.now() - 1;
        taskList[1].Type_Of_Activity__c = 'BU Specific Activities';
        taskList[1].Activity_Status__c = 'Open';
        taskList[1].Month__c = monthList[0].Id;
        taskList[1].Account_Name__c = 'Test Account';
        taskList[1].Check_in__c = System.now();
        taskList[1].Check_out__c = System.now();
        
        taskList[2].Work_Type__c = 'Field Work';
        taskList[2].EndDateTime__c = System.now().addHours(2) - 1;
        taskList[2].StartDateTime__c = System.now() - 1;
        taskList[2].Type_Of_Activity__c = 'BU Specific Activities';
        taskList[2].Activity_Status__c = 'Open';
        taskList[2].Month__c = monthList[0].Id;
        taskList[2].IsUnplanned__c = True;
        taskList[2].Account_Name__c = 'Test Account';
        taskList[2].Check_in__c = System.now();
        taskList[2].Check_out__c = System.now();
        insert taskList;
        
        List<Task> lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c, Month__c From Task];
        Set<Id> monthIdSet = new Set<Id>();
        for(Task taskObk : lstTask) {
            monthIdSet.add(taskObk.Month__c);
            taskObk.Task_Completion_Datetime__c = System.now().addHours(-2) - 1;
        }
        update lstTask;
        
        List<Month__c> monthListToUpdate = new List<Month__c>();
        for(Month__c monthObj : [SELECT Id, Status__c FROM Month__c WHERE Id IN: monthIdSet]) {
            monthObj.Status__c = 'Approved';
            monthListToUpdate.add(monthObj);
        }

        update monthListToUpdate;
        Test.startTest();
            Id batchJobId = Database.executeBatch(new BatchToUpdateIncompleteTasks(), 200);
        Test.stopTest();
        lstTask = [SELECT EForm__c,EndDateTime__c,Work_Type__c,Activity_Status__c From Task];
        Account_Configuration__c accConfig = [select Id, Accounts_Left__c, Number_of_Accounts__c, Accounts_Created__c, Activity_Status__c from Account_Configuration__c limit 1];
        
        System.assertEquals(2 , accConfig.Accounts_Left__c);
        System.assertEquals(1 , accConfig.Accounts_Created__c);
        System.assertEquals('Incomplete' , accConfig.Activity_Status__c);
        System.assert(lstTask.size() > 0);
        System.debug('lstTask[0].Activity_Status__c :: '+lstTask[0].Activity_Status__c);
        System.assertEquals(lstTask[0].Activity_Status__c, 'Complete');
    }

}