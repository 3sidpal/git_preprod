public with sharing class TradeCheckFormExtension {

    public TradeCheckFormExtension(ApexPages.StandardController stdController) {
    }

    @RemoteAction
    public static Trade_Audit__c getExistingTradeCheck(Id tradeId) {
        List<Trade_Audit__c> lstTradeAudit = [
            SELECT Id
                 , Name
                 , Tindera_Merchandisers_Name__c
                 , eForm__c
                 , Outlet_Name__c
                 , Price_Movement__c
                 , Product_Formats__c
                 , Schedule_Of_Deliveries__c
                 , Status__c
                 , Stocks_Supply_Source__c
                 , Submitted_Date_Time__c
                 , Sync_Date_Time__c
                 , Unserved_Orders__c
                 , Market_Share_Percentage__c
                 , Wet_Market_Supermarket_Chain__c
                 ,(Select Brand_Name__c,SKU__c,Selling_Price__c,Volume__c From Trade_Audit_Brands__r ORDER BY Sequence__c)
              FROM Trade_Audit__c
             WHERE Id = :tradeId
        ];

        if(lstTradeAudit.isEmpty() == True) {
            return new Trade_Audit__c();
        }
        return lstTradeAudit[0];
    }
}