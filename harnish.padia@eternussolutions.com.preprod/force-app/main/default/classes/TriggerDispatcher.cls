/*-------------------------------------------------------------------------------------------
Author       :   eugenebasianomutya
Created Date :   May 1 2017
Definition   :   Class primarily responsible in handling trigger execution per object.
History      :   
-------------------------------------------------------------------------------------------*/
public class TriggerDispatcher {
    
    /*-------------------------------------------------------------------------------------------
    Author       :   
    Created Date :   May 1 2017
    Definition   :   Call this method from your trigger, passing in an instance of a trigger handler which implements ITriggerHandler.
                     This method will fire the appropriate methods on the handler depending on the trigger context.
    History      :   
    -------------------------------------------------------------------------------------------*/
    public static void Run(TriggerHandler handler) {
       
        // Before trigger logic
        if (Trigger.IsBefore) {
            if (Trigger.IsInsert)
                handler.BeforeInsert(Trigger.New);
            
            if (Trigger.IsUpdate)
                handler.BeforeUpdate(Trigger.New,
                                     Trigger.NewMap,
                                     Trigger.Old,
                                     Trigger.OldMap);
            
            if (Trigger.IsDelete)
                handler.BeforeDelete(Trigger.Old, Trigger.OldMap);
        }
        
        // After trigger logic
        if (Trigger.IsAfter) {
            if (Trigger.IsInsert)
                handler.AfterInsert(Trigger.New, Trigger.NewMap);
            
            if (Trigger.IsUpdate)
                handler.AfterUpdate(Trigger.New,
                                    Trigger.NewMap,
                                    Trigger.Old,
                                    Trigger.OldMap);
            
            if (trigger.IsDelete)
                handler.AfterDelete(Trigger.Old, Trigger.OldMap);
            
            if (trigger.isUndelete)
                handler.AfterUndelete(Trigger.New, Trigger.NewMap);
        }
    }
}