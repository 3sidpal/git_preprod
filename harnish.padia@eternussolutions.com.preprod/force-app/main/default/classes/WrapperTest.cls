@isTest
public with sharing class WrapperTest {
    public static testMethod void testMethodForSIVWrappers() {
        Test.startTest();
        SIVWrapper wrapperObj = new SIVWrapper();
        wrapperObj.accountId = null;
        wrapperObj.productId = null;
        wrapperObj.buCode = '';
        wrapperObj.distributorCode = '';
        wrapperObj.skuCode = '';
        wrapperObj.uomName = '';
        wrapperObj.channelCode = '';
        wrapperObj.Inv = null;
        wrapperObj.Item = null;
        wrapperObj.Line = null;
        Test.stopTest();
        System.assert(true);
    }

    // Test method for Wrapper class
    public static testMethod void testMethodForWrappers() {
        List<String> errorMsg = new List<String>();
        List<Task>   taskList = new List<Task>();
        Map<String, String> eformNameVsEformPrefix = new Map<String, String>();
        Test.startTest();
        Wrappers.TaskResponseWrapper wrapperObj = new Wrappers.TaskResponseWrapper();
        wrapperObj.success = true;
        wrapperObj.jsonStr = 'Test';
        wrapperObj.errorMsg = errorMsg;
        wrapperObj.taskList = taskList;
        wrapperObj.eformNameVsEformPrefix = eformNameVsEformPrefix;
        wrapperObj.timeStamp = System.now();
        Test.stopTest();
        System.assert(true);
    }
}