global class orderCleanerBatch implements Database.Batchable<sObject>, Database.Stateful {

    global Database.querylocator start(Database.BatchableContext BC) {
        ExtractionUpdateUtility eupUtility = new ExtractionUpdateUtility();
        String query = eupUtility.createQuery('Order__c', new Set<String>(), 'Status__c = \'Draft\'', new List<String>());
         
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Order__c> orderQueryResult) {
    
        if(!orderQueryResult.isEmpty()) {
            try {
                delete orderQueryResult;
            }
            catch(exception e) {
            
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}