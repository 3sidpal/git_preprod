global class orderCleanerBatch_Scheduler implements Schedulable {

    global void execute(SchedulableContext sc) {
        orderCleanerBatch batchUploader = new orderCleanerBatch(); 
        database.executebatch(batchUploader);        
    }
}