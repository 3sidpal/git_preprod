@isTest
private class orderCleanerBatch_Test {

    @isTest static void clearDraftOrder() {
        
        Test.startTest(); 
        
        String purchaseOrderRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId();
        String accoundDistributorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
        
            //Insert Business Unit Record
            Market__c mkt = new Market__c(
            Name = 'POULTRY');
            Insert mkt;
            
            //Insert Account Record for Customer And Distributor Records.
            Account disAcc0 = new Account(
            Active__c = TRUE,
            Name = 'distributorAccount',
            Market__c = mkt.Id,
            AccountNumber = '12345',
            General_Trade__c = TRUE,
            Delivery_Time_From__c = '5:00 AM',
            Delivery_Time_To__c = '6:00 PM',
            RecordTypeId = accoundDistributorRecordTypeId);
            Insert disAcc0;
            
            //Insert SAS Contact
            String sasRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
            Contact con0 = new Contact(
            FirstName = 'First0',
            LastName = 'Last0',
            AccountId = disAcc0.id,
            Is_Primary__c = TRUE,
            RecordTypeId = sasRecordTypeId);
        
            insert con0;
        
            //Insert Order Record
            Order__c ord = new Order__c(
            Account__c = disAcc0.Id,
            SAS__c = con0.Id,
            PO_No__c = 'TEST0004',
            Status__c = 'Draft',
            RecordTypeId = purchaseOrderRecordTypeId);
            Insert ord; 
        
            orderCleanerBatch_Scheduler sh1 = new orderCleanerBatch_Scheduler();
            String sch = '0 0 23 * * ?'; system.schedule('Test Territory Check', sch, sh1); 
        
        Test.stopTest(); 
    }
}