var acvityMap;
var recordObj = {};
var recordObjList = [];
var dailyHoursList = [];
var taskList = [];
var newTaskList = [];
var taskDltList = [];
var accountTaskWrapperList = [];
var accountTaskWrapperObj = {};
var newTaskHoursMap = {};
var metaDataVarClassVar;
var userListClassVar;
var totalWorkDays;
var totalOfficeHours;
var totalFieldHours;
var totalLeaveHours;
var totalWorkDaysCompleted;
var rowIndex;
var frequencyValues = [];
var holidayList = [];
var leaveReasons = [];
var monthDetails;
var userBU;

$(document).ready(function() {
    documentReadyFunction();
});


function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

//---------------------------------------documentReadyFunction start-------------------------------------------
function documentReadyFunction() {

    monthDetails = JSON.parse(localStorage.getItem("monthDetails"));

    unableDisableStartEnd('w1');
    unableDisableStartEnd('w2');
    unableDisableStartEnd('w3');
    unableDisableStartEnd('w4');
    unableDisableStartEnd('w5');
    unableDisableStartEnd('w6');
    $('.eTime').timepicker({
        timeFormat: 'hh:mm:ss p',
        interval: 5,
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('.sTime').timepicker({
        timeFormat: 'hh:mm:ss p',
        interval: 5,
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        change:function(time) {
            var weekNo = $(this).attr("id").substr(0, 2);

            $('#'+weekNo+'EndTime').val("");
            $('#'+weekNo+'EndTime').timepicker("destroy");

            if(moment(time, "hh:mm:ss A", true).isValid()) {
                $("#error").hide();
                $("#error").text("");
                $(this).css("border","");
                time = moment(time,'hh:mm:ss A').add(5,'minutes').format('hh:mm:ss A');
                $('#'+weekNo+'EndTime').timepicker({
                    timeFormat: 'hh:mm:ss p',
                    interval: 5,
                    startTime: time,
                    minTime: time,
                    dynamic: false,
                    dropdown: true,
                    scrollbar: true,
                    change:function() {
                        if(!(moment($(this).val(), "hh:mm:ss A", true).isValid())) {
                            $(this).css("border","1px solid red");
                            $("#error").show();
                            $("#error").text("Please enter valid End Time for Week "+$(this).attr("id").substr(1,1));
                        }
                        else {
                            $("#error").hide();
                            $("#error").text("");
                            $(this).css("border","");
                        }
                    }
                });
            } else {
                $(this).css("border","1px solid red");
                $("#error").show();
                $("#error").text("Please enter valid Start Time for Week "+$(this).attr("id").substr(1,1));
                $('#'+weekNo+'EndTime').timepicker({
                    timeFormat: 'hh:mm:ss p',
                    interval: 5,
                    startTime: '00:00',
                    dynamic: false,
                    dropdown: true,
                    scrollbar: true,
                    change:function() {
                        if(!(moment($(this).val(), "hh:mm:ss A", true).isValid())) {
                            $(this).css("border","1px solid red");
                            $("#error").show();
                            $("#error").text("Please enter valid End Time for Week "+$(this).attr("id").substr(1,1));
                        }
                        else {
                            $("#error").hide();
                            $("#error").text("");
                            $(this).css("border","");
                        }
                    }
                });
            }
        }
    });



    // Fetching existing task details from controller
    var monthName = getnumMonth(monthDetails.month);
    var existingRecTasks = [];
    Visualforce.remoting.Manager.invokeAction(
        'RecurringTasksController.taskHours',
        monthDetails.ownerId,
        monthDetails.year,
        monthName,
        function(result, event) {
            if (event.status) {
                existingRecTasks = JSON.parse(result)["existingRecTasks"];
                console.log('existingRecTasks ',existingRecTasks);
                totalWorkDays = JSON.stringify(JSON.parse(result)["totalWorkDays"]);
                taskList = JSON.parse(result)["taskList"];
                holidayList = JSON.parse(result)["holidayList"];
                leaveReasons = JSON.parse(result)["leaveReasons"];
                for(let item of leaveReasons) {
                    item = item.trim();
                    if(!item) {
                        continue;
                    }
                    $('#leaveReasonId').append('<option value="' + item + '">' + item + '</option>');
                }

                if(JSON.parse(JSON.parse(result).currentUser).currentUserRecord.Business_Unit__c == 'GFS') {
                    userBU = 'GFS';
                    var cityElm = '<span class="fieldInfo">Filter Account Details</span>';
                    $(cityElm).appendTo($("#cityLabel"));
                    var provineElm = '<span class="fieldInfo">Filter Account Details</span>';
                    $(provineElm).appendTo($("#provinceLabel"));
                    $(".numOfAccounts").hide();
                }
                else if(JSON.parse(JSON.parse(result).currentUser).currentUserRecord.Business_Unit__c == 'Poultry'
                     || JSON.parse(JSON.parse(result).currentUser).currentUserRecord.Business_Unit__c == 'Feeds') {
                    userBU = 'Agro';
                    var cityElm = '<font color="red" id="requiredCity"> *</font> <span class="fieldInfo">Filter Account Details</span>';
                    $(cityElm).appendTo($("#cityLabel"));
                    var provineElm = '<font color="red" id="requiredProvince"> *</font> <span class="fieldInfo">Filter Account Details</span>';
                    $(provineElm).appendTo($("#provinceLabel"));
                }
                disableWeek();
                getSummary(result);
            } else if (event.type === 'exception') {
            }
        },
        {escape: false}
    );

    fetchMonthStatus(monthDetails.ownerId, monthName, monthDetails.year);

    fetchExistingTask();

    // initializing clock picker
    $("#nextMonth").text(monthName + '');
    $("#nextYear").text(monthDetails.year);
    $("#targetDiv").hide();
    rowIndex = -1;

    // onchange on work type
    $("#workTypeId").change(function() {
        $("#accountId").val("");
        $("#FrequencyId :selected").val("None");
        $('#workTypeId').css("border-color","");
        $('#workTypeErr').hide();
        $('#activityId').empty();
        $('.day').each(function() {
            this.checked = false;
        });
        $("#targetDiv").hide();
        typeOfActivityCompute();
        $("#error").hide();
        $("#error").text("");
        $('#leaveTypeId').val('Full Day');
        unableDisableStartEnd('w1');
        unableDisableStartEnd('w2');
        unableDisableStartEnd('w3');
        unableDisableStartEnd('w4');
        unableDisableStartEnd('w5');
        unableDisableStartEnd('w6');
    });

    // onchange on work type
    $("#cityId").keyup(function() {
        $("#FrequencyId :selected").val("None");
        if($("#numOfAccId").val() == '') {
            $("#accountId").val('');
        }

        if($("#cityId").val().trim() && $("#numOfAccId").val() == '') {
            $("#accountId").prop('disabled',false);
            $("#accountId").css('background-color', '');
        } else {
            $("#cityId").attr("data","");
            $("#cityId").attr("name","");
            if(userBU == 'Agro') {
                $("#accountId").prop('disabled',true);
                $("#accountId").css('background-color', 'grey');
            }
        }

    });


    var temp = window.alert;
    $('#closeEditModal').mousedown(function(e) {
        window.alert = function() { };
    });

    $('#closeEditModal').mouseup(function(e) {
        window.alert = temp;
    });

    // onchange on work type
    $("#accountId").keyup(function() {
        $("#accountId").removeAttr("data");
        $("#accountId").removeAttr("name");
    });

    // on change on activity
    $("#activityId").change(function() {
        $('#activityId').css("border-color","");
        $('#activityErr').hide();
        $('#activityId').css("border","0px");
        if($("#activityId").val() == 'Account Sales Call / Order Booking') {
            $("#targetDiv").show();
        }
        else {
            $("#targetDiv").hide();
        }
    });

    // on change on activity type
    $("#activityTypeId").change(function() {
        $('#activityTypeId').css("border-color","");
        $('#activityTypeErr').hide();
        $('#activityTypeId').css("border","0px");
        editEventBUCompute();
        $("#targetDiv").hide();
        // displayActivity($('#activityTypeId :selected').text());
    });

    // on change on frequency
    $("#FrequencyId").change(function() {
        $('.day').each(function() {
            this.checked = false;
        });
        hideShowStartEndTime($("#workTypeId").val());
        $(".sTime").css('disabled', 'true');
        $(".eTime").css('disabled', 'true');
        hideStartEndTime('w1');
        hideStartEndTime('w2');
        hideStartEndTime('w3');
        hideStartEndTime('w4');
        hideStartEndTime('w5');
        hideStartEndTime('w6');
    });

    $("#leaveTypeId").change(function() {
        if($('#leaveTypeId').val() == 'Half Day') {
            $('.halfDayleaveDiv').show();
        }
        else {
            $('.halfDayleaveDiv').hide();
        }
    });

    //on change on checkbox
    $(".day").change(function() {
        var weekNo = $(this).attr("id").substr(0, 2);
        unableDisableStartEnd(weekNo);
        if(this.checked) {
            requiredFields();
        }
    });

    $("#numOfAccId").blur(function() {
        if($("#numOfAccId").val() != '' && $("#numOfAccId").val() < 2){
            $("#numOfAccId").val('');
            alert('Multiple accounts cannot be less than 2');
            $("#freqDiv").show();
            $("#activityTypeDiv").show();
            $("#activityDiv").show();
            return;
        }
    })

    $("#numOfAccId").keyup(function() {

        if($("#numOfAccId").val() != '') {
            $("#requiredCity").text("");
            $("#requiredProvince").text("");
            $("#freqDiv").hide();
            $("#activityTypeDiv").hide();
            $("#activityDiv").hide();
            $("#accountId").val("Multiple Accounts "+$("#numOfAccId").val())
            $("#accountId").css('background-color', 'grey');
            $("#accountId").prop('disabled',true);
            $("#accountId").removeAttr("name");
            // $("#provinceId").removeAttr("data");
            // $("#provinceId").removeAttr("name");
            // $("#cityId").removeAttr("data");
            // $("#cityId").removeAttr("name");
            // $("#cityId").val("");
            // $("#provinceId").val("");
            if($("#provinceId").val() == '') {
                $("#cityId").prop('disabled',true);
                $("#cityId").css('background-color', 'grey');
            }
            else {
                $("#cityId").prop('disabled',false);
            }

        }
        else {
            $("#requiredCity").text(" *");
            $("#requiredProvince").text(" *");
            $("#accountId").val("");
            $("#freqDiv").show();
            $("#activityTypeDiv").show();
            $("#activityDiv").show();
            // $("#accountId").prop('disabled',true);
            // $("#accountId").css('background-color', 'grey');
            // $("#cityId").prop('disabled',true);
            // $("#cityId").css('background-color', 'grey');
            // $("#provinceId").removeAttr("data");
            // $("#provinceId").removeAttr("name");
            // $("#cityId").removeAttr("data");
            // $("#cityId").removeAttr("name");
            // $("#cityId").val("");
            // $("#provinceId").val("");
            $("#provinceId").prop('disabled',false);
            $("#provinceId").css('background-color', '');

            if($("#cityId").val() == '') {
                $("#accountId").prop('disabled',true);
                $("#accountId").css('background-color', 'grey');
            }
            else {
                $("#accountId").prop('disabled',false);
                $("#accountId").css('background-color', '');
            }
        }
    })

    $("#numOfAccId").click(function() {
        if($("#numOfAccId").val() != '') {
            // $("#requiredCity").text("");
            // $("#requiredProvince").text("");
            $("#freqDiv").hide();
            $("#activityTypeDiv").hide();
            $("#activityDiv").hide();
            $("#accountId").val("Multiple Accounts "+$("#numOfAccId").val());
            $("#accountId").removeAttr("name");
            $("#accountId").css('background-color', 'grey');
            // $("#accountId").prop('disabled',true);
            // $("#accountId").removeAttr("name");
            // $("#provinceId").removeAttr("data");
            // $("#provinceId").removeAttr("name");
            // $("#cityId").removeAttr("data");
            // $("#cityId").removeAttr("name");
            // $("#cityId").val("");
            // $("#provinceId").val("");
            // $("#cityId").prop('disabled',true);
            // $("#cityId").css('background-color', 'grey');
        }
        else {
        }
    })

    $("#provinceId").keyup(function() {
        $("#provinceId").css("border","");
        $("#cityId").css("border","");
        $("#cityId").val("");
        $("#error").hide();
        $("#error").text("");
        if($("#provinceId").val().trim()) {
            $("#cityId").prop('disabled',false);
            $("#cityId").css('background-color', '');
            // $("#numOfAccId").val('');
            if($("#numOfAccId").val() == "") {
                $("#accountId").val("");
                $("#freqDiv").show();
                $("#activityTypeDiv").show();
                $("#activityDiv").show();
            }

        } else {
            $("#provinceId").removeAttr("data");
            $("#provinceId").removeAttr("name");
            $("#cityId").removeAttr("data");
            $("#cityId").removeAttr("name");
            if(userBU == 'Agro') {
                $("#cityId").prop('disabled',true);
                $("#cityId").css('background-color', 'grey');
                $("#accountId").prop('disabled',true);
                $("#accountId").css('background-color', 'grey');
            }
        }
        if($("#numOfAccId").val() == '') {
            $("#accountId").val('');
        }
    });

    $("#customerGroupId").keyup(function() {
        $("#customerGroupId").css("border","");
        $("#error").hide();
        $("#error").text("");
        $("#accountId").val('');
        if($("#customerGroupId").val().trim()) {
        } else {
            $("#customerGroupId").removeAttr("data");
            $("#customerGroupId").removeAttr("data2");
            $("#customerGroupId").removeAttr("name");
        }
        if($("#customerGroupId").attr("data") == '') {
            $("#customerGroupId").attr("data","");
            $("#customerGroupId").attr("name","");
        }

    });

    $("#activityDetail").scroll(function(){
        $('.ui-timepicker-container, #ui-datepicker-div').hide();
        $('.sTime').blur();
        $('.eTime').blur();

    });


    $(".sTime").css('disabled', 'true');
    $(".eTime").css('disabled', 'true');

    fetchBUMetaData();
    fetchFrequencyValues();

}

function requiredFields() {
    if(($('#activityTypeId').val() == 'None' || $('#activityTypeId').val() == null) && $('#workTypeId').val() != 'Leave') {
        if($('#workTypeId').val() == 'Field work' && userBU != 'Agro' && $("#numOfAccId").val() == '') {
            $('#accountId').css("border","");
            $('#activityTypeId').focus();
            $('#activityTypeId').css("border","1px solid red");
            $("#error").show();
            $("#error").text("Please fill required fields");
        }
    }
    else if(($('#activityId').val() == 'None' || $('#activityId').val() == null) && $('#workTypeId').val() != 'Leave') {
        if($('#workTypeId').val() == 'Field work' && userBU != 'Agro' && $("#numOfAccId").val() == '') {
            $('#activityTypeId').focus();
            $('#activityTypeId').css("border","");
            $('#activityId').css("border","1px solid red");
            $("#error").show();
            $("#error").text("Please fill required fields");
        }
    }
    else{
        $('#accountId').css("border","");
        $('#activityTypeId').css("border","");
        $('#activityId').css("border","");
        $("#error").hide();
        $("#error").text("");
    }
}

function unableDisableStartEnd(weekNo) {
    var enabledStartEnd = false;
        if($("#"+weekNo+"Mo")[0].checked) {
            enabledStartEnd = true;
        }
        if($("#"+weekNo+"Tu")[0].checked) {
            enabledStartEnd = true;
        }
        if($("#"+weekNo+"Wd")[0].checked) {
            enabledStartEnd = true;
        }
        if($("#"+weekNo+"Th")[0].checked) {
            enabledStartEnd = true;
        }
        if($("#"+weekNo+"Fr")[0].checked) {
            enabledStartEnd = true;
        }
        if($("#"+weekNo+"Sa")[0].checked) {
            enabledStartEnd = true;
        }
        if($("#"+weekNo+"Su")[0].checked) {
            enabledStartEnd = true;
        }
        if(enabledStartEnd && $("#workTypeId").val() != 'Leave') {
            $("#"+weekNo+"StartTime").removeAttr("readonly");
            $("#"+weekNo+"EndTime").removeAttr("readonly");
            $("#"+weekNo+"StartTime").css('pointer-events', 'auto');
            $("#"+weekNo+"EndTime").css('pointer-events', 'auto');
            $("#"+weekNo+"StartTime").css('background-color', '');
            $("#"+weekNo+"EndTime").css('background-color', '');
        }
        else {
            $("#"+weekNo+"StartTime").val("");
            $("#"+weekNo+"EndTime").val("");
            $("#"+weekNo+"StartTime").attr("readonly","true");
            $("#"+weekNo+"EndTime").attr("readonly","true");
            $("#"+weekNo+"StartTime").css('pointer-events', 'none');
            $("#"+weekNo+"EndTime").css('pointer-events', 'none');
            $("#"+weekNo+"StartTime").css('background-color', 'grey');
            $("#"+weekNo+"EndTime").css('background-color', 'grey');
            $("#"+weekNo+"StartTime").css("border","0px");
            $("#"+weekNo+"EndTime").css("border","0px");
        }
}

function isNumberKey(evt, obj) {

    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = obj.value;
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
    	if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    	return false;
    return true;
}

function hideStartEndTime(weekNo) {
    $("#"+weekNo+"StartTime").val("");
    $("#"+weekNo+"EndTime").val("");
    $("#"+weekNo+"StartTime").attr("readonly","true");
    $("#"+weekNo+"EndTime").attr("readonly","true");
    $("#"+weekNo+"StartTime").css('pointer-events', 'none');
    $("#"+weekNo+"EndTime").css('pointer-events', 'none');
    $("#"+weekNo+"StartTime").css('background-color', 'grey');
    $("#"+weekNo+"EndTime").css('background-color', 'grey');
}

function fetchFrequencyValues() {
    $('#FrequencyId').empty();
    Visualforce.remoting.Manager.invokeAction(
        'RecurringTasksController.frequencyValues',
        function(result, event) {
            if (event.status) {
                frequencyValues = result;
                $('#FrequencyId').append('<option value="None">--None--</option>');
                for(let item of frequencyValues) {
                    item = item.trim();
                    if(!item) {
                        continue;
                    }
                    $('#FrequencyId').append('<option value="' + item + '">' + item + '</option>');
                }

            } else if (event.type === 'exception') {
            } else {
            }
        },
        {escape: false}
    );
}
//---------------------------------------documentReadyFunction end-------------------------------------------


function fetchBUMetaData() {

    Visualforce.remoting.Manager.invokeAction(
        'RecurringTasksController.getBUMetaData',
        function(result, event) {
            if (event.status) {
                var resultJSON = JSON.parse(result);
                metaDataVarClassVar = JSON.parse(resultJSON.metaDataList);
                userListClassVar    = JSON.parse(resultJSON.userMap);

            } else if (event.type === 'exception') {
            } else {
            }
        },
        {escape: false}
    );

}

//---------------------------------Start of typeOfActivityCompute method------------------------------------------------
function typeOfActivityCompute() {
    hideShowStartEndTime($('#workTypeId :selected').text());
    $('#activityTypeId').find('option').remove();
    $('#activityTypeId').append('<option value="None">--None--</option>');

    var activityTypeSet = new Set();
    $.each(metaDataVarClassVar, function(index, value) {
        if($("#workTypeId").val().toLowerCase() == value.Work_Type__c.toLowerCase()) {
            activityTypeSet.add(value.MasterLabel);
        }
    });

    for(let item of activityTypeSet) {
        item = item.trim();
        if(!item) {
            continue;
        }
        $('#activityTypeId').append('<option value="' + item + '">' + item + '</option>');
    }

}

//---------------------------------End of typeOfActivityCompute method------------------------------------------------


function editEventBUCompute() {

    $('#activityId').find('option').remove();

    $('#activityId').append('<option value="None">--None--</option>');

    if("--None--" == $("#activityTypeId").val()) {
        $('#activityId').val("--None--");
        return;
    }

    var taskTypeSet = new Set();

    $.each(metaDataVarClassVar, function(index, value) {
        if(  $("#workTypeId").val().toLowerCase() == value.Work_Type__c.toLowerCase()
          && $("#activityTypeId").val()
          && $("#activityTypeId").val().toLowerCase() == value.MasterLabel.toLowerCase()
          ) {

            if(value.Activity__c) {
                taskTypeSet.add(value.Activity__c);
            }
        }

    });
	
    for(let item of Array.from(taskTypeSet).sort()) {
        item = item.trim();
        if(!item) {
            continue;
        }
        $('#activityId').append('<option value="' + item + '">' + item + '</option>');
    }

    return taskTypeSet;

}

//---------------------------------End of editEventBUCompute method-------------------------------------------------------------------------

function fetchCustomerGroupRecords() {

    var customerGroupRecList = [];

    $("#customerGroupId").autocomplete({
        minLength: 2,
        source: function(request, response) {

            Visualforce.remoting.Manager.invokeAction(
                'RecurringTasksController.fetchCustomerRecordData',
                $("#customerGroupId").val(),
                function(result, event) {
                    if (event.status) {

                        cityRecList = [];

                        $.each(JSON.parse(result), function(index, value) {

                            var record = {};
                            record.label  = value.Description__c;
                            record.value  = value.Description__c;
                            record.Id     = value.Id;
                            record.concat = value.Name +' - '+ value.Description__c;

                            cityRecList.push(record);
                        });

                        console.log("I cant believe city", cityRecList);
                        response(cityRecList);

                    } else if (event.type === 'exception') {
                    } else {
                    }
                },
                {escape: false}
            );
        },
        select: function(event, ui) {

            console.log("--yoyyo city", ui ,"---", ui.item.label, '-----', ui.item.Id);
            $("#customerGroupId").attr("name", ui.item.Id);
            $("#customerGroupId").attr("data", ui.item.label);
            $("#customerGroupId").attr("data2", ui.item.concat);
        },
        appendTo: "#ui-widgetCustomerGroup"

    });
}

function fetchProvinceRecords() {

    var provinceRecList = [];

    $("#provinceId").autocomplete({
        minLength: 2,
        source: function(request, response) {

            Visualforce.remoting.Manager.invokeAction(
                'RecurringTasksController.fetchProvinceData',
                $("#provinceId").val(),
                function(result, event) {
                    if (event.status) {

                        provinceRecList = [];

                        $.each(JSON.parse(result), function(index, value) {

                            var record = {};
                            record.label = value.text;
                            record.value = value.text;
                            record.Id    = value.value;

                            provinceRecList.push(record);
                        });

                        console.log("I cant believe province", provinceRecList);
                        response(provinceRecList);

                    } else if (event.type === 'exception') {
                    } else {
                    }
                },
                {escape: false}
            );
        },
        select: function(event, ui) {

            console.log("--yoyyo city", ui ,"---", ui.item.label, '-----', ui.item.Id);
            $("#provinceId").attr("name", ui.item.Id);
            $("#provinceId").attr("data", ui.item.label);
        },
        appendTo: "#ui-widgetProvince"

    });
}

function fetchCityRecords() {

    var cityRecList = [];

    $("#cityId").autocomplete({
        minLength: 2,
        source: function(request, response) {
            if((userBU == 'Agro' || userBU == 'GFS') && $("#provinceId").val() == '') {
                $("#provinceId").css("border","1px solid red");
                $("#error").show();
                $("#error").text("Please provide Province to filter City");
            }
            else {
                $("#provinceId").css("border","");
                $("#error").hide();
                $("#error").text("");
                Visualforce.remoting.Manager.invokeAction(
                'RecurringTasksController.fetchCityData',
                $("#cityId").val(),
                $('#provinceId')[0].name,
                function(result, event) {
                    if (event.status) {

                        cityRecList = [];

                        $.each(JSON.parse(result), function(index, value) {

                            var record = {};
                            record.label = value.text;
                            record.value = value.text;
                            record.Id    = value.value;

                            cityRecList.push(record);
                        });

                        console.log("I cant believe city", cityRecList);
                        response(cityRecList);

                    } else if (event.type === 'exception') {
                    } else {
                    }
                },
                {escape: false});
            }
        },
        select: function(event, ui) {

            console.log("--yoyyo city", ui ,"---", ui.item.label, '-----', ui.item.Id);
            $("#cityId").attr("name", ui.item.Id);
            $("#cityId").attr("data", ui.item.label);
        },
        appendTo: "#ui-widgetCity"

    });
}

//---------------------------------------fetchAccountRecords start-------------------------------------------

function fetchAccountRecords() {
    var accRecList = [];

    $("#accountId").autocomplete({
        minLength: 2,
        source: function(request, response) {
            if(  $("#provinceId").val().trim()
               && $("#provinceId").attr("data") != $("#provinceId").val().trim()
               ) {
                $("#error").show();
                $("#error").text("Please select the Province from the dropdown when typing");
                return;
            }

            if(  $("#cityId").val().trim()
               && $("#cityId").attr("data") != $("#cityId").val().trim()
               ) {
                $("#error").show();
                $("#error").text("Please select the City from the dropdown when typing");
                return;
            }

            var cityIdVar;
            var countryGroupVar;
            if($("#cityId").attr("name")) {
               cityIdVar = $("#cityId").attr("name");
            } else {
               cityIdVar = '';
            }

            if($("#customerGroupId").attr("data")) {
               countryGroupVar = $("#customerGroupId").attr("data");
            } else {
               countryGroupVar = '';
            }
            if(userBU == 'GFS' && countryGroupVar == '') {
                $("#customerGroupId").css("border","1px solid red");
                $("#error").show();
                $("#error").text("Please provide Customer Group to filter Accounts");
            }
            else if(userBU == 'Agro' && $("#provinceId").val() == '' && $("#numOfAccId").val() == '') {
                $("#provinceId").css("border","1px solid red");
                $("#cityId").css("border","1px solid red");
                $("#error").show();
                $("#error").text("Please provide Province and City to filter Accounts");
            }
            else if(userBU == 'Agro' && $("#cityId").val() == '' && $("#numOfAccId").val() == '') {
                $("#cityId").css("border","1px solid red");
                $("#error").show();
                $("#error").text("Please provide City to filter Accounts");
            }
            else {
                $("#customerGroupId").css("border","");
                $("#provinceId").css("border","");
                $("#cityId").css("border","");
                $("#error").hide();
                $("#error").text("");
                if(($("#customerGroupId").val()).toLowerCase() == 'others' || $("#numOfAccId").val() != '')
                {
                    // Dont show dropdown for account
                }
                else {
                    Visualforce.remoting.Manager.invokeAction(
                    'RecurringTasksController.fetchAccountData',
                    $("#accountId").val(),
                    String(cityIdVar),
                    String(countryGroupVar),
                    function(result, event) {
                        if (event.status) {

                            accRecList = [];

                            $.each(JSON.parse(result), function(index, value) {

                                var record = {};
                                record.label = value.text;
                                record.value = value.text;
                                record.Id    = value.value;

                                accRecList.push(record);
                            });
                            response(accRecList);

                        }
                        else if (event.type === 'exception') {
                        }
                        else {
                        }
                    },
                    {escape: false});
                }

            }

        },
        select: function(event, ui) {
            $("#accountId").attr("name", ui.item.Id);
            $("#accountId").attr("data", ui.item.label);
            Visualforce.remoting.Manager.invokeAction(
                'RecurringTasksController.populateAccountData',
                ui.item.Id,
                function(result, event) {
                    if (event.status) {
                        $("#FrequencyId").val(result.Frequency_of_DSP_visit_del__c);
                        $("#FrequencyId").attr("name",result.Frequency_of_DSP_visit_del__c);
                        updateWeeks(result.Week_1_Route__c.split(';'),"w1");
                        updateWeeks(result.Week_2_Route__c.split(';'),"w2");
                        updateWeeks(result.Week_3_Route__c.split(';'),"w3");
                        updateWeeks(result.Week_4_Route__c.split(';'),"w4");
                        updateWeeks(result.Week_5_Route__c.split(';'),"w5");
                        unableDisableStartEnd('w1');
                        unableDisableStartEnd('w2');
                        unableDisableStartEnd('w3');
                        unableDisableStartEnd('w4');
                        unableDisableStartEnd('w5');
                        unableDisableStartEnd('w6');
                    } else if (event.type === 'exception') {
                    } else {
                    }
                },
                {escape: false}
            );

        },
        appendTo: "#ui-widgetAcc"

    });
}
//---------------------------------------fetchAccountRecords end-------------------------------------------

function fetchExistingTask() {
    var monthName = getnumMonth(monthDetails.month);
    Visualforce.remoting.Manager.invokeAction(
        'RecurringTasksController.getRecurringTasks',
        monthDetails.year,
        monthName,
        monthDetails.ownerId,
        function(result, event) {
            console.log('...fetchExistingTask: result', result);
            if (event.status) {
                for(var i = 0; i < result.length; i++) {
                    var recordObj = result[i].accountTaskWrapperObj;
                    recordObjList[i] = recordObj;
                    var week1data = recordObjList[i].Account.Week_1_Route.split(';');
                    var week2data = recordObjList[i].Account.Week_2_Route.split(';');
                    var week3data = recordObjList[i].Account.Week_3_Route.split(';');
                    var week4data = recordObjList[i].Account.Week_4_Route.split(';');
                    var week5data = recordObjList[i].Account.Week_5_Route.split(';');
                    var week6data = recordObjList[i].Account.Week_6_Route.split(';');
                    var week1Map = getcheckedWeeks(week1data);
                    var week2Map = getcheckedWeeks(week2data);
                    var week3Map = getcheckedWeeks(week3data);
                    var week4Map = getcheckedWeeks(week4data);
                    var week5Map = getcheckedWeeks(week5data);
                    var week6Map = getcheckedWeeks(week6data);
                    recordObjList[i].week1StartTime = recordObj.week1StartTimeStr ? recordObj.week1StartTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week2StartTime = recordObj.week2StartTimeStr ? recordObj.week2StartTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week3StartTime = recordObj.week3StartTimeStr ? recordObj.week3StartTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week4StartTime = recordObj.week4StartTimeStr ? recordObj.week4StartTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week5StartTime = recordObj.week5StartTimeStr ? recordObj.week5StartTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week6StartTime = recordObj.week6StartTimeStr ? recordObj.week6StartTimeStr.replace('.000Z','') : '';

                    recordObjList[i].week1EndTime = recordObj.week1EndTimeStr ? recordObj.week1EndTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week2EndTime = recordObj.week2EndTimeStr ? recordObj.week2EndTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week3EndTime = recordObj.week3EndTimeStr ? recordObj.week3EndTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week4EndTime = recordObj.week4EndTimeStr ? recordObj.week4EndTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week5EndTime = recordObj.week5EndTimeStr ? recordObj.week5EndTimeStr.replace('.000Z','') : '';
                    recordObjList[i].week6EndTime = recordObj.week6EndTimeStr ? recordObj.week6EndTimeStr.replace('.000Z','') : '';
                    console.log('-------', recordObjList[i]);

                    var accN ;
                    if(recordObj.Account.Name != null && recordObj.Account.Name !=  undefined) {
                        accN = recordObj.Account.Name;
                    }
                    else if(recordObj.Task.accountName != null && recordObj.Task.accountName != undefined){
                        accN = recordObj.Task.accountName;
                    }
                    else {
                        accN = 'None';
                    }
                    if(userBU == 'Agro' && $("#workTypeId").val() == 'Field work' && $("#numOfAccId").val() != '') {
                        accN = 'Multiple Accounts '+ recordObj.Task.numberOfAccounts;
                    }

                    createAccountDivs( accN,
                        recordObj.Task.Description,
                        recordObj.Task.Work_Type,
                        week1Map,
                        week2Map,
                        week3Map,
                        week4Map,
                        week5Map,
                        week6Map);
                }
            } else if (event.type === 'exception') {
            }
        },
        {escape: false}
    );
}


function getcheckedWeeks(data) {
    var returnVal = '';
    for(var itr = 0; itr < data.length ; itr++) {
        if(data[itr] == 'SUNDAY') {
            returnVal += 'Su,'
        }
        else if(data[itr] == 'MONDAY') {
            returnVal += 'Mo,'
        }
        else if(data[itr] == 'TUESDAY') {
            returnVal += 'Tu,'
        }
        else if(data[itr] == 'WEDNESDAY') {
            returnVal += 'Wd,'
        }
        else if(data[itr] == 'THURSDAY') {
            returnVal += 'Th,'
        }
        else if(data[itr] == 'FRIDAY') {
            returnVal += 'Fr,'
        }
        else if(data[itr] == 'SATURDAY') {
            returnVal += 'Sa,'
        }
    }
    returnVal = returnVal.slice(0, -1);
    if(returnVal == '') {
        returnVal = '-';
    }
    return returnVal;
}

function updateWeeks(value,week) {
    for(let item of value) {
        if(item == 'MONDAY') {
            if(!$("#"+week+"Mo").prop('disabled')) {
                $("#"+week+"Mo").prop('checked', true);
            }
        }
        else if(item == 'TUESDAY') {
            if(!$("#"+week+"Tu").prop('disabled')) {
                $("#"+week+"Tu").prop('checked', true);
            }
        }
        else if(item == 'WEDNESDAY') {
            if(!$("#"+week+"Wd").prop('disabled')) {
                $("#"+week+"Wd").prop('checked', true);
            }
        }
        else if(item == 'THURSDAY') {
            if(!$("#"+week+"Th").prop('disabled')) {
                $("#"+week+"Th").prop('checked', true);
            }
        }
        else if(item == 'FRIDAY') {
            if(!$("#"+week+"Fr").prop('disabled')) {
                $("#"+week+"Fr").prop('checked', true);
            }
        }
        else if(item == 'SATURDAY') {
            if(!$("#"+week+"Sa").prop('disabled')) {
                $("#"+week+"Sa").prop('checked', true);
            }
        }
        else if(item == 'SUNDAY') {
            if(!$("#"+week+"Su").prop('disabled')) {
                $("#"+week+"Su").prop('checked', true);
            }
        }
    }
}

//---------------------------------------closeModal start-------------------------------------------
function closeModal() {
    $("#leaveReasonId").val($("#leaveReasonId option:first").val());
    $('#w1StartTime').css("border","");
    $('#w1EndTime').css("border","");
    $('#w2StartTime').css("border","");
    $('#w2EndTime').css("border","");
    $('#w3StartTime').css("border","");
    $('#w3EndTime').css("border","");
    $('#w4StartTime').css("border","");
    $('#w4EndTime').css("border","");
    $('#w5StartTime').css("border","");
    $('#w6EndTime').css("border","");
    $('#w6StartTime').css("border","");
    $('#w6EndTime').css("border","");
    $("#provinceId").removeAttr("data");
    $("#provinceId").removeAttr("name");
    $("#provinceId").val("");
    $("#customerGroupId").val("");
    $("#customerGroupId").removeAttr("data");
    $("#customerGroupId").removeAttr("data2");
    $("#customerGroupId").removeAttr("name");
    $('#customerGroupId').css("border","");
    $('#provinceId').css("border","");
    $('#cityId').css("border","");
    $('#accountId').css("border","");
    $('#activityTypeId').css("border","");
    $('#activityId').css("border","");
    $("#error").hide();
    $("#error").text("");
    rowIndex = -1;
    $('#activityDetail').removeClass('show');
    $('#activityDetail').css("display","none");
    $('#backdrop').removeClass('slds-backdrop--open');
    $('#workTypeId').css("border-color","");
    $('#workTypeErr').css("color","");
    $('#workTypeErr').hide();
    $('#activityTypeId').css("border-color","");
    $('#activityTypeErr').hide();
    $('#activityTypeErr').css("color","");
    $('#activityId').css("border-color","");
    $('#activityErr').hide();
    $('#activityErr').css("color","");
    $('.day').each(function() {
        this.checked = false;
    });
}

function disableStartEnd(weekNo) {
    var enabledStartEnd = false;
        $("#"+weekNo+"Mo")[0].checked;
        $("#"+weekNo+"Tu")[0].checked;
        $("#"+weekNo+"Wd")[0].checked;
        $("#"+weekNo+"Th")[0].checked;
        $("#"+weekNo+"Fr")[0].checked;
        $("#"+weekNo+"Sa")[0].checked;
        $("#"+weekNo+"Su")[0].checked;
}

//---------------------------------------closeModal end-------------------------------------------

//---------------------------------------getData start--------------------------------------------
function getData(index,workType) {
    getDetails(index);
}
//---------------------------------------getData end-----------------------------------------------

//---------------------------------------openModal start-------------------------------------------
function openModal() {
    $('#cityId').val("");
    $('#workTypeId').val('Field work');
    $("#taskComment").val('');
    $("#accountId").val('');
    $("#FrequencyId").val('None');
    $("#numOfAccId").val('');
    $('#activityId').empty();
    $("#accountId").css('background-color', '');
    $("#accountId").removeAttr("readonly");
    $("#accountId").css('pointer-events', 'auto');
    $('#leaveTypeId').val('Full Day');
    $("#targetDiv").hide();
    typeOfActivityCompute();
    unableDisableStartEnd('w1');
    unableDisableStartEnd('w2');
    unableDisableStartEnd('w3');
    unableDisableStartEnd('w4');
    unableDisableStartEnd('w5');
    unableDisableStartEnd('w6');
    $("#newRowBtn").show();
    $("#updateBtn").hide();
    blankPopup();
    $('#activityDetail').addClass('show');
    $('#activityDetail').css("display","block");
    $('#backdrop').addClass('slds-backdrop--open');
    hideShowStartEndTime('Field work');
    fetchCustomerGroupRecords();
    fetchProvinceRecords();
    fetchCityRecords();
    fetchAccountRecords();

    $("#cityId").keyup(function() {

        if($("#cityId").val().trim()) {
        } else {
            $("#cityId").removeAttr("data");
            $("#cityId").removeAttr("name");
        }

    });
    return false;
}
//---------------------------------------openModal end-------------------------------------------

function timeFormat12to24(startEndTime) {
    if(startEndTime != "") {
        try{
            var time = startEndTime;
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if(AMPM == "PM" && hours<12) hours = hours+12;
            if(AMPM == "AM" && hours==12) hours = hours-12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if(hours<10) sHours = "0" + sHours;
            if(minutes<10) sMinutes = "0" + sMinutes;
            return sHours + ":" + sMinutes + ":00";
        }
        catch(err) {
            $("#error").show();
            $("#error").text("Please provide valid Start time and End time");
        }

    }

}


function timeFormat24to12(startEndTime) {
    if(startEndTime === undefined) {
        return null
    }
    else {
        var time = startEndTime;
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        if(time.join ('').length == 9) {
            return '0'+time.join ('').replace('AM',' AM').replace('PM',' PM');
        }
        else {
            return time.join ('').replace('AM',' AM').replace('PM',' PM');
        }
    }
}

//---------------------------------------viewMore start-------------------------------------------
function viewMore(index){
    $('#cityId').val("");
    rowIndex = index;
    $("#newRowBtn").hide();
    if(recordObjList[index].Task.Work_Type == 'Field work' && userBU == 'Agro' && recordObjList[index].Task.numberOfAccounts > '1') {
        $("#updateBtn").hide();

    }
    else {
        $("#updateBtn").show();
    }

    blankPopup();
    $("#workTypeId").val(recordObjList[index].Task.Work_Type);
    typeOfActivityCompute();
    if(recordObjList[index].Task.Work_Type == 'Leave') {
        $("#activityTypeId").val("");
        $("#activityId").val("");
        $("#accountId").val("");
        $("#FrequencyId").val("None");
        $(".accountDiv").hide();
        $("#activityTypeDiv").hide();
        $("#activityDiv").hide();
        $(".leaveDiv").show();
        $('#leaveReasonId').val(recordObjList[index].Task.LeaveReason);
        if(recordObjList[index].Task.LeaveType == 'Half Day') {
            $('#leaveDiv').show();
            $('#leaveTypeId').val('Half Day');
            $('.halfDayleaveDiv').show();
            $('#halfDayleaveTypeId').val(recordObjList[index].Task.HalfDayLeaveType);
        }
        else {
            $('#leaveTypeId').val('Full Day');
            $('.halfDayleaveDiv').hide();
        }
    }
    else if(recordObjList[index].Task.Work_Type == 'Office work') {
        $("#activityTypeId").val(recordObjList[index].activityType);
        editEventBUCompute();
        $("#activityId").val(recordObjList[index].activity);
        $("#accountId").val("");
        $("#FrequencyId").val("None");
        $(".accountDiv").hide();
        $("#activityTypeDiv").show();
        $("#activityDiv").show();
        $(".leaveDiv").hide();
        $('.halfDayleaveDiv').hide();
    }
    else if(recordObjList[index].Task.Work_Type == 'Field work'){
        $("#activityTypeId").val(recordObjList[index].activityType);
        editEventBUCompute();
        $("#activityId").val(recordObjList[index].activity);
        if(recordObjList[index].Account.Name != null) {
            $("#accountId").val(recordObjList[index].Account.Name);
            $("#accountId").attr('data',recordObjList[index].Account.Name);
            $("#accountId").attr('name',recordObjList[index].Account.Id);
            $("#FrequencyId").val(recordObjList[index].Account.Frequency_of_DSP_visit_del);
            $("#accountId").prop('disabled',false);
            $("#accountId").css('background-color', '');
        }
        else if(recordObjList[index].Task.accountName != null) {
            $("#accountId").val(recordObjList[index].Task.accountName);
            $("#accountId").attr('data',recordObjList[index].Task.accountName);
            $("#accountId").prop('disabled',false);
            $("#accountId").css('background-color', '');
        }
        else {
            $("#accountId").prop('disabled',true);
            $("#accountId").css('background-color', 'grey');
            $("#accountId").val(recordObjList[index].Task.accountName);
        }
        if(recordObjList[index].Task.numberOfAccounts != '' && recordObjList[index].Task.numberOfAccounts != undefined) {
            $("#numOfAccId").val(recordObjList[index].Task.numberOfAccounts);
            $("#accountId").css('background-color', 'grey');
            $("#accountId").attr("readonly","true");
            $("#accountId").css('pointer-events', 'none');
        }
        else {
            $("#numOfAccId").val('');
        }
        $(".accountDiv").show();
        $("#activityTypeDiv").show();
        $("#activityDiv").show();
        $(".leaveDiv").hide();
        $('.halfDayleaveDiv').hide();
        if(userBU == 'Agro' && recordObjList[index].Task.Work_Type == 'Field work' && recordObjList[index].Task.numberOfAccounts != '' && recordObjList[index].Task.numberOfAccounts != undefined) {
            $("#freqDiv").hide();
            $("#activityTypeDiv").hide();
            $("#activityDiv").hide();
        }
    }
    if(recordObjList[index].activity == 'Account Sales Call / Order Booking') {
        $("#targetDiv").show();
    }
    else {
        $("#targetDiv").hide();
    }
    if(recordObjList[index].Task.targetRevenue != '' || recordObjList[index].Task.targetRevenue != '0') {
        $("#targetRevenueId").val(recordObjList[index].Task.targetRevenue);
    }
    $("#taskComment").val(recordObjList[index].Task.Description);
    $("#cityId").val(recordObjList[index].Task.taskCity);
    $("#provinceId").val(recordObjList[index].Task.taskProvince);
    $("#customerGroupId").val(recordObjList[index].Task.taskCustomerGroup);

    if(recordObjList[index].Task.taskCity) {
        $("#cityId").prop('disabled',false);
        $("#cityId").css('background-color', '');
        $("#cityId").attr("data",recordObjList[index].Task.taskCity);
        $("#cityId").attr("name",recordObjList[index].Task.taskCityId);
    }
    else {
        $("#cityId").prop('disabled',true);
        $("#cityId").css('background-color', 'grey');
    }

    if(recordObjList[index].Task.taskProvince) {
        $("#provinceId").prop('disabled',false);
        $("#provinceId").css('background-color', '');
        $("#provinceId").attr("data",recordObjList[index].Task.taskProvince);
        $("#provinceId").attr("name",recordObjList[index].Task.taskProvinceId);
    }
    // else {
    //     $("#provinceId").prop('disabled',true);
    //     $("#provinceId").css('background-color', 'grey');
    // }


    $("#customerGroupId").attr("data",recordObjList[index].Task.taskCustomerGroup);
    $("#customerGroupId").attr("name",recordObjList[index].Task.taskCustomerGroupId);

    updateWeeks(recordObjList[index].Account.Week_1_Route.replace(/\{/g, '').replace(/\}/g, '').split(';'),"w1");
    updateWeeks(recordObjList[index].Account.Week_2_Route.replace(/\{/g, '').replace(/\}/g, '').split(';'),"w2");
    updateWeeks(recordObjList[index].Account.Week_3_Route.replace(/\{/g, '').replace(/\}/g, '').split(';'),"w3");
    updateWeeks(recordObjList[index].Account.Week_4_Route.replace(/\{/g, '').replace(/\}/g, '').split(';'),"w4");
    updateWeeks(recordObjList[index].Account.Week_5_Route.replace(/\{/g, '').replace(/\}/g, '').split(';'),"w5");
    updateWeeks(recordObjList[index].Account.Week_6_Route.replace(/\{/g, '').replace(/\}/g, '').split(';'),"w6");
    setStartEndTime(index);
    $('#activityDetail').addClass('show');
    $('#activityDetail').css("display","block");
    $('#backdrop').addClass('slds-backdrop--open');
    unableDisableStartEnd('w1');
    unableDisableStartEnd('w2');
    unableDisableStartEnd('w3');
    unableDisableStartEnd('w4');
    unableDisableStartEnd('w5');
    unableDisableStartEnd('w6');
    fetchCustomerGroupRecords();
    fetchProvinceRecords();
    fetchCityRecords();
    fetchAccountRecords();
}
//---------------------------------------viewMore end-------------------------------------------

//---------------------------------------setStartEndTime start----------------------------------
function setStartEndTime(index) {
    if(recordObjList[index].week1StartTime == recordObjList[index].week1EndTime) {
        $("#w1StartTime").val("");
        $("#w2EndTime").val("");
    }
    else {
        $("#w1StartTime").val(timeFormat24to12(recordObjList[index].week1StartTime));
        $("#w1EndTime").val(timeFormat24to12(recordObjList[index].week1EndTime));
    }

    if(recordObjList[index].week2StartTime == recordObjList[index].week2EndTime) {
        $("#w2StartTime").val("");
        $("#w2EndTime").val("");
    }
    else {
        $("#w2StartTime").val(timeFormat24to12(recordObjList[index].week2StartTime));
        $("#w2EndTime").val(timeFormat24to12(recordObjList[index].week2EndTime));
    }

    if(recordObjList[index].week3StartTime == recordObjList[index].week3EndTime) {
        $("#w3StartTime").val("");
        $("#w3EndTime").val("");
    }
    else {
        $("#w3StartTime").val(timeFormat24to12(recordObjList[index].week3StartTime));
        $("#w3EndTime").val(timeFormat24to12(recordObjList[index].week3EndTime));
    }

    if(recordObjList[index].week4StartTime == recordObjList[index].week4EndTime) {
        $("#w4StartTime").val("");
        $("#w4EndTime").val("");
    }
    else {
        $("#w4StartTime").val(timeFormat24to12(recordObjList[index].week4StartTime));
        $("#w4EndTime").val(timeFormat24to12(recordObjList[index].week4EndTime));
    }

    if(recordObjList[index].week5StartTime == recordObjList[index].week5EndTime) {
        $("#w5StartTime").val("");
        $("#w5EndTime").val("");
    }
    else {
        $("#w5StartTime").val(timeFormat24to12(recordObjList[index].week5StartTime));
        $("#w5EndTime").val(timeFormat24to12(recordObjList[index].week5EndTime));
    }

    if(recordObjList[index].week6StartTime == recordObjList[index].week6EndTime) {
        $("#w6StartTime").val("");
        $("#w6EndTime").val("");
    }
    else {
        $("#w6StartTime").val(timeFormat24to12(recordObjList[index].week6StartTime));
        $("#w6EndTime").val(timeFormat24to12(recordObjList[index].week6EndTime));
    }
}
//---------------------------------------setStartEndTime end------------------------------------

//---------------------------------------redirectToCalendar start------------------------------------
function redirectToCalendar() {

    var monthName = getnumMonth(monthDetails.month);
    localStorage.setItem(
            "monthDetails",
            JSON.stringify(
                {
                    ownerId : monthDetails.ownerId,
                    month : monthDetails.month,
                    year: monthDetails.year
                }
            )
        );
    window.open('/apex/calendar','_self');
}
//---------------------------------------redirectToCalendar end------------------------------------

//---------------------------------------hideShowStartEndTime start-------------------------------------------
function hideShowStartEndTime(value) {
    if(value == 'Leave') {
        $(".accountDiv").hide();
        $("#activityTypeDiv").hide();
        $("#activityDiv").hide();
        $(".leaveDiv").show();
        $(".customerGroup").hide();
        $(".province").hide();
        $(".numOfAccounts").hide();
        $("#w1StartTime").attr("readonly","true");
        $("#w2StartTime").attr("readonly","true");
        $("#w3StartTime").attr("readonly","true");
        $("#w4StartTime").attr("readonly","true");
        $("#w5StartTime").attr("readonly","true");
        $("#w6StartTime").attr("readonly","true");

        $("#w1EndTime").attr("readonly","true");
        $("#w2EndTime").attr("readonly","true");
        $("#w3EndTime").attr("readonly","true");
        $("#w4EndTime").attr("readonly","true");
        $("#w5EndTime").attr("readonly","true");
        $("#w6EndTime").attr("readonly","true");

        $('#w1StartTime').css('disabled', 'true');
        $('#w2StartTime').css('disabled', 'true');
        $('#w3StartTime').css('disabled', 'true');
        $('#w4StartTime').css('disabled', 'true');
        $('#w5StartTime').css('disabled', 'true');
        $('#w6StartTime').css('disabled', 'true');

        $('#w1EndTime').css('disabled', 'true');
        $('#w2EndTime').css('disabled', 'true');
        $('#w3EndTime').css('disabled', 'true');
        $('#w4EndTime').css('disabled', 'true');
        $('#w5EndTime').css('disabled', 'true');
        $('#w6EndTime').css('disabled', 'true');
    }
    else {
        if(value == 'None') {
            $(".accountDiv").hide();
            $("#activityTypeDiv").hide();
            $("#activityDiv").hide();
        }
        else {
            if(value == 'Office work') {
                $(".accountDiv").hide();
                $(".leaveDiv").hide();
                $(".halfDayleaveDiv").hide();
                $(".customerGroup").hide();
                $(".province").hide();
                $(".numOfAccounts").hide();
            }
            else if(value == 'Field work') {
                $(".accountDiv").show();
                $(".leaveDiv").hide();
                $(".halfDayleaveDiv").hide();
                if (userBU == 'GFS') {
                    $(".customerGroup").show();
                    $(".province").show();
                }
                else if(userBU == 'Agro'){
                    $(".customerGroup").hide();
                    $(".province").show();
                    $(".numOfAccounts").show();
                }
            }
            if($("#activityTypeId :selected").text() != '--None--') {
                $("#activityTypeDiv").show();
                $("#activityDiv").show();

            }
            else {
                $('#activityTypeDiv').show();
                $('#workTypeId').show();
                $("#activityDiv").show();
            }
            if(userBU == 'Agro' && value == 'Field work' && $("#numOfAccId").val() != '') {
                $("#freqDiv").hide();
                $("#activityTypeDiv").hide();
                $("#activityDiv").hide();
                $("#cityId").val('');
                $("#provinceId").val('');
                // $("#activityId").val('');
                // $("#activityTypeId").val('');
                // $("#activityTypeId").val('');
                $("#cityId").prop('disabled',true);
                $("#cityId").css('background-color', 'grey');
                // $("#provinceId").prop('disabled',true);
                // $("#provinceId").css('background-color', 'grey');
            }
        }
        $("#w1StartTime").removeAttr("readonly");
        $("#w2StartTime").removeAttr("readonly");
        $("#w3StartTime").removeAttr("readonly");
        $("#w4StartTime").removeAttr("readonly");
        $("#w5StartTime").removeAttr("readonly");
        $("#w6StartTime").removeAttr("readonly");

        $("#w1EndTime").removeAttr("readonly");
        $("#w2EndTime").removeAttr("readonly");
        $("#w3EndTime").removeAttr("readonly");
        $("#w4EndTime").removeAttr("readonly");
        $("#w5EndTime").removeAttr("readonly");
        $("#w6EndTime").removeAttr("readonly");
        disableWeek();
    }
    $("#w1StartTime").val("");
    $("#w2StartTime").val("");
    $("#w3StartTime").val("");
    $("#w4StartTime").val("");
    $("#w5StartTime").val("");
    $("#w6StartTime").val("");
    $("#w1EndTime").val("");
    $("#w2EndTime").val("");
    $("#w3EndTime").val("");
    $("#w4EndTime").val("");
    $("#w5EndTime").val("");
    $("#w6EndTime").val("");
}
//------------------------------------hideShowStartEndTime end----------------------------------------------

//------------------------------------updateHours start----------------------------------------------
function updateHours() {
    if(!confirm("Do you want to save these changes ??")) {
        return;
    }

    $("#error").hide();
    $("#error").text("");
    var index = rowIndex;
    var validationResult = startEndTimeValidation();
    if(validationResult) {
        if($('#workTypeId :selected').text() == "None") {
            $('#workTypeId').css("border-color","red");
            $('#workTypeErr').show();
            $('#workTypeErr').css("color","red");
        }
        else {
            $('#workTypeId').css("border-color","");
            $('#workTypeErr').hide();
            if(($('#activityTypeId :selected').text() == '--None--' && $('#workTypeId :selected').text() != 'Leave' && userBU != 'Agro'  && $("#numOfAccId").val() == '')) {
                $('#activityTypeId').css("border-color","red");
                $('#activityTypeErr').show();
                $('#activityTypeErr').css("color","red");
            }
            else {
                $('#activityTypeId').css("border-color","");
                $('#activityTypeErr').hide();
                if(($('#activityId :selected').text() == '--None--' && $('#activityTypeId :selected').text() != '--None--' && userBU != 'Agro' && $("#numOfAccId").val() == '')) {
                    $('#activityId').css("border-color","red");
                    $('#activityErr').show();
                    $('#activityErr').css("color","red");
                }
                else {
                    $('#activityId').css("border-color","");
                    $('#activityErr').hide();
                    // tableDataUpdate();
                    recordObj = {};
                    recordObj.Account = {};
                    if($("#accountId").attr("name") != undefined) {
                        recordObj.Account.Name = $("#accountId").val();
                        recordObj.Account.Id = $("#accountId").attr("name");
                    }

                    recordObj.Account.Frequency_of_DSP_visit_del = $("#FrequencyId :selected").text() == '--None--' ? '':$("#FrequencyId :selected").text();

                    var week1Map = fetchWeekDays("w1",$("#workTypeId").val());
                    var week2Map = fetchWeekDays("w2",$("#workTypeId").val());
                    var week3Map = fetchWeekDays("w3",$("#workTypeId").val());
                    var week4Map = fetchWeekDays("w4",$("#workTypeId").val());
                    var week5Map = fetchWeekDays("w5",$("#workTypeId").val());
                    var week6Map = fetchWeekDays("w6",$("#workTypeId").val());
                    recordObj.Account.Week_1_Route = week1Map[0];
                    recordObj.Account.Week_2_Route = week2Map[0];
                    recordObj.Account.Week_3_Route = week3Map[0];
                    recordObj.Account.Week_4_Route = week4Map[0];
                    recordObj.Account.Week_5_Route = week5Map[0];
                    recordObj.Account.Week_6_Route = week6Map[0];

                    recordObj.Task = {};
                    if($("#accountId").attr("name") == undefined) {
                        recordObj.Task.accountName = $("#accountId").val();
                    }
                    if($("#numOfAccId").val() != '') {
                        recordObj.Task.numberOfAccounts = $("#numOfAccId").val();
                    }
                    else {
                        recordObj.Task.numberOfAccounts = '';
                    }
                    recordObj.Task.Work_Type = $("#workTypeId").val();
                    if($("#workTypeId").val() == 'Leave') {
                        console.log('reason :: ',$("#leaveReasonId").val());
                        recordObj.Task.LeaveType = $("#leaveTypeId").val();
                        recordObj.Task.LeaveReason = $("#leaveReasonId").val();
                        if($("#leaveTypeId").val() == 'Half Day') {
                            recordObj.Task.HalfDayLeaveType = $("#halfDayleaveTypeId").val();
                            if($("#halfDayleaveTypeId").val() == 'Morning') {
                                recordObj.leaveStartTime = '09:00:00';
                                recordObj.leaveEndTime = '14:00:00';
                            }
                            else {
                                recordObj.leaveStartTime = '14:00:00';
                                recordObj.leaveEndTime = '18:00:00';
                            }
                        }
                        else {
                            recordObj.leaveStartTime = '09:00:00';
                            recordObj.leaveEndTime = '17:00:00';
                        }

                    }
                    recordObj.Task.Description = $("#taskComment").val();

                    recordObj.Task.taskCity = $("#cityId").attr("data");
                    recordObj.Task.taskCityId = $("#cityId").attr("name");

                    recordObj.Task.taskProvince = $("#provinceId").attr("data");
                    recordObj.Task.taskProvinceId = $("#provinceId").attr("name");

                    recordObj.Task.taskCustomerGroup = $("#customerGroupId").attr("data");
                    recordObj.Task.taskCustomerGroupId = $("#customerGroupId").attr("name");

                    if($("#targetRevenueId").val() != '') {
                        recordObj.Task.targetRevenue = $("#targetRevenueId").val();
                    }
                    else {
                        recordObj.Task.targetRevenue = '0';
                    }

                    recordObj.activityType = $("#activityTypeId").val();
                    recordObj.activity = $("#activityId").val();
                    recordObj.week1StartTime = timeFormat12to24($("#w1StartTime").val()) ? timeFormat12to24($("#w1StartTime").val()) : "00:00";
                    recordObj.week1EndTime = timeFormat12to24($("#w1EndTime").val()) ? timeFormat12to24($("#w1EndTime").val()) : "00:00";
                    recordObj.week2StartTime = timeFormat12to24($("#w2StartTime").val()) ? timeFormat12to24($("#w2StartTime").val()) : "00:00";
                    recordObj.week2EndTime = timeFormat12to24($("#w2EndTime").val()) ? timeFormat12to24($("#w2EndTime").val()) : "00:00";
                    recordObj.week3StartTime = timeFormat12to24($("#w3StartTime").val()) ? timeFormat12to24($("#w3StartTime").val()) : "00:00";
                    recordObj.week3EndTime = timeFormat12to24($("#w3EndTime").val()) ? timeFormat12to24($("#w3EndTime").val()) : "00:00";
                    recordObj.week4StartTime = timeFormat12to24($("#w4StartTime").val()) ? timeFormat12to24($("#w4StartTime").val()) : "00:00";
                    recordObj.week4EndTime = timeFormat12to24($("#w4EndTime").val()) ? timeFormat12to24($("#w4EndTime").val()) : "00:00";
                    recordObj.week5StartTime = timeFormat12to24($("#w5StartTime").val()) ? timeFormat12to24($("#w5StartTime").val()) : "00:00";
                    recordObj.week5EndTime = timeFormat12to24($("#w5EndTime").val()) ? timeFormat12to24($("#w5EndTime").val()) : "00:00";
                    recordObj.week6StartTime = timeFormat12to24($("#w6StartTime").val()) ? timeFormat12to24($("#w6StartTime").val()) : "00:00";
                    recordObj.week6EndTime = timeFormat12to24($("#w6EndTime").val()) ? timeFormat12to24($("#w6EndTime").val()) : "00:00";
                    recordObj.index = index;

                    $("#"+index+"-workType").text($("#workTypeId").val());
                    var monthName = getnumMonth(monthDetails.month);
                    $("#blurDivId").attr("style", "display: block");
                    console.log('...update recordObj' , recordObj);
                    console.log('...update taskList' , taskList);
                    var taskToDlt = [];
                    console.log('newTaskList :: ',newTaskList);
                    console.log('recordObjList[index].preTaskList :: ',recordObjList[index].preTaskList);
                    if(recordObjList[index].preTaskList != "" && recordObjList[index].preTaskList != undefined) {
                        taskToDlt = JSON.stringify(recordObjList[index].preTaskList);
                    }
                    else {
                        taskToDlt = newTaskList;
                    }
                    Visualforce.remoting.Manager.invokeAction(
                        'RecurringTasksController.updateNewTasks',
                        JSON.stringify(recordObj),
                        JSON.stringify(taskList),
                        taskToDlt,
                        monthDetails.ownerId,
                        monthDetails.year,
                        monthName,
                        function(result, event) {
                            if (event.status) {
                                if(JSON.parse(result)["overlapping"] == true) {
                                    $("#blurDivId").attr("style", "display: none");
                                    $("#error").show();
                                    if(JSON.parse(result)["workType"] == 'Leave') {
                                        $("#error").text("Leave is present for "+JSON.parse(result)["overlappingDay"]+" of week "+JSON.parse(result)["overlappingWeek"]);
                                    }
                                    else if(JSON.parse(result)["workType"] == 'Other Task') {
                                        $("#error").text("Task is present for "+JSON.parse(result)["overlappingDay"]+" of week "+JSON.parse(result)["overlappingWeek"]);
                                    }
                                    else {
                                        $("#error").text("Another overlapping Task is present for "+JSON.parse(result)["overlappingDay"]+" of week "+JSON.parse(result)["overlappingWeek"]);
                                    }
                                    setTimeout(function() {
                                        $("#error").hide('blind', {}, 1000)
                                    }, 5000);
                                }
                                else {
                                    recordObjList[index] = recordObj;
                                    accountTaskWrapperObj = JSON.parse(result)["accountTaskWrapperObj"];
                                    accountTaskWrapperList[JSON.parse(result)["accountTaskWrapperObj"].index] = accountTaskWrapperObj;
                                    newTaskList = JSON.stringify(JSON.parse(result)["newTaskList"]);
                                    recordObjList[index].preTaskList = JSON.parse(result)["newTaskList"];
                                    if(recordObj.Task.Work_Type == 'Leave' || recordObj.Task.Work_Type == 'Office work') {
                                        $("#accName"+index).text('-');
                                        $("#accFreq"+index).text('-');
                                    }
                                    else {
                                        if($("#accountId").attr("name") != undefined) {
                                            $("#accName"+index).text(recordObj.Account.Name);
                                            $("#accFreq"+index).text($("#FrequencyId :selected").val());
                                        }
                                        else {
                                            $("#accName"+index).text(recordObj.Task.accountName);
                                            $("#accFreq"+index).text($("#FrequencyId :selected").val());
                                        }
                                    }
                                    if(userBU == 'Agro' && $("#workTypeId").val() == 'Field work' && $("#numOfAccId").val() != '') {
                                        $("#accName"+index).text('Multiple Accounts '+ recordObj.Task.numberOfAccounts);
                                    }
                                    $("#accFreq"+index).text(recordObj.Task.Description);
                                    $("#accWrkType"+index).text(recordObj.Task.Work_Type);
                                    $("#accW1"+index).text(week1Map[1]);
                                    $("#accW2"+index).text(week2Map[1]);
                                    $("#accW3"+index).text(week3Map[1]);
                                    $("#accW4"+index).text(week4Map[1]);
                                    $("#accW5"+index).text(week5Map[1]);
                                    $("#accW6"+index).text(week6Map[1]);
                                    fetchMonthStatus(monthDetails.ownerId, monthName, monthDetails.year);
                                    getSummary(result);
                                    closeModal();
                                    $("#accountId").removeAttr("name");
                                    $("#blurDivId").attr("style", "display: none");
                                    $("#error").hide();
                                }

                            } else if (event.type === 'exception') {
                                $("#blurDivId").attr("style", "display: none");
                            } else {
                                $("#blurDivId").attr("style", "display: none");
                            }
                        },
                        {escape: false}
                    );
                }
            }
        }
    }
    else {
        $("#blurDivId").attr("style", "display: none");
    }
}



//---------------------------------------updateHours end--------------------------------------------
//----------------------------------------addNewRow start------------------------------------------

function addNewRow() {
    $("#error").hide();
    $("#error").text("");
    var validationResult = startEndTimeValidation();
    var index = recordObjList.length;
    if(validationResult) {
        if($('#workTypeId :selected').text() == "None") {
            $('#workTypeId').css("border-color","red");
            $('#workTypeErr').show();
            $('#workTypeErr').css("color","red");
        }
        else {
            $('#workTypeId').css("border-color","");
            $('#workTypeErr').hide();
            if(($('#activityTypeId :selected').text() == '--None--' && $('#workTypeId :selected').text() != 'Leave' && userBU != 'Agro' && $("#numOfAccId").val() == '') ) {
                $('#activityTypeId').css("border-color","red");
                $('#activityTypeErr').show();
                $('#activityTypeErr').css("color","red");
            }
            else {
                $('#activityTypeId').css("border-color","");
                $('#activityTypeErr').hide();
                if(($('#activityId :selected').text() == '--None--' && $('#activityTypeId :selected').text() != '--None--' && userBU != 'Agro' && $("#numOfAccId").val() == '')) {
                $('#activityId').css("border-color","red");
                $('#activityErr').show();
                $('#activityErr').css("color","red");
                }
                else {
                    $('#activityId').css("border-color","");
                    $('#activityErr').hide();
                    recordObj = {};
                    recordObj.Account = {};
                    if($("#accountId").attr("name") != undefined) {
                        recordObj.Account.Name = $("#accountId").val();
                        recordObj.Account.Id = $("#accountId").attr("name");
                    }
                    recordObj.Account.Frequency_of_DSP_visit_del = $("#FrequencyId :selected").text() == '--None--' ? '':$("#FrequencyId :selected").text();

                    var week1Map = fetchWeekDays("w1",$("#workTypeId").val());
                    var week2Map = fetchWeekDays("w2",$("#workTypeId").val());
                    var week3Map = fetchWeekDays("w3",$("#workTypeId").val());
                    var week4Map = fetchWeekDays("w4",$("#workTypeId").val());
                    var week5Map = fetchWeekDays("w5",$("#workTypeId").val());
                    var week6Map = fetchWeekDays("w6",$("#workTypeId").val());
                    recordObj.Account.Week_1_Route = week1Map[0];
                    recordObj.Account.Week_2_Route = week2Map[0];
                    recordObj.Account.Week_3_Route = week3Map[0];
                    recordObj.Account.Week_4_Route = week4Map[0];
                    recordObj.Account.Week_5_Route = week5Map[0];
                    recordObj.Account.Week_6_Route = week6Map[0];

                    recordObj.Task = {};
                    if($("#accountId").attr("name") == undefined) {
                        recordObj.Task.accountName = $("#accountId").val();
                    }
                    if($("#numOfAccId").val() != '') {
                        recordObj.Task.numberOfAccounts = $("#numOfAccId").val();
                    }
                    else {
                        recordObj.Task.numberOfAccounts = '';
                    }
                    recordObj.Task.Work_Type = $("#workTypeId").val();
                    if($("#workTypeId").val() == 'Leave') {
                        recordObj.Task.LeaveType = $("#leaveTypeId").val();
                        recordObj.Task.LeaveReason = $("#leaveReasonId").val();
                        if($("#leaveTypeId").val() == 'Half Day') {
                            recordObj.Task.HalfDayLeaveType = $("#halfDayleaveTypeId").val();
                            if($("#halfDayleaveTypeId").val() == 'Morning') {
                                recordObj.leaveStartTime = '09:00:00';
                                recordObj.leaveEndTime = '14:00:00';
                            }
                            else {
                                recordObj.leaveStartTime = '14:00:00';
                                recordObj.leaveEndTime = '18:00:00';
                            }
                        }
                        else {
                            recordObj.leaveStartTime = '09:00:00';
                            recordObj.leaveEndTime = '17:00:00';
                        }

                    }
                    recordObj.Task.Description = $("#taskComment").val();

                    recordObj.Task.taskCity = $("#cityId").attr("data");
                    recordObj.Task.taskCityId = $("#cityId").attr("name");

                    recordObj.Task.taskProvince = $("#provinceId").attr("data");
                    recordObj.Task.taskProvinceId = $("#provinceId").attr("name");

                    recordObj.Task.taskCustomerGroup = $("#customerGroupId").attr("data");
                    recordObj.Task.taskCustomerGroupId = $("#customerGroupId").attr("name");

                    if($("#targetRevenueId").val() != '') {
                        recordObj.Task.targetRevenue = $("#targetRevenueId").val();
                    }
                    else {
                        recordObj.Task.targetRevenue = '0';
                    }

                    recordObj.activityType = $("#activityTypeId").val();
                    recordObj.activity = $("#activityId").val();
                    recordObj.week1StartTime = timeFormat12to24($("#w1StartTime").val()) ? timeFormat12to24($("#w1StartTime").val()) : "00:00";
                    recordObj.week1EndTime = timeFormat12to24($("#w1EndTime").val()) ? timeFormat12to24($("#w1EndTime").val()) : "00:00";
                    recordObj.week2StartTime = timeFormat12to24($("#w2StartTime").val()) ? timeFormat12to24($("#w2StartTime").val()) : "00:00";
                    recordObj.week2EndTime = timeFormat12to24($("#w2EndTime").val()) ? timeFormat12to24($("#w2EndTime").val()) : "00:00";
                    recordObj.week3StartTime = timeFormat12to24($("#w3StartTime").val()) ? timeFormat12to24($("#w3StartTime").val()) : "00:00";
                    recordObj.week3EndTime = timeFormat12to24($("#w3EndTime").val()) ? timeFormat12to24($("#w3EndTime").val()) : "00:00";
                    recordObj.week4StartTime = timeFormat12to24($("#w4StartTime").val()) ? timeFormat12to24($("#w4StartTime").val()) : "00:00";
                    recordObj.week4EndTime = timeFormat12to24($("#w4EndTime").val()) ? timeFormat12to24($("#w4EndTime").val()) : "00:00";
                    recordObj.week5StartTime = timeFormat12to24($("#w5StartTime").val()) ? timeFormat12to24($("#w5StartTime").val()) : "00:00";
                    recordObj.week5EndTime = timeFormat12to24($("#w5EndTime").val()) ? timeFormat12to24($("#w5EndTime").val()) : "00:00";
                    recordObj.week6StartTime = timeFormat12to24($("#w6StartTime").val()) ? timeFormat12to24($("#w6StartTime").val()) : "00:00";
                    recordObj.week6EndTime = timeFormat12to24($("#w6EndTime").val()) ? timeFormat12to24($("#w6EndTime").val()) : "00:00";
                    recordObj.index = index;

                    $("#"+index+"-workType").text($("#workTypeId").val());
                    var monthName = getnumMonth(monthDetails.month);
                    $("#blurDivId").attr("style", "display: block");
                    console.log('...recordObj' , recordObj);
                    console.log('...taskList' , taskList);
                    Visualforce.remoting.Manager.invokeAction(
                        'RecurringTasksController.createNewTasks',
                        JSON.stringify(recordObj),
                        JSON.stringify(taskList),
                        monthDetails.ownerId,
                        monthDetails.year,
                        monthName,
                        function(result, event) {
                            if (event.status) {
                                if(JSON.parse(result)["overlapping"] == true) {
                                    $("#blurDivId").attr("style", "display: none");
                                    $("#error").show();
                                    if(JSON.parse(result)["workType"] == 'Leave') {
                                        $("#error").text("Leave is present for "+JSON.parse(result)["overlappingDay"]+" of week "+JSON.parse(result)["overlappingWeek"]);
                                    }
                                    else if(JSON.parse(result)["workType"] == 'Other Task') {
                                        $("#error").text("Task is present for "+JSON.parse(result)["overlappingDay"]+" of week "+JSON.parse(result)["overlappingWeek"]);
                                    }
                                    else {
                                        $("#error").text("Another overlapping Task is present for "+JSON.parse(result)["overlappingDay"]+" of week "+JSON.parse(result)["overlappingWeek"]);
                                    }
                                    setTimeout(function() {
                                        $("#error").hide('blind', {}, 1000)
                                    }, 5000);
                                }
                                else {
                                    recordObjList[index] = recordObj;
                                    accountTaskWrapperObj = JSON.parse(result)["accountTaskWrapperObj"];
                                    accountTaskWrapperList[JSON.parse(result)["accountTaskWrapperObj"].index] = accountTaskWrapperObj;
                                    newTaskList = JSON.stringify(JSON.parse(result)["newTaskList"]);
                                    fetchMonthStatus(monthDetails.ownerId, monthName, monthDetails.year);
                                    getSummary(result);
                                    var accN ;
                                    if($("#accountId").attr("name") != undefined) {
                                        accN = recordObj.Account.Name;
                                    }
                                    else {
                                        accN = recordObj.Task.accountName;
                                    }
                                    if(userBU == 'Agro' && $("#workTypeId").val() == 'Field work' && $("#numOfAccId").val() != '') {
                                        accN = 'Multiple Accounts '+ recordObj.Task.numberOfAccounts;
                                    }
                                    createAccountDivs( accN,
                                                   recordObj.Task.Description,
                                                   recordObj.Task.Work_Type,
                                                   week1Map[1],
                                                   week2Map[1],
                                                   week3Map[1],
                                                   week4Map[1],
                                                   week5Map[1],
                                                   week6Map[1]);
                                    closeModal();
                                    $("#accountId").removeAttr("name");
                                    $("#blurDivId").attr("style", "display: none");
                                    $("#error").hide();
                                }
                            } else if (event.type === 'exception') {
                                $("#blurDivId").attr("style", "display: none");
                            } else {
                                $("#blurDivId").attr("style", "display: none");
                            }
                        },
                        {escape: false}
                    );
                }
            }
        }
    }
    else {
        $("#blurDivId").attr("style", "display: none");
    }
}
//----------------------------------------addNewRow end------------------------------------------


//----------------------------------------dailyHours start-------------------------------------------
function dailyHours(result) {
    var totalHours = 0;
    totalOfficeHours = JSON.stringify(JSON.parse(result)["totalOffWorkHrs"]);
    totalFieldHours = JSON.stringify(JSON.parse(result)["totalFieldWorkHrs"]);
    totalLeaveHours = JSON.stringify(JSON.parse(result)["totalLeaveHrs"]);
    totalWorkDaysCompleted = JSON.stringify(JSON.parse(result)["totalWorkDaysCompleted"]);
    totalHours = parseFloat(totalFieldHours) + parseFloat(totalLeaveHours) + parseFloat(totalOfficeHours);
    //dailyHoursList =
    $("#totalWorkDaysId").text(totalWorkDaysCompleted +'/'+totalWorkDays);
    $("#totalOffWorkHoursId").text(totalOfficeHours);
    $("#totalFieldWorkHoursId").text(totalFieldHours);
    $("#totalLeaveHoursId").text(totalLeaveHours);
    $("#totalWorkHoursId").text(parseFloat(totalHours)+'/'+totalWorkDays*8);
    if(totalWorkDaysCompleted != 0) {
        $("#totalWorkDaysPercent").text((totalWorkDaysCompleted/totalWorkDays*100).toFixed(2)+'%');
    }
    if(totalHours != 0) {
        $("#totalWorkHoursPercent").text((parseFloat(totalHours)/(totalWorkDays*8)*100).toFixed(2)+'%');
    }
    if(totalOfficeHours != 0) {
        $("#totalOffHrsPercent").text((totalOfficeHours/(totalWorkDays*8)*100).toFixed(2)+'%');
    }
    if(totalFieldHours != 0) {
        $("#totalFieldHrsPercent").text((totalFieldHours/(totalWorkDays*8)*100).toFixed(2)+'%');
    }
    if(totalLeaveHours != 0) {
        $("#totalLeaveHrsPercent").text((totalLeaveHours/(totalWorkDays*8)*100).toFixed(2)+'%');
    }



}
//----------------------------------------dailyHours end---------------------------------------------

function fetchMonthStatus(selectedUserID, selectedMonth, selectedYear) {

    $("#blurDivId").attr("style", "display: block");

    Visualforce.remoting.Manager.invokeAction(
        'RecurringTasksController.fetchMonthStatus',
        selectedMonth,
        selectedYear,
        selectedUserID,
        function(result, event) {
            if (event.status) {
                console.log('success in remoting monthfetch--');

                var resultJSON = JSON.parse(result);
                monthJSONClass = resultJSON;
                console.log('--now see the result on hold--', resultJSON.Total_Work_Days_Completed__c, ":::", selectedMonth);

                setMonthStatusParameters(resultJSON, selectedMonth, selectedUserID);

                $("#blurDivId").attr("style", "display: none");

            } else if (event.type === 'exception') {
                $("#blurDivId").attr("style", "display: none");
            } else {
                $("#blurDivId").attr("style", "display: none");
            }
        },
        {escape: false}
    );

}

function setMonthStatusParameters(resultJSON, selectedMonth, selectedUserID) {
    var selectedMonth = selectedMonth;

    var totalWorkDaysComVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Days_Completed__c)
                            ? resultJSON[0].Total_Work_Days_Completed__c
                            : "0";

    var totalWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Days__c)
                         ? resultJSON[0].Total_Work_Days__c
                         : "0";

    var totalWorkHrsComVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Hours_Completed__c)
                           ? resultJSON[0].Total_Work_Hours_Completed__c
                           : "0";

    var totalWorkHrsVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Work_Hours__c)
                        ? resultJSON[0].Total_Work_Hours__c
                        : "0";

    var totalOfficeWorkVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Office_Work_Hours__c)
                           ? resultJSON[0].Total_Office_Work_Hours__c
                           : "0";

    var totalFieldWorkVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Field_Work_Hours__c)
                          ? resultJSON[0].Total_Field_Work_Hours__c
                          : "0";

    var totalLeaveVar = (resultJSON && resultJSON[0] && resultJSON[0].Total_Leave_Hours__c)
                      ? resultJSON[0].Total_Leave_Hours__c
                      : "0";

    var totalOfficeWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Office_Work_Days__c)
                               ? resultJSON[0].Office_Work_Days__c
                               : "0";

    var totalFieldWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Field_Work_Days__c)
                              ? resultJSON[0].Field_Work_Days__c
                              : "0";

    var totalLeaveWorkDaysVar = (resultJSON && resultJSON[0] && resultJSON[0].Leave_Days__c)
                              ? resultJSON[0].Leave_Days__c
                              : "0";
    var totalTime     = totalWorkDaysVar * 8;
    var workDaysPer   = 0;
    var workHrsPer    = 0;
    var officeDaysPer = 0;
    var officeHrsPer  = 0;
    var fieldHrsPer   = 0;
    var leaveHrsPer   = 0;
    var outletsCovered   = (resultJSON && resultJSON[0] && resultJSON[0].Outlets_Covered__c)
                              ? resultJSON[0].Outlets_Covered__c
                              : "0";

     if(totalWorkDaysVar && totalWorkDaysVar != 0) {
        workDaysPer   = (totalWorkDaysComVar / totalWorkDaysVar) * 100;
        workHrsPer    = (totalWorkHrsComVar / totalWorkHrsVar) * 100;
        officeDaysPer = (totalOfficeWorkDaysVar / totalWorkDaysVar) * 100;
        officeHrsPer  = (totalOfficeWorkVar / totalTime) * 100;
        fieldDaysPer  = (totalFieldWorkDaysVar / totalWorkDaysVar) * 100;
        fieldHrsPer   = (totalFieldWorkVar / totalTime) * 100;
        leaveDaysPer  = (totalLeaveWorkDaysVar / totalWorkDaysVar) * 100;
        leaveHrsPer   = (totalLeaveVar / totalTime) * 100;
    } else {
        workDaysPer   = 0;
        workHrsPer    = 0;
        officeDaysPer = 0;
        officeHrsPer  = 0;
        fieldDaysPer  = 0;
        fieldHrsPer   = 0;
        leaveDaysPer  = 0;
        leaveHrsPer   = 0;
    }

    var setterJSON = {};
    setterJSON = { totalWorkDaysComVar    : totalWorkDaysComVar
                 , totalWorkDaysVar       : totalWorkDaysVar
                 , totalWorkHrsComVar     : totalWorkHrsComVar
                 , totalWorkHrsVar        : totalWorkHrsVar
                 , workDaysPer            : workDaysPer
                 , workHrsPer             : workHrsPer
                 , totalOfficeWorkDaysVar : totalOfficeWorkDaysVar
                 , totalOfficeWorkVar     : totalOfficeWorkVar
                 , officeDaysPer          : officeDaysPer
                 , officeHrsPer           : officeHrsPer
                 , totalFieldWorkDaysVar  : totalFieldWorkDaysVar
                 , totalFieldWorkVar      : totalFieldWorkVar
                 , fieldDaysPer           : fieldDaysPer
                 , fieldHrsPer            : fieldHrsPer
                 , totalLeaveWorkDaysVar  : totalLeaveWorkDaysVar
                 , totalLeaveVar          : totalLeaveVar
                 , leaveDaysPer           : leaveDaysPer
                 , leaveHrsPer            : leaveHrsPer
                 , outletsCovered         : outletsCovered
                 };
    setMonthDivValues(setterJSON);
}





function setMonthDivValues(setterJSON) {

    if(setterJSON) {

        ////////////////////////////////////Month/////////////////////////////////////////////////////
        if(setterJSON.totalWorkDaysComVar && setterJSON.totalWorkDaysVar) {
            $("#workDaysId").html(setterJSON.totalWorkDaysComVar + "/" + setterJSON.totalWorkDaysVar);
        } else {
            $("#workDaysId").html("");
        }

        if(setterJSON.totalWorkHrsComVar && setterJSON.totalWorkHrsVar) {
            $("#workHoursId").html(setterJSON.totalWorkHrsComVar + "/" + setterJSON.totalWorkHrsVar);
        } else {
            $("#workHoursId").html("");
        }

        if(setterJSON.workDaysPer) {
            $("#perDaysId").text((setterJSON.workDaysPer).toFixed(2) + " %");
        } else {
            $("#perDaysId").text("0 %");
        }

        if(setterJSON.workHrsPer) {
            $("#perHoursId").text((setterJSON.workHrsPer).toFixed(2) + " %");
        } else {
            $("#perHoursId").text("0 %");
        }

        //////////////////////////////////////////Office///////////////////////////////////////////////

        if(setterJSON.totalOfficeWorkDaysVar) {
            $("#officeDaysId").text((setterJSON.totalOfficeWorkDaysVar));
        } else {
            $("#officeDaysId").text("");
        }

        if(setterJSON.totalOfficeWorkVar) {
            $("#officeHoursId").text((setterJSON.totalOfficeWorkVar));
        } else {
            $("#officeHoursId").text("");
        }

        if(setterJSON.officeDaysPer) {
            $("#officePerDaysId").text((setterJSON.officeDaysPer).toFixed(2) + " %");
        } else {
            $("#officePerDaysId").text("0 %");
        }

        if(setterJSON.officeHrsPer) {
            $("#officePerHoursId").text((setterJSON.officeHrsPer).toFixed(2) + " %");
        } else {
            $("#officePerHoursId").text("0 %");
        }


        //////////////////////////////////////////Field///////////////////////////////////////////////


        if(setterJSON.totalFieldWorkDaysVar) {
            $("#fieldDaysId").text((setterJSON.totalFieldWorkDaysVar));
        } else {
            $("#fieldDaysId").text("");
        }

        if(setterJSON.totalFieldWorkVar) {
            $("#fieldHoursId").text((setterJSON.totalFieldWorkVar));
        } else {
            $("#fieldHoursId").text("");
        }

        if(setterJSON.fieldDaysPer) {
            $("#fieldPerDaysId").text((setterJSON.fieldDaysPer).toFixed(2) + " %");
        } else {
            $("#fieldPerDaysId").text("0 %");
        }

        if(setterJSON.fieldHrsPer) {
            $("#fieldPerHoursId").text((setterJSON.fieldHrsPer).toFixed(2) + " %");
        } else {
            $("#fieldPerHoursId").text("0 %");
        }

        if(userBU != 'GFS') {
            $("#outletsCoveredId").text((setterJSON.outletsCovered));
        }
        else {
            $("#outletDivId").hide()
            $("#outletsCoveredId").text('0');
        }
        ///////////////////////////////////////Leave//////////////////////////////////////////////


        if(setterJSON.totalLeaveWorkDaysVar) {
            $("#leaveDaysId").text((setterJSON.totalLeaveWorkDaysVar));
        } else {
            $("#leaveDaysId").text("");
        }

        if(setterJSON.totalLeaveVar) {
            $("#leaveHoursId").text((setterJSON.totalLeaveVar));
        } else {
            $("#leaveHoursId").text("");
        }

        if(setterJSON.leaveDaysPer) {
            $("#leavePerDaysId").text((setterJSON.leaveDaysPer).toFixed(2) + " %");
        } else {
            $("#leavePerDaysId").text("0 %");
        }

        if(setterJSON.leaveHrsPer) {
            $("#leavePerHoursId").text((setterJSON.leaveHrsPer).toFixed(2) + " %");
        } else {
            $("#leavePerHoursId").text("0 %");
        }

    }
}
//----------------------------------------blankPopup start------------------------------------------
function blankPopup() {
    $("#workTypeId").val("Field work");
    if(userBU == 'Agro') {
        $("#cityId").prop('disabled',true);
        $("#cityId").css('background-color', 'grey');
        $("#accountId").prop('disabled',true);
        $("#accountId").css('background-color', 'grey');
    }
    $('#customerGroupId').val("");
    $('#provinceId').val("");
    $('#cityId').val("");
    uncheckWeek('w1');
    uncheckWeek('w2');
    uncheckWeek('w3');
    uncheckWeek('w4');
    uncheckWeek('w5');
    uncheckWeek('w6');
    $("#w1StartTime").val("");
    $("#w2StartTime").val("");
    $("#w3StartTime").val("");
    $("#w4StartTime").val("");
    $("#w5StartTime").val("");
    $("#w6StartTime").val("");
    $("#w1EndTime").val("");
    $("#w2EndTime").val("");
    $("#w3EndTime").val("");
    $("#w4EndTime").val("");
    $("#w5EndTime").val("");
    $("#w6EndTime").val("");

}
//----------------------------------------blankPopup end---------------------------------------

function uncheckWeek(week) {
    $("#"+week+"Mo")[0].checked = false;
    $("#"+week+"Tu")[0].checked = false;
    $("#"+week+"Wd")[0].checked = false;
    $("#"+week+"Th")[0].checked = false;
    $("#"+week+"Fr")[0].checked = false;
    $("#"+week+"Sa")[0].checked = false;
    $("#"+week+"Su")[0].checked = false;
}

//----------------------------------------fetchWeekDays start---------------------------------------
function fetchWeekDays(weekId,wrkType) {
    var week = '{';
    var weekDays = '';
    if($("#"+weekId+"Mo")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'MONDAY;';
        weekDays += 'Mo,';
    }
    if($("#"+weekId+"Tu")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'TUESDAY;';
        weekDays += 'Tu,';
    }
    if($("#"+weekId+"Wd")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'WEDNESDAY;';
        weekDays += 'Wd,';
    }
    if($("#"+weekId+"Th")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'THURSDAY;';
        weekDays += 'Th,';
    }
    if($("#"+weekId+"Fr")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'FRIDAY;';
        weekDays += 'Fr,';
    }
    if($("#"+weekId+"Sa")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'SATURDAY;';
        weekDays += 'Sa,';

    }
    if($("#"+weekId+"Su")[0].checked == true &&
        (($("#"+weekId+"StartTime").val() != '' &&
         $("#"+weekId+"EndTime").val() != '') ||
         wrkType == 'Leave')) {
        week += 'SUNDAY;';
        weekDays += 'Su,';
    }

    week = week.slice(0, -1);
    week += '}';
    weekDays = weekDays.slice(0, -1);
    if(weekDays == '') {
        weekDays = '-';
    }
    var weekdaysMap = {};
    weekdaysMap[0] = week;
    weekdaysMap[1] = weekDays;
    return weekdaysMap;
}
//----------------------------------------fetchWeekDays end---------------------------------------

//---------------------------------------displayActivity start-------------------------------------------
function displayActivity(activityType) {
    if(activityType == 'None') {
        $('#activityDiv').hide();
    }
    else {
        $('#activityDiv').show();
    }
}
//---------------------------------------displayActivity end-------------------------------------------

//---------------------------------------startEndTimeValidation start----------------------------------
function startEndTimeValidation() {
    var checkedBoxes = 0;
    var checkedBoxes1 = 0;
    var checkedBoxes2 = 0;
    var checkedBoxes3 = 0;
    var checkedBoxes4 = 0;
    var checkedBoxes5 = 0;
    var checkedBoxes6 = 0;
    var enabledW1 = 0;
    var enabledW2 = 0;
    var enabledW3 = 0;
    var enabledW4 = 0;
    var enabledW5 = 0;
    var enabledW6 = 0;
    var freqValue = 0;
    if($("#FrequencyId :selected").val() == 'F4 - WEEKLY') {
        freqValue = 1;
    }
    else if($("#FrequencyId :selected").val() == 'F8 - TWICE A WEEK') {
        freqValue = 2;
    }
    else if($("#FrequencyId :selected").val() == 'F12 - 3 TIMES A WEEK') {
        freqValue = 3;
    }
    else if($("#FrequencyId :selected").val() == 'F16 - 4 TIMES A WEEK') {
        freqValue = 4;
    }
    else if($("#FrequencyId :selected").val() == 'F20 - 5 TIMES A WEEK') {
        freqValue = 5;
    }
    else if($("#FrequencyId :selected").val() == 'F24 - 6 TIMES A WEEK') {
        freqValue = 6;
    }
    else if($("#FrequencyId :selected").val() == 'DAILY- DAILY') {
        freqValue = 7;
    }

    var freqErr1 = false;
    var freqErr2 = false;
    var freqErr3 = false;
    var freqErr4 = false;
    var freqErr5 = false;
    var freqErr6 = false;

    $('input[type=checkbox]').each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes++;
        }
    });

    $(".week1 .day").each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes1++;
        }
        if(!this.disabled){
            enabledW1++;
        }
    });
    $(".week2 .day").each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes2++;
        }
        if(!this.disabled){
            enabledW2++;
        }
    });
    $(".week3 .day").each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes3++;
        }
        if(!this.disabled){
            enabledW3++;
        }
    });
    $(".week4 .day").each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes4++;
        }
        if(!this.disabled){
            enabledW4++;
        }
    });
    $(".week5 .day").each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes5++;
        }
        if(!this.disabled){
            enabledW5++;
        }
    });
    $(".week6 .day").each(function () {
        var sThisVal = (this.checked ? 1 : 0);
        if(sThisVal == 1) {
            checkedBoxes6++;
        }
        if(!this.disabled){
            enabledW6++;
        }
    });


    if(((enabledW1 >= freqValue) && checkedBoxes1 == freqValue) || ((enabledW1 < freqValue) && checkedBoxes1 == enabledW1)) {
        freqErr1 = false;
        console.log('if 1');
    }
    else {
        freqErr1 = true;
        console.log('else 1');
    }
    if(((enabledW2 >= freqValue) && checkedBoxes2 == freqValue) || ((enabledW2 < freqValue) && checkedBoxes2 == enabledW2)) {
        freqErr2 = false;
        console.log('if 2');
    }
    else {
        freqErr2 = true;
        console.log('else 2');
    }
    if(((enabledW3 >= freqValue) && checkedBoxes3 == freqValue) || ((enabledW3 < freqValue) && checkedBoxes3 == enabledW3)) {
        freqErr3 = false;
        console.log('if 3');
    }
    else {
        freqErr3 = true;
        console.log('else 3');
    }
    if(((enabledW4 >= freqValue) && checkedBoxes4 == freqValue) || ((enabledW4 < freqValue) && checkedBoxes4 == enabledW4)) {
        freqErr4 = false;
        console.log('if 4');
    }
    else {
        freqErr4 = true;
        console.log('else 4');
    }
    if(((enabledW5 >= freqValue) && checkedBoxes5 == freqValue) || ((enabledW5 < freqValue) && checkedBoxes5 == enabledW5)) {
        freqErr5 = false;
        console.log('if 5');
    }
    else {
        freqErr5 = true;
        console.log('if 1');
    }
    if(((enabledW6 >= freqValue) && checkedBoxes6 == freqValue) || ((enabledW6 < freqValue) && checkedBoxes6 == enabledW6)) {
        freqErr6 = false;
    }
    else {
        freqErr6 = true;
    }
    var w1StartTime = timeFormat12to24($("#w1StartTime").val());
    var w2StartTime = timeFormat12to24($("#w2StartTime").val());
    var w3StartTime = timeFormat12to24($("#w3StartTime").val());
    var w4StartTime = timeFormat12to24($("#w4StartTime").val());
    var w5StartTime = timeFormat12to24($("#w5StartTime").val());
    var w6StartTime = timeFormat12to24($("#w6StartTime").val());
    var w1EndTime = timeFormat12to24($("#w1EndTime").val());
    var w2EndTime = timeFormat12to24($("#w2EndTime").val());
    var w3EndTime = timeFormat12to24($("#w3EndTime").val());
    var w4EndTime = timeFormat12to24($("#w4EndTime").val());
    var w5EndTime = timeFormat12to24($("#w5EndTime").val());
    var w6EndTime = timeFormat12to24($("#w6EndTime").val());

    var week1Checked = checkWeekValues('w1');
    var week2Checked = checkWeekValues('w2');
    var week3Checked = checkWeekValues('w3');
    var week4Checked = checkWeekValues('w4');
    var week5Checked = checkWeekValues('w5');
    var week6Checked = checkWeekValues('w6');

    var accountSetName = $("#accountId").attr("data");
    var accountName    = $("#accountId").val();
    var accountId      = "";

    if(accountName) {
        accountId = $("#accountId").get(0).name;
    } else {
        $("#accountId").attr("name", "");
    }

    var timeError = false;
    if(week1Checked && (((w1StartTime >= w1EndTime && w1StartTime != '' && w1EndTime != '')
     || (w1StartTime == '' && w1EndTime != '')
     || (w1StartTime != '' && w1EndTime == ''))
     || (w1StartTime == undefined || w1EndTime == undefined)
     && ($("#workTypeId").val() != 'Leave'))){
        $('#w1StartTime').css("border","1px solid red");
        $('#w1EndTime').css("border","1px solid red");
        timeError = true;
    }
    if(week2Checked && (((w2StartTime >= w2EndTime && w2StartTime != '' && w2EndTime != '')
          || (w2StartTime == '' && w2EndTime != '')
          || (w2StartTime != '' && w2EndTime == ''))
          || (w2StartTime == undefined || w2EndTime == undefined)
          && ($("#workTypeId").val() != 'Leave'))){
        $('#w2StartTime').css("border","1px solid red");
        $('#w2EndTime').css("border","1px solid red");
        timeError = true;
    }
    if(week3Checked && (((w3StartTime >= w3EndTime && w3StartTime != '' && w3EndTime != '')
          || (w3StartTime == '' && w3EndTime != '')
          || (w3StartTime != '' && w3EndTime == ''))
          || (w3StartTime == undefined || w3EndTime == undefined)
          && ($("#workTypeId").val() != 'Leave'))){
        $('#w3StartTime').css("border","1px solid red");
        $('#w3EndTime').css("border","1px solid red");
        timeError = true;
    }
    if(week4Checked && (((w4StartTime >= w4EndTime && w4StartTime != '' && w4EndTime != '')
          || (w4StartTime == '' && w4EndTime != '')
          || (w4StartTime != '' && w4EndTime == ''))
          || (w4StartTime == undefined || w4EndTime == undefined)
          && ($("#workTypeId").val() != 'Leave'))){
        $('#w4StartTime').css("border","1px solid red");
        $('#w4EndTime').css("border","1px solid red");
        timeError = true;
    }
    if(week5Checked && (((w5StartTime >= w5EndTime && w5StartTime != '' && w5EndTime != '')
          || (w5StartTime == '' && w5EndTime != '')
          || (w5StartTime != '' && w5EndTime == ''))
          || (w5StartTime == undefined || w5EndTime == undefined)
          && ($("#workTypeId").val() != 'Leave'))){
        $('#w5StartTime').css("border","1px solid red");
        $('#w5EndTime').css("border","1px solid red");
        timeError = true;
    }
    if(week6Checked && (((w6StartTime >= w6EndTime && w6StartTime != '' && w6EndTime != '')
          || (w6StartTime == '' && w6EndTime != '')
          || (w6StartTime != '' && w6EndTime == ''))
          || (w6StartTime == undefined || w6EndTime == undefined)
          && ($("#workTypeId").val() != 'Leave'))){
        $('#w6StartTime').css("border","1px solid red");
        $('#w6EndTime').css("border","1px solid red");
        timeError = true;
    }

    if(($('#activityTypeId').val() == 'None' || $('#activityTypeId').val() == null) && $('#workTypeId').val() != 'Leave' && userBU == 'GFS') {
        $('#accountId').css("border","");
        $('#activityTypeId').focus();
        $('#activityTypeId').css("border","1px solid red");
        $("#error").show();
        $("#error").text("Please fill required fields");
        return false;
    }

    if(($('#activityTypeId').val() == 'None' || $('#activityTypeId').val() == null) && ($('#workTypeId').val() == 'Field work' && userBU == 'Agro' && $("#numOfAccId").val() == '')) {
        $('#accountId').css("border","");
        $('#activityTypeId').focus();
        $('#activityTypeId').css("border","1px solid red");
        $("#error").show();
        $("#error").text("Please fill required fields");
        return false;
    }
    else if(($('#activityId').val() == 'None' || $('#activityId').val() == null) && $('#workTypeId').val() != 'Leave' && userBU == 'GFS') {
        $('#activityTypeId').focus();
        $('#activityTypeId').css("border","");
        $('#activityId').css("border","1px solid red");
        $("#error").show();
        $("#error").text("Please fill required fields");
        return false;
    }
    else if(($('#activityId').val() == 'None' || $('#activityId').val() == null) && $('#workTypeId').val() == 'Field work' && userBU == 'Agro' && $("#numOfAccId").val() == '') {
        $('#activityTypeId').focus();
        $('#activityTypeId').css("border","");
        $('#activityId').css("border","1px solid red");
        $("#error").show();
        $("#error").text("Please fill required fields");
        return false;
    }
    else if(timeError == true) {
        $("#error").show();
        $("#error").text("Please provide valid Start time and End time");
        return false;
    }
    else if( !week1Checked && !week2Checked && !week3Checked && !week4Checked && !week5Checked && !week6Checked) {
        $("#error").show();
        $("#error").text("Please select week data for creating Tasks");
        return false;
    }

    // Validation to select account from dropdown only
    else if(  "Field work" == $("#workTypeId").val()
           && (  (!accountId
              && !accountName )
              || accountName != accountSetName
              )
           && ($("#customerGroupId").val()).toLowerCase() != 'others'
           && accountName != ''
           && userBU != 'Agro'
           ) {
        $("#error").show();
        $("#error").text("Please select the Account from the dropdown when typing");
        return false;
    }
    else if(  userBU == 'Agro'
           && $("#workTypeId").val() == 'Field work'
           && ($("#numOfAccId").val() == ''
           && $("#accountId").val() == '')) {
        $("#error").show();
        $("#error").text("Please provide values for Account or specify number of Accounts");
        return false;
    }
    else if(( userBU == 'Agro' && $("#workTypeId").val() == 'Field work')
        && $("#accountId").attr("name") == undefined
        && ($("#numOfAccId").val() == '')
        && $("#accountId").val() == '') {
        $("#error").show();
        $("#error").text("Please provide values for number of Accounts");
        return false;
    }
    else if( userBU == 'Agro'
            && $("#provinceId").val() == ''
            && $("#workTypeId").val() == 'Field work'
            && $("#numOfAccId").val() == '') {
        $("#error").show();
        $("#error").text("Please provide Province and City to filter Accounts");
        return false;
    }
    else if( userBU == 'Agro'
            && $("#cityId").val() == ''
            && $("#workTypeId").val() == 'Field work'
            && $("#numOfAccId").val() == '') {
        $("#error").show();
        $("#error").text("Please provide City to filter Accounts");
        return false;
    }
    else if(  $("#accountId").attr("name") == undefined
             && $("#accountId").val() != ''
             && $("#numOfAccId").val() == ''
             && userBU == 'Agro'
             && $("#workTypeId").val() == 'Field work'
            ){
        $("#error").show();

        $("#error").text("Please provide value for 'Number of Accounts' when Account is not in SF");
        return false;
    }
    else if( $("#accountId").attr("name") != undefined
             && $("#accountId").val() != ''
             && $("#numOfAccId").val() != ''
             && userBU == 'Agro'
            ){
        $("#error").show();
        $("#error").text("Please provide value for 'Number of Accounts' only when Account is not in SF");
        return false;
    }
    else {
        $('#accountId').css("border","");
        $('#activityTypeId').css("border","");
        $('#activityId').css("border","");
        $("#error").hide();
        $("#error").text("");
        return true;
    }
}
//---------------------------------------startEndTimeValidation end-------------------------------------
//---------------------------------------checkWeekValues start-------------------------------------
function checkWeekValues(week) {
    if( $("#"+week+"Mo")[0].checked ||
        $("#"+week+"Tu")[0].checked ||
        $("#"+week+"Wd")[0].checked ||
        $("#"+week+"Th")[0].checked ||
        $("#"+week+"Fr")[0].checked ||
        $("#"+week+"Sa")[0].checked ||
        $("#"+week+"Su")[0].checked
        ) {
        return true;
    }
    else {
        false;
    }
}
//---------------------------------------checkWeekValues end-------------------------------------

//---------------------------------------disableWeek start-------------------------------------
function disableWeek() {
    $(".checkmark").css('background-color','grey');
    var monthName = getnumMonth(monthDetails.month);
    var monthNumbers = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    var date = new Date();
    var month = monthNumbers[monthName]; //months from 1-12
    var day = date.getUTCDate();
    var year = monthDetails.year;
    var firstDay;
    firstDay = new Date(parseInt(year), jQuery.inArray(monthName, monthNumbers), 1);
    const dayNames = ["Su","Mo", "Tu", "Wd", "Th", "Fr", "Sa"];
    const daysInMonth = Math.round(((new Date(firstDay.getFullYear(),
                                              firstDay.getMonth()+1)
                                             )-(
                                             new Date(firstDay.getFullYear(),
                                                      firstDay.getMonth()
                                                      )))/86400000);
    var monthStartDay;
    monthStartDay = firstDay.getDay();
    var dateVal = 1;
    for(var itr = monthStartDay; itr < daysInMonth+monthStartDay ; itr++) {

        $('[data-day="'+itr+'"]').removeAttr('disabled');
        $('[data-day="'+itr+'"]').siblings('.checkmark').css('background-color','');
        $('[data-date="'+itr+'"]').text(dateVal);
        dateVal++;
    }
    var week5Disable;
    for(itr = 29; itr <=35; itr++) {
        if($('[data-day="'+itr+'"]').attr('disabled') == 'disabled') {
            week5Disable = true;
        }
        else {
            week5Disable = false;
            break;
        }
    }
    var week6Disable;
    for(itr = 36; itr <=41; itr++) {
        if($('[data-day="'+itr+'"]').attr('disabled') == 'disabled') {
            week6Disable = true;
        }
        else {
            week6Disable = false;
            break;
        }
    }
    if(jQuery.inArray(monthName, monthNumbers) == date.getMonth() && parseInt(year) == date.getFullYear()) {
        for(var itr = monthStartDay; itr < date.getDate()+monthStartDay-1; itr++) {
            $('[data-day="'+itr+'"]').attr('disabled',true);
            $('[data-day="'+itr+'"]').siblings('.checkmark').css('background-color','grey');
        }
    }
    else if(jQuery.inArray(monthName, monthNumbers) < date.getMonth() && parseInt(year) <= date.getFullYear()) {
        for(var itr = 0; itr < 42; itr++) {
            $('[data-day="'+itr+'"]').attr('disabled',true);
            $('[data-day="'+itr+'"]').siblings('.checkmark').css('background-color','grey');
        }
    }

    var holidayDates = [];
    var holidayReasons = [];
    for(var i=0;i<holidayList.length;i++){
        holidayDates.push(holidayList[i].Date_of_Holiday__c.split('-')[2]);
        holidayReasons.push(holidayList[i].Reason_of_Holiday__c);
    }
    console.log('holiday length :: ',holidayDates);
    for(var itr = 0; itr < holidayDates.length ; itr++) {
        // $('[data-day="'+(parseInt(holidayDates[itr])+monthStartDay-1)+'"]').attr('disabled',true);
        $('[data-day="'+(parseInt(holidayDates[itr])+monthStartDay-1)+'"]').siblings('.checkmark').css('background-color','#4284c8');
        $('[data-day="'+(parseInt(holidayDates[itr])+monthStartDay-1)+'"]').siblings('.checkmark').attr('title',holidayReasons[itr]);
    }


    if(week5Disable) {
        $("#w5StartTime").attr("readonly","true");
        $("#w6StartTime").attr("readonly","true");
        $("#w5EndTime").attr("readonly","true");
        $("#w6EndTime").attr("readonly","true");
        $('#w5StartTime').css('disabled', 'true');
        $('#w6StartTime').css('disabled', 'true');
        $('#w5EndTime').css('disabled', 'true');
        $('#w6EndTime').css('disabled', 'true');
    }
    else if(week6Disable) {
        $("#w6StartTime").attr("readonly","true");
        $("#w6EndTime").attr("readonly","true");
        $('#w6StartTime').css('disabled', 'true');
        $('#w6EndTime').css('disabled', 'true');
    }

}
//---------------------------------------disableWeek end---------------------------------------


//---------------------------------------getTimeDiff start-------------------------------------
function getTimeDiff(start,end) {
    s = start.split(':');
    e = end.split(':');
    min = e[1]-s[1];
    hour_carry = 0;
    if(min < 0){
        min += 60;
        hour_carry += 1;
    }
    hour = e[0]-s[0]-hour_carry;
    min = ((min/60)*100).toString()
    return diff = hour + ":" + min.substring(0,2);
}
//-------------------------------------------getTimeDiff end------------------------------------
//-------------------------------------------createAccountDivs start------------------------------------

var rowCounter = 0;
function createAccountDivs(AccName,AccFreq,wrkType,week1,week2,week3,week4,week5,week6){
    var remark;
    if(AccFreq == '' || AccFreq === undefined) {
        remark = 'None';
    }
    else {
        remark = AccFreq;
    }
    if(wrkType == 'Leave' || wrkType == 'Office work') {
        setAcc = 'None';
    }
    else {
        setAcc = AccName;
    }
    var elm =   '<div class="well activityList" id="row'+(rowCounter)+'"> ' +
                '<div class="row"> ' +
                '<div class="col-sm-7 col-12">' +
                '<div class="activitySummary">' +
                '<div class="row"> ' +
                '<div class="col-sm-4 col-12">' +
                '<div class="actHeading">Account</div>' +
                '<div class="activityDetail txtMain" id="accName'+(rowCounter)+'">'+setAcc+'</div>' +
                '</div>' +
                '<div class="col-sm-3 col-12">' +
                '<div class="actHeading">Type of Work</div>' +
                '<div class="activityDetail" id="accWrkType'+(rowCounter)+'">'+wrkType+'</div>' +
                '</div>' +
                '<div class="col-sm-3 col-12">' +
                '<div class="actHeading">Remarks/Notes</div>' +
                '<div class="activityDetail" style="word-wrap: break-word;" id="accFreq'+(rowCounter)+'">'+remark+'</div>' +
                '</div>' +
                '<div class="col-sm-2 col-12">' +
                '<a class="btn btn-viewMore" href="#" role="button" onClick="viewMore('+rowCounter+');">View More</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-5 col-12">' +
                '<div class="bgWeek">' +
                '<div class="row">' +
                '<span class="selectedDays col-sm-2 col-2">' +
                '<div class="weekHeading">w1</div>' +
                '<div class="weekDaysContent" style="word-wrap: break-word;" id="accW1'+(rowCounter)+'">'+week1+'</div>' +
                '</span>' +
                '<span class="selectedDays col-sm-2 col-2">' +
                '<div class="weekHeading">w2</div>' +
                '<div class="weekDaysContent" style="word-wrap: break-word;" id="accW2'+(rowCounter)+'">'+week2+'</div>' +
                '</span>' +
                '<span class="selectedDays col-sm-2 col-2">' +
                '<div class="weekHeading">w3</div>' +
                '<div class="weekDaysContent" style="word-wrap: break-word;" id="accW3'+(rowCounter)+'">'+week3+'</div>' +
                '</span>' +
                '<span class="selectedDays col-sm-2 col-2">' +
                '<div class="weekHeading">w4</div>' +
                '<div class="weekDaysContent" style="word-wrap: break-word;" id="accW4'+(rowCounter)+'">'+week4+'</div>' +
                '</span>' +
                '<span class="selectedDays col-sm-2 col-2">' +
                '<div class="weekHeading">w5</div>' +
                '<div class="weekDaysContent" style="word-wrap: break-word;" id="accW5'+(rowCounter)+'">'+week5+'</div>' +
                '</span>' +
                '<span class="selectedDays col-sm-2 col-2">' +
                '<div class="weekHeading">w6</div>' +
                '<div class="weekDaysContent" style="word-wrap: break-word;" id="accW6'+(rowCounter)+'">'+week6+'</div>' +
                '</span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
    $(elm).appendTo($("#activityList"));
    rowCounter = rowCounter + 1;
}
//-------------------------------------------createAccountDivs end-----------------------------

//-------------------------------------------getSummary start----------------------------------
function getSummary(result) {
    $('#hourSummaryId').empty();
    var elm = '';
    for(let item of JSON.parse(result)["dailyHours"]) {
        elm +=  '<tr>'+
                '<td class="modalDate">'+item["taskDate"]+'</td>'+
                '<td class="modalDay">'+item["taskDay"]+'</td>'+
                '<td class="modalHr">'+item["taskHours"]+'</td>'+
                '</tr>';
    }
    $(elm).appendTo($("#hourSummaryId"));
}
//-------------------------------------------getSummary end------------------------------------
function showSummary() {
    $('#activitySummaryId').addClass('show');
    $('#activitySummaryId').css("display","block");
}

function hideSummary() {
    $('#activitySummaryId').removeClass('show');
    $('#activitySummaryId').css("display","none");
}

function getnumMonth(strMonth) {

    if(!strMonth) {
        return "01";
    }

    var numMonth = "01";

    switch(strMonth) {

        case "Jan":
            numMonth = "January";
            break;

        case "Feb":
            numMonth = "February";
            break;

        case "Mar":
            numMonth = "March";
            break;

        case "Apr":
            numMonth = "April";
            break;

        case "May":
            numMonth = "May";
            break;

        case "Jun":
            numMonth = "June";
            break;

        case "Jul":
            numMonth = "July";
            break;

        case "Aug":
            numMonth = "August";
            break;

        case "Sep":
            numMonth = "September";
            break;

        case "Oct":
            numMonth = "October";
            break;

        case "Nov":
            numMonth = "November";
            break;

        case "Dec":
            numMonth = "December";
            break;

        default:
            numMonth = "January";
    }

    return numMonth;

}
