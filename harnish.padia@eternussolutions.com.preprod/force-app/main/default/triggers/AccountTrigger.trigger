trigger AccountTrigger on Account (after insert, before insert,before update) {
    //MODIFIED BY   : LENNARD PAUL M SANTOS(DELOITTE)
    //REASON        : ADDED TRIGGER EVENT CHECKING AND ADDED HANDLER CLASS THAT WILL HANDLE TRIGGER PROCESS.
    //DATE MODIFIED : JUN.9.2016
    //CHECK TRIGGER EVENTS
    if(Trigger.isBefore){
        //CALL CORRESPONDING CLASSES THAT WILL HANDLE TRIGGER EVENT
        if(Trigger.isInsert){
            AccountTrigger_Handler.onBeforeInsert(Trigger.new);
        }
        if(Trigger.isUpdate){
            AccountTrigger_Handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
        }
    }
    
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            AccountTrigger_Handler.onAfterInsert(Trigger.new);
        }
    }

    

}