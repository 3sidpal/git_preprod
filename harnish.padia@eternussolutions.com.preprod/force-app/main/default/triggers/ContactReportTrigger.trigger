trigger ContactReportTrigger on Contact_Report__c (after insert, after update, before update) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            new GenericTriggerHandler().populateActivityStatus(trigger.oldMap, Trigger.new);
        }
    }

    if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            new GenericTriggerHandler().populateSyncDateSubmittedDate(Trigger.oldMap, Trigger.newMap);
        }
    }
}