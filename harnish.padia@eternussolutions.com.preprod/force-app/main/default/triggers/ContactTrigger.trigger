/*-------------------------------------------------------------------------------------------
Author       :   eugenebasianomutya
Created Date :   May 1 2017
Definition   :   
History      :   
-------------------------------------------------------------------------------------------*/
trigger ContactTrigger on Contact (before insert,
                                                   before update,
                                                   before delete,
                                                   after insert,
                                                   after update,
                                                   after delete,
                                                   after undelete) {
    TriggerDispatcher.Run(new ContactTriggerHandler());
}