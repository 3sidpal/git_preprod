trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {
 
    ContentDocumentLinkTriggerHandler.processUploadedFiles();
}