trigger OrderItemTrigger on Order_Item__c (before update, before insert, before delete, after insert, after update, after delete){

    if(TriggerFlagControl__c.getInstance().Order_ByPassTrigger__c == false){
        if(Trigger.isBefore){
            if(Trigger.isInsert){
                OrderItemTrigger_Handler.onBeforeInsert(Trigger.new);
            }
            if(Trigger.isUpdate){
                OrderItemTrigger_Handler.onBeforeUpdate(Trigger.new,Trigger.oldMap);
            }
            if(Trigger.isUpdate || Trigger.isInsert){
                OrderItemTrigger_Handler.computeTotalProductDiscount(Trigger.new);
                
                
            }
            if(Trigger.isDelete){
                OrderItemTrigger_Handler.onBeforeDelete(Trigger.old);
            }
        }
        else if(Trigger.isAfter) {
            if(Trigger.isInsert || Trigger.isUpdate) {
                
                OrderItemTrigger_Handler.calculateParentValues(Trigger.newMap);
                OrderItemTrigger_Handler.hasTradeAsset(Trigger.New);
                OrderItemTrigger_Handler.rollupMultipleInvoice(Trigger.New,Trigger.oldMap);
                OrderItemTrigger_Handler.updateTimelinessMultipleInvoice(Trigger.New,Trigger.oldMap);
                
            }
            
            if(Trigger.isInsert) {
                try{
                    /*if(!StockAllocationHandler.isRunning){
                        StockAllocationHandler.processOrderItemsFuture(Trigger.newMap.keySet());    
                    }*/ //Commented Out for DI deployment - Kiko Roberto 02212019
                        
                    //StockAllocationHandler objStockAllocationHandler = new StockAllocationHandler();
                    //objStockAllocationHandler.processOrderItems(Trigger.new, true);
                }catch(Exception e){
                    System.debug('Exception in Stock Allocation Order Item : '+e);
                }
                //Method to get zkb order type for modcon - Kiko Roberto - 05282019
                OrderItemTrigger_Handler.getSalesOrgModCon(Trigger.New);
            }

            if(Trigger.isUpdate) {
                new UpdateOrderStatusHandler().updateOrderStatus(trigger.new,trigger.oldmap);
                OrderItemTrigger_Handler.onAfterUpdate(trigger.new);
            }
            
            if(Trigger.isDelete){
                OrderItemTrigger_Handler.hasTradeAssetDelete(Trigger.old);
            }
        }
    }
}