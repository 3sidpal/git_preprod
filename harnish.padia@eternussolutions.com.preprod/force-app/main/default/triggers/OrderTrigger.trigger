trigger OrderTrigger on Order__c (before insert, before update, before delete, after insert, after update) {
        
    if(TriggerFlagControl__c.getInstance().Order_ByPassTrigger__c == false) {
    
        if(Trigger.isBefore) {
            if(Trigger.isInsert) {
                OrderTrigger_Handler.onBeforeInsert(Trigger.new);
                OrderTrigger_Handler.populateParentSIFMultipleInvoice(Trigger.New, Trigger.oldMap);
                OrderTrigger_Returns_Handler.updateSIFViaInvoice(Trigger.new);
            }
            if(Trigger.isUpdate) {
                OrderTrigger_Handler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
                OrderTrigger_Handler.populateParentSIFMultipleInvoice(Trigger.New, Trigger.oldMap);
            }
            if(Trigger.isDelete) {
                OrderTrigger_Handler.onBeforeDelete(Trigger.old);
            }
        }
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                OrderTrigger_Handler.onAfterInsert(Trigger.new);
                OrderTrigger_Handler.calculateTimelinessParentMultipleInvoice(Trigger.New);
            }
            if(Trigger.isUpdate){
                try{
                    //StockAllocationHandler.validateServingPlantChangeFuture(Trigger.newMap.keySet(), Trigger.oldMap.keySet());
                    /*StockAllocationHandler objStockAllocationHandler = new StockAllocationHandler();
                    objStockAllocationHandler.validateServingPlantChange(Trigger.new, Trigger.oldMap);*/ //Commented out for DI Deployment - Kiko Roberto 02212019
                    
                    OrderTrigger_Handler.calculateTimelinessParentMultipleInvoice(Trigger.New);
                }catch(Exception e){
                    System.debug('Exception in Stock Allocation : '+e);
                }
            }
        }
    }
}