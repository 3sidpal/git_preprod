trigger PromoBundleTriggger on Order_Item__c (After Insert) {
     Id actualSalesOrederItemRecTypeId = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
    List<Order_Item__c > newOrderItems = new List<Order_Item__c >();
    Set<id> orderItemIds = new Set<Id>();
    //Set<id> productIdsWithPromo = new Set<id>();
    Map<id,List<promotion__c>> currentPromotionsMap = new Map<id,List<promotion__c>>();
    List<promotion__c> tempPromotionList = new List<promotion__c>();
    promotion__c currentPromotion = new promotion__c();
    
    
    List<order_item__c> newOrderItemsToInsert = new List<order_item__c> ();
    
    order_item__c  tempOrderItem = new order_item__c ();
    
    Decimal baseRateQty = 0;
    Decimal convertedQty = 0;
    
    List<Order_Item__c> orderItemDetails = new List<Order_Item__c>();
    
    List<Promotion__c> CurrentValidPromotions = new List<Promotion__c>();
    
    CurrentValidPromotions  = [SELECT id, Custom_Product__c, custom_product__r.name,Trigger_Quantity__c,Conversion__r.Conversion_Rate__c,
                    (SELECT id, Bundled_Price__c, Custom_Product__c, Quantity__c, Conversion__c, 
                            Promotion__r.Conversion__r.Conversion_Rate__c, Promotion__r.Trigger_Quantity__c 
                    FROM Bundles__r)   
            FROM Promotion__c 
            WHERE Valid_Until__c >= TODAY];
            
            system.debug('%% CurrentValidPromotions:'+CurrentValidPromotions);
    if(CurrentValidPromotions != null && !CurrentValidPromotions.isEmpty()){
        //get active promotion for today.   
        for(Promotion__c p: CurrentValidPromotions){
            tempPromotionList = new List<promotion__c>();
            if(currentPromotionsMap.get(p.custom_product__c)==null){
                tempPromotionList.add(p);
            }else{
                tempPromotionList = currentPromotionsMap.get(p.custom_product__c);
                tempPromotionList.add(p);
            }
            system.debug('%% ['+p.custom_product__r.name+']tempPromotionList:'+tempPromotionList);
            currentPromotionsMap.put(p.custom_product__c, tempPromotionList);
        }
        
        //get all newly created/updated records with related product having a promotion.
        for(Order_Item__c oi : trigger.new){
        system.debug('%% ['+oi.Price__c+'] oi.Price__c:'+(oi.Price__c > 0)+'  next: '+(currentPromotionsMap.get(oi.Product__c)!=null));
            if(oi.Price__c > 0 && currentPromotionsMap.get(oi.Product__c)!=null){
                orderItemIds.add(oi.Id);
            }
        }
        
        system.debug('&& orderItemIds:'+orderItemIds);
        
        orderItemDetails = [SELECT id, Invoice_Quantity__c,product__c, Conversion__r.Conversion_Rate__c,Order_Form__c FROM Order_item__c WHERE Id IN :orderItemIds];
        
        system.debug(' $$ orderItemDetails:'+orderItemDetails);
        //get the promotion with higher trigger quantity.
        for(Order_item__c oid: orderItemDetails ){
            system.debug('%% vp:'+oid);
            currentPromotion = new promotion__c();
            for(promotion__c vp: currentPromotionsMap.get(oid.product__c)){
                baseRateQty = vp.Trigger_Quantity__c/vp.Conversion__r.Conversion_Rate__c;
                convertedQty = baseRateQty*vp.Conversion__r.Conversion_Rate__c;
                
                system.debug('%% baseRateQty:'+baseRateQty+'  base oi:'+(oid.Invoice_Quantity__c/oid.Conversion__r.Conversion_Rate__c));
                
                if(baseRateQty<=(oid.Invoice_Quantity__c/oid.Conversion__r.Conversion_Rate__c) ){
                    if(currentPromotion.id!=null){
                        system.debug(' $$ not null');
                        //get the larger promotion with higher trigger quantity else retain the old promotion record.
                        if(baseRateQty > vp.Trigger_Quantity__c/vp.Conversion__r.Conversion_Rate__c ){
                            system.debug(' $$ not null2');
                            currentPromotion = vp;
                        }
                    }else{
                        system.debug(' $$ null');
                        currentPromotion = vp;
                    }
                    
                }
            }
            system.debug('^^ currentPromotion:'+currentPromotion);
            
            
            for(bundle__c b: currentPromotion.bundles__r){
                //changed formula to accomodate diffent UOM conversions - Kiko Roberto 03/20/2019 - Start -
                //(old formula)String sQty=String.valueOf(((oid.Invoice_Quantity__c/oid.Conversion__r.Conversion_Rate__c) / (b.Promotion__r.Trigger_Quantity__c/b.Promotion__r.Conversion__r.Conversion_Rate__c)).setscale(0,RoundingMode.DOWN));
                String sQty=String.valueOf(((oid.Invoice_Quantity__c * oid.Conversion__r.Conversion_Rate__c) / (b.Promotion__r.Trigger_Quantity__c * b.Promotion__r.Conversion__r.Conversion_Rate__c)).setscale(0,RoundingMode.DOWN));
                //changed formula to accomodate diffent UOM conversions - Kiko Roberto 03/20/2019 - End -
                tempOrderItem = new order_item__c ();
                tempOrderItem.Product__c = b.Custom_Product__c;
                tempOrderItem.Invoice_Net_Total__c = 0;
                tempOrderItem.Invoice_Quantity__c =Decimal.valueOf(sQty)*b.Quantity__c;
                tempOrderItem.Order_Form__c = oid.Order_Form__c;
                //Changed from hard coded 0 to Bundled_Price__c - Kiko Roberto - 03/22/2019 - Start -
                //tempOrderItem.Price__c = 0;
                tempOrderItem.Price__c = b.Bundled_Price__c;
                //Changed from hard coded 0 to Bundled_Price__c - Kiko Roberto - 03/22/2019 - End -
                tempOrderItem.Conversion__c=b.Conversion__c;
                tempOrderItem.recordTypeId = actualSalesOrederItemRecTypeId;
                //Added check for quantity before creating line item - Kiko Roberto - 03/24/2019 - Start -
                if(tempOrderItem.Invoice_Quantity__c != null && tempOrderItem.Invoice_Quantity__c > 0){
                    newOrderItemsToInsert.add(tempOrderItem);
                }
                //Added check for quantity before creating line item - Kiko Roberto - 03/24/2019 - End -
                
            }
        }
        
        system.debug('^^ newOrderItemsToInsert:'+newOrderItemsToInsert);
        
        if(newOrderItemsToInsert.size()>0){
            insert newOrderItemsToInsert;
            system.debug('** inserted newOrderItemsToInsert:'+newOrderItemsToInsert);
        }     
    }
}