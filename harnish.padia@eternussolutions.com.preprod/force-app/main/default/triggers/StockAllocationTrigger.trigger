trigger StockAllocationTrigger on Stock_Allocation__c (before update, before insert) {
    system.debug('====StockAllocationTrigger==');
    StockAllocationTriggerHandler.populateProductLookups();
    StockAllocationTriggerHandler.populateStockUOMLookups();
    //StockAllocationTriggerHandler.populatePrimaryStockLookups();
}