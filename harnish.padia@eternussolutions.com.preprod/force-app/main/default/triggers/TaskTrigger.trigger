/*
-----------------------------------------------------------------------------------------------------------
Modified By                 Date          Version         Description
-----------------------------------------------------------------------------------------------------------
Eternus Solutions           22/06/2018    Initial         Trigger for task
-----------------------------------------------------------------------------------------------------------

*/

trigger TaskTrigger on Task ( before insert
                            , before update
                            , after insert
                            , after update
                            , before delete
                            , after delete
                            , after undelete
                            ) {

    if(StaticResources.byPassTaskTrigger) {
        return;
    }

    if(trigger.isBefore) {

        if(trigger.isInsert || trigger.isUpdate) {

            new TaskTriggerHandler().computeMonthData(trigger.oldMap, trigger.new);
            new TaskTriggerHandler().populateFormsToBeFilled(trigger.new);

        }

        if(trigger.isInsert) {
            new TaskTriggerHandler().tagTasksCreatedAfterApproval(Trigger.new);
        }
    }

    if(trigger.isAfter) {


        if(trigger.isInsert || trigger.isUpdate) {
            new TaskTriggerHandler().computeWorkingHours(trigger.new);
            if (!StaticResources.byPassCreateEForm) {
                new TaskTriggerHandler().createEForm(trigger.oldMap, trigger.newMap);
            }
            new TaskTriggerHandler().updateAccountConfigStatus(trigger.new);
            //new TaskTriggerHandler().rollupActualAccountCount(trigger.new, trigger.isInsert);
            new TaskTriggerHandler().returnMapUserToTypeOfActivityToUniqueAccount(trigger.new);
        }

        if (trigger.isInsert) {
            if (!StaticResources.byPassCreateEForm) {
                 new TaskTriggerHandler().cancelOverlappingTasks(trigger.new);
            }
            //new TaskTriggerHandler().rollupPlannedAccountCount(trigger.new);
        }

        if(trigger.isUpdate) {
            new TaskTriggerHandler().persistOldvaluesOnRecordUpdate(Trigger.oldMap);
        }

        if(trigger.isDelete) {
            new TaskTriggerHandler().computeWorkingHours(trigger.old);
            new TaskTriggerHandler().deleteOrphanFiles(trigger.oldMap);
            new TaskTriggerHandler().activityToUniqueAccountOnDelete(trigger.old);
            //new TaskTriggerHandler().rollupPlannedAndActualCountAfterDelete(trigger.old);

        }
        if(trigger.isUndelete) {
            new TaskTriggerHandler().computeWorkingHours(trigger.new);
        }

    }

}