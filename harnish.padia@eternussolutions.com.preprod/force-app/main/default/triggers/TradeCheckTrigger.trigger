Trigger TradeCheckTrigger on Trade_Check__c (after insert, after update, before update) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            new GenericTriggerHandler().populateActivityStatus(Trigger.oldMap, Trigger.new);
        }
    }

    if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            new GenericTriggerHandler().populateSyncDateSubmittedDate(Trigger.oldMap, Trigger.newMap);
        }
    }
}