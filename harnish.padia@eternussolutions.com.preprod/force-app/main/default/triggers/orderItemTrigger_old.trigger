trigger orderItemTrigger_old on Order_Item__c (after update, after insert) {
    public TriggerFlagControl__c snapshotSettings = TriggerFlagControl__c.getInstance();
    public Integer snapshotDayStart{get;set;}
    public Integer snapshotDayEnd{get;set;}
    Map<string, string> mapOrderIdLineKey = new Map<string, string>();
    Set<id> tempIdSet = new Set<id>();
    Set<id> sifWithReturnAmountId = new Set<id>();
    Set<id> sifWithReturnAmounts = new Set<Id>();
    List<Order__c> sifWithReturnAmountRec = new List<Order__c>();
    List<Order_Item__c> newOrderItemDetails =  new List<Order_Item__c>();
    List<Order_Item__c> orderItemsToDeduct =  new List<Order_Item__c>();
    List<Order_Item__c> orderItemsToAdd =  new List<Order_Item__c>();
    List<String> lineKeysToAdd = new List<String>();
    List<String> lineKeysToDeduct = new List<String>();
    List<String> itemKeysToDeduct = new List<String>();
    List<String> inventoryKeysToDeduct = new List<String>();
    List<String> itemKeysToAdd = new List<String>();
    List<String> inventoryKeysToAdd = new List<String>();
    Map<Id,Decimal> returnedItemAmountsMap = new Map<Id,Decimal>();
    Decimal tempReturnedAmount;
    Decimal totalRawWeighQtyToAdd = 0;
    String tempStrings = '';
    
    Set<Id> sifDistributors = new Set<Id>();
    
    TriggerFlagControl__c settings = TriggerFlagControl__c.getInstance();
        
    Id returnRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId();
    Id actualSalesRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
    system.debug('####trigger.isBefore:'+trigger.isBefore+' trigger.isAfter:'+trigger.isAfter);
    
    /* GAAC: TRIGGER ONLY EXECUTE AFTER EVENTS
    if(trigger.isBefore ){
        system.debug('^^^^^ before (validation) update 1');
        if(trigger.isUpdate){
            validateWarehouseAndDistributorId(trigger.new);
        }
        if(trigger.isInsert){
            validateWarehouseAndDistributorId(trigger.new);
        }
        
    }*/    
    
    if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)){
    
        if(!TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE){
    
            SIFOrderItemBatchSwitch__c snapshotSwitch = SIFOrderItemBatchSwitch__c.getInstance();
            System.debug('snapshotSwitch.RunByBatch__c----'+snapshotSwitch.RunByBatch__c);
            if (snapshotSwitch.RunByBatch__c == false) {
                System.debug('snapshotSwitch.RunByBatch__c-22---'+snapshotSwitch.RunByBatch__c);
                for(Order_Item__c oi: trigger.new){
                    tempIdSet.add(oi.id);
                }
           
                if(settings != null){
                    if(tempIdSet.size()>0 && settings.orderItemOldTrigger__c == true){//CHECK CUSTOM SETTING VALUE IF ENABLED
                        system.debug('^^^^^ after (validation) update 0');
                        newOrderItemDetails = [SELECT Order_Form__r.Monthly_order__c, Product__c, Product__r.Catch_weight__c, Conversion__c,Condition__c, Conversion__r.Conversion_Rate__c, Invoice_Quantity__c, Returned_Quantity__c,  
                                    Order_Form__r.Distributor_SFA_ID__c, Order_Form__r.RecordTypeID, RecordTypeID, Order_Form__r.Account__r.Warehouse__c, 
                                    Order_Form__r.Account__r.Distributor__r.Warehouse__c, Order_Form__r.Account__r.Distributor__c,Order_Form__r.Account__c, Invoice_Date__c,
                                    Weight_Quantity__c, Weight_UOM__c, Weight_UOM__r.Conversion_Rate__c, Invoice_Net_Total__c, Order_Form__r.Sales_Information_Form__c
                                FROM Order_Item__c 
                                WHERE Id IN: tempIdSet AND (Order_Form__r.RecordTypeID = :actualSalesRecordTypeId OR Order_Form__r.RecordTypeID = :returnRecordTypeId )];
                                
                                
                        for(Order_Item__c  oiNew : newOrderItemDetails ) {
                            
                            sifDistributors.add(oiNew.Order_Form__r.Account__r.Distributor__c);
                            orderItemsToAdd.add(oiNew);
                            
                            tempStrings = String.valueOf(oiNew.Order_Form__r.Distributor_SFA_ID__c).substring(0, 15)+'-'+String.valueOf(oiNew.Order_Form__r.Account__r.Warehouse__c).substring(0, 15);
                            inventoryKeysToAdd.add(tempStrings);
                            tempStrings = tempStrings+'-'+ String.valueOf(oiNew.Product__c).substring(0, 15);
                            itemKeysToAdd.add(tempStrings);
                            
                            if(oiNew.Order_Form__r.RecordTypeID==returnRecordTypeId && oiNew.Condition__c != 'Defect') {
                                tempStrings = tempStrings+'-'+'--RETURNED--';
                                //add returned items amount to related Sales Information form.

                                if(oiNew.Order_Form__r.Sales_Information_Form__c!=null) {
                                    tempReturnedAmount = returnedItemAmountsMap.get(oiNew.Order_Form__r.Sales_Information_Form__c);
                                                                        
                                    if(oiNew.Invoice_Net_Total__c == NULL) {
                                        oiNew.Invoice_Net_Total__c = 0;    
                                    }
                                    
                                    If(tempReturnedAmount==null){
                                        returnedItemAmountsMap.put(oiNew.Order_Form__r.Sales_Information_Form__c, (0-oiNew.Invoice_Net_Total__c));
                                    }else{
                                        tempReturnedAmount-=oiNew.Invoice_Net_Total__c;
                                        returnedItemAmountsMap.put(oiNew.Order_Form__r.Sales_Information_Form__c, tempReturnedAmount);
                                    }
                                }
                            }
                            lineKeysToAdd .add(tempStrings);
                            
                        }
                        //update return Amounts.
                        sifWithReturnAmounts = returnedItemAmountsMap.keySet();
                        sifWithReturnAmountRec = [SELECT id, Returned_Item_Amount__c FROM Order__c WHERE Id IN :sifWithReturnAmounts ];
                        for(Order__c ora: sifWithReturnAmountRec){
                            tempReturnedAmount = returnedItemAmountsMap.get(ora.Id);
                            if(tempReturnedAmount!=null){
                                if(ora.Returned_Item_Amount__c==null){
                                    ora.Returned_Item_Amount__c=tempReturnedAmount;
                                }else{
                                    ora.Returned_Item_Amount__c+=tempReturnedAmount;                    
                                }
                            }
                            
                        }
                        update sifWithReturnAmountRec;
                        AddToInventory(orderItemsToAdd,inventoryKeysToAdd, itemKeysToAdd, lineKeysToAdd, sifDistributors);
                        Integer startDay = snapshotSettings.Snapshot_Start_Day__c != null ? Integer.valueOf(snapshotSettings.Snapshot_Start_Day__c) : null;
                        Integer endDay = snapshotSettings.Snapshot_End_Day__c != null ? Integer.valueOf(snapshotSettings.Snapshot_End_Day__c) : null;
                        if(Test.isRunningTest()){
                            startDay = System.today().day();
                            endDay = System.today().day();
                        }
                        
                        if((startDay!= null && endDay != null) && (startDay!=0 && endDay != 0)){
                            Date date1 = Date.newInstance(System.today().year(), System.today().month(), startDay);//HARDCODED DATE??????
                            Date date2 = Date.newInstance(System.today().year(), System.today().month(), endDay);
                            System.debug('\n\n\nSTART DATE : ' + date1 + '\n\n\n');
                            System.debug('\n\n\nEND DATE : ' + date2 + '\n\n\n');
                            
                            if(date1<=System.today() && date2>=System.today()){
                                System.debug('\n\n\n**********UPDATE MONTHLY SNAPSHOT***********');
                                UpdateMonthlySnapshot(orderItemsToAdd);        
                            }
                        }
                        
                        
                        // if (System.Today() == date1 || System.Today() == date2) {
                        //     UpdateMonthlySnapshot(orderItemsToAdd);
                        // }
                    }
                
                }
            } 
        }
    }
    
    public void UpdateMonthlySnapshot(List<Order_Item__c> newOrderItems){
        List<Order_Item__c> previousSIFList = new List<Order_Item__c>();
        for (Order_Item__c oi : newOrderItems){
            System.debug('\n\n\nORDER ITEM : ' + oi + '\n\n\n');
            System.debug('\n\n\nORDER ITEM INVOICE DATE : ' + oi.Invoice_Date__c + '\n\n\n');
            //If invoice of SIF is dated on the previous month, use the SIF qty to deduct the ending balance in the snapshot
            if (oi.Invoice_Date__c < System.Today().toStartOfMonth()){
                System.debug('\n\n\n***************GET PREVIOUS SIF LIST******************\n\n\n');
                previousSIFList.add(oi);
            }
            
        }//end for
        List<Monthly_Inventory__c> snapshotList = new List<Monthly_Inventory__c>();     
        //Get the last snapshots
        snapshotList = [Select Linekey__c, Ending_Balance__c From Monthly_Inventory__c]; // where CreatedDate = LAST_MONTH
        Map<string, Monthly_Inventory__c> mapSnapshot = new Map<string, Monthly_Inventory__c>();
        for (Monthly_Inventory__c inv : snapshotList){
            System.debug('INVENTORY SNAPSHOT : ' + inv + '\n\n\n');
            System.debug('INVENTORY SNAPSHOT LINE KEY: ' + inv.Linekey__c + '\n\n\n');
            mapSnapshot.put(inv.Linekey__c, inv);
        }       
        List<Monthly_Inventory__c> updatedSnapshotList = new List<Monthly_Inventory__c>();
        for (Order_Item__c o : previousSIFList){
            if (mapSnapshot.get(mapOrderIdLineKey.get(o.Id)) != null){
                Monthly_Inventory__c miv = mapSnapshot.get(mapOrderIdLineKey.get(o.Id));
                miv.Ending_Balance__c -= o.Invoice_Quantity__c; 
                updatedSnapshotList.add(miv);
            }
        }
        System.debug('\n\n\nUPDATE SNAPSHOT LIST : ' + updatedSnapshotList + '\n\n\n');
        if(updatedSnapshotList.size() > 0){
            update updatedSnapshotList;
        }
    }
    
    public void AddToInventory(List<Order_Item__c> newOrderItems, List<String> inventoryKeys, List<String> itemKeys, List<String> lineKeys, Set<Id> sifDistributorIds) {
            //get all inventory lines
            Set<ID> newOrderItemsUomIdSet = new Set<ID>();
            Boolean withErrors = false;
            Decimal totalRawQtyToAdd = 0.00;
            Decimal totalRawReturnQty = 0.00;
            Map<String, Inventory_Line__c> existingInventoryLinesMap_Return = new Map<String, Inventory_Line__c>();
            Map<String, List<Inventory_Line__c>> existingInventoryLinesMap_Old = new Map<String, List<Inventory_Line__c>>();
            List<Inventory_Line__c> existingInventoryLinesList_Oldest = new List<Inventory_Line__c>();
            Map<id,Decimal> newOrderItemsConvertionRateMap = new Map<id,Decimal>();
            Map<String, Inventory_Item__c> existingInventoryItemsMap = new Map<String, Inventory_Item__c>();
            Map<String, Inventory__c> existingInventoriesMap = new Map<String, Inventory__c>();
            List<Inventory_Line__c> inventoryLineToUpdate = new List<Inventory_Line__c>();
            Map<Id, Inventory_Line__c> inventoryLineToUpdateMap =  new Map<Id, Inventory_Line__c> ();
            List<Inventory_Line__c> inventoryLineToinsert = new List<Inventory_Line__c>();
            Inventory_item__c tempinventoryItemToInsert = new Inventory_item__c();
            List<Inventory_item__c> inventoryItemToInsertList = new List<Inventory_item__c>();
            Inventory__c tempinventoryToInsert = new Inventory__c();
            List<Inventory__c> inventoryToInsertList = new List<Inventory__c>();
            Map<String,List<Inventory_Line__c>> inventoryLineToinsertMap = new  Map<String,List<Inventory_Line__c>>();
            List<Inventory_Line__c> existingInventoryLinesList_Return = new List<Inventory_Line__c>();
            List<Inventory_Item__c> existingInventoryItemList = new List<Inventory_Item__c>();
            List<Inventory__c> existingInventoryList = new List<Inventory__c>();
            List<Inventory_Line__c> tempInventoryLineList = new List<Inventory_Line__c> ();
            List<Conversion__c> newOrderItemConversionList = new List<Conversion__c>();
            Inventory_Line__c newInventoryLines_temp = new Inventory_Line__c();
            Inventory_Item__c newInventoryItem_temp = new Inventory_Item__c();
            String lineKeyStr = '';
            String itemKeyStr = '';
            String inventoryKeyStr = '';
            withErrors = false;

            Savepoint sp = Database.setSavepoint();
            if(!withErrors){
                existingInventoryLinesList_Oldest  = [
                        SELECT Id, LineKey__c,Inventory_Item__r.ItemKey__c, Age__c, SIF_Quantity__c, UOM__r.Conversion_Rate__c , 
                            Ending_Balance__c, Depleted__c , UOM__r.UOM__r.Catch_Weight_UOM__c, Inventory_Item__r.Product__r.Catch_weight__c,
                            Weight_UOM__c, Weight_Quantity__c,  Weight_UOM__r.Conversion_Rate__c, Batch_Code__c
                        FROM Inventory_Line__c 
                        WHERE  (NOT  LineKey__c LIKE '%--RETURNED--%') 
                            AND Inventory_Item__r.ItemKey__c IN: itemKeys
                            AND Depleted__c = false
                            AND Inventory_Item__r.Inventory__r.Account__c IN :sifDistributorIds
                        ORDER BY Date_Manufactured__c ASC NULLS LAST];
                system.debug('%%t existingInventoryLinesList_Oldest:' +existingInventoryLinesList_Oldest);
                for(Inventory_Line__c  il : existingInventoryLinesList_Oldest ){
                    if(existingInventoryLinesMap_Old.get(il.Inventory_Item__r.ItemKey__c)==null){
                        //do nothing.
                    }else if(existingInventoryLinesMap_Old.get(il.Inventory_Item__r.ItemKey__c).size()>0){
                        tempInventoryLineList = new List<Inventory_Line__c> ();
                        tempInventoryLineList = existingInventoryLinesMap_Old.get(il.Inventory_Item__r.ItemKey__c);
                    }
                    tempInventoryLineList.add(il);
                    existingInventoryLinesMap_Old.put(il.Inventory_Item__r.ItemKey__c, tempInventoryLineList);
                }
                existingInventoryLinesList_Return = [SELECT Id, LineKey__c,Inventory_Item__r.ItemKey__c,SIF_Quantity__c, UOM__r.Conversion_Rate__c FROM Inventory_Line__c WHERE LineKey__c IN: lineKeys AND Inventory_Item__r.Inventory__r.Account__c IN :sifDistributorIds];
                existingInventoryItemList  = [SELECT Id, ItemKey__c FROM Inventory_Item__c WHERE ItemKey__c IN: itemKeys ];
                existingInventoryList = [SELECT ID,InventoryKey__c   FROM Inventory__c WHERE InventoryKey__c   IN: inventoryKeys ];
                for(Inventory_Line__c  il : existingInventoryLinesList_Return ){
                    existingInventoryLinesMap_Return.put(il.LineKey__c , il);
                }
                for(Inventory_Item__c ii: existingInventoryItemList){
                    existingInventoryItemsMap.put(ii.ItemKey__c,ii);
                }
                for(Inventory__c i: existingInventoryList){
                    existingInventoriesMap.put(i.InventoryKey__c,i);
                }
                
                //Check for exisitng Header, if it does not exist, create a new one.
                for(Order_Item__c oiNew: newOrderItems){
                    if(existingInventoriesMap.get(String.valueOf(oiNew.Order_Form__r.Distributor_SFA_ID__c).substring(0, 15)+'-'+String.valueOf(oiNew.Order_Form__r.Account__r.Warehouse__c).substring(0, 15))==null ){
                        tempinventoryToInsert = new Inventory__c();
                        tempinventoryToInsert.Account__c = oiNew.Order_Form__r.Distributor_SFA_ID__c;
                        tempinventoryToInsert.Last_Update__c = Date.today();
                        tempinventoryToInsert.Warehouse1__c = oiNew.Order_Form__r.Account__r.Warehouse__c;
                        inventoryToInsertList.add(tempinventoryToInsert);
                    }
                    if(oiNew.Conversion__c!=null){
                        newOrderItemsUomIdSet.add(oiNew.Conversion__c);
                    }
                }
                if(newOrderItemsUomIdSet.size()>0){
                    newOrderItemConversionList = [SELECT id, Conversion_Rate__c FROM Conversion__c WHERE Id IN:newOrderItemsUomIdSet];
                }
                for(Conversion__c cl : newOrderItemConversionList ){
                    newOrderItemsConvertionRateMap.put(cl.id,cl.Conversion_Rate__c);
                }
                //get new order items UOM conversion rates.
                
                
                if(inventoryToInsertList.size()>0){
                    insert inventoryToInsertList;
                    tempIdSet = new Set<id>();
                    for(Inventory__c iins: inventoryToInsertList){
                        tempIdSet.add(iins.id);
                    }
                    existingInventoryList = [SELECT ID,InventoryKey__c   FROM Inventory__c WHERE InventoryKey__c   IN: inventoryKeys OR id IN: tempIdSet];
                    for(Inventory__c i: existingInventoryList){
                        existingInventoriesMap.put(i.InventoryKey__c,i);
                    }
                }
                // Check for existing inventory Item. insert if none exist
                for(Order_Item__c oiNew: newOrderItems){
                    inventoryKeyStr = String.valueOf(oiNew.Order_Form__r.Distributor_SFA_ID__c).substring(0, 15)+'-'+String.valueOf(oiNew.Order_Form__r.Account__r.Warehouse__c).substring(0, 15);
                    if(existingInventoryItemsMap.get(inventoryKeyStr+'-'+ String.valueOf(oiNew.Product__c).substring(0, 15))==null ){
                        tempinventoryItemToInsert= new Inventory_Item__c ();
                        tempinventoryItemToInsert.Inventory__c = existingInventoriesMap.get(inventoryKeyStr).id;
                        tempinventoryItemToInsert.Product__c = oiNew.Product__c;
                        inventoryItemToInsertList.add(tempinventoryItemToInsert);
                    }
                }
                if(inventoryItemToInsertList.size()>0){
                    insert inventoryItemToInsertList;
                    tempIdSet = new Set<id>();
                    for(Inventory_Item__c iiins: inventoryItemToInsertList){
                        tempIdSet.add(iiins.id);
                    }
                    existingInventoryItemList  = [SELECT Id, ItemKey__c FROM Inventory_Item__c WHERE ItemKey__c IN: itemKeys OR id IN:tempIdSet];
                    for(Inventory_Item__c ii: existingInventoryItemList){
                        existingInventoryItemsMap.put(ii.ItemKey__c,ii);
                    }
                    existingInventoryLinesList_Oldest  = [
                    SELECT Id, LineKey__c,Inventory_Item__r.ItemKey__c, Age__c, SIF_Quantity__c, UOM__r.Conversion_Rate__c , UOM__r.UOM__r.Catch_Weight_UOM__c,
                            Ending_Balance__c, Depleted__c 
                    FROM Inventory_Line__c 
                    WHERE  (NOT  LineKey__c LIKE '%--RETURNED--%') 
                        AND Inventory_Item__r.ItemKey__c IN: itemKeys
                        AND Depleted__c =false
                        AND Inventory_Item__r.Inventory__r.Account__c IN :sifDistributorIds
                    Order By Date_Manufactured__c ASC ];
                    for(Inventory_Line__c  il : existingInventoryLinesList_Oldest ){
                        if(existingInventoryLinesMap_Old.get(il.Inventory_Item__r.ItemKey__c)==null){
                            //do nothing.
                        }else if(existingInventoryLinesMap_Old.get(il.Inventory_Item__r.ItemKey__c).size()>0){
                            tempInventoryLineList = new List<Inventory_Line__c> ();
                            tempInventoryLineList = existingInventoryLinesMap_Old.get(il.Inventory_Item__r.ItemKey__c);
                        }
                        tempInventoryLineList.add(il);
                        existingInventoryLinesMap_Old.put(il.Inventory_Item__r.ItemKey__c, tempInventoryLineList);
                    }

                }

                }

                //check for inventory line to update, if none insert esle update.
                for(Order_Item__c oiNew: newOrderItems) {
                                
                    itemKeyStr = String.valueOf(oiNew.Order_Form__r.Distributor_SFA_ID__c).substring(0, 15)+'-'+String.valueOf(oiNew.Order_Form__r.Account__r.Warehouse__c).substring(0, 15)+'-'+  String.valueOf(oiNew.Product__c).substring(0, 15);
                    system.debug('%%t 2itemKeyStr:'+itemKeyStr);
                    if(oiNew.Order_Form__r.RecordTypeID==returnRecordTypeId && oiNew.Condition__c != 'Defect'){
                        lineKeyStr =itemKeyStr+'-'+'--RETURNED--';
                    }
                    if(oiNew.Order_Form__r.RecordTypeID==returnRecordTypeId && oiNew.Condition__c != 'Defect'){
                        if(existingInventoryLinesMap_Return.get(lineKeyStr)==null ){
                            newInventoryLines_temp = new Inventory_Line__c();
                            newInventoryLines_temp.Batch_Code__c = '--RETURNED--';
                            newInventoryLines_temp.SIF_Quantity__c =  oiNew.Invoice_Quantity__c;
                            newInventoryLines_temp.UOM__c = oiNew.Conversion__c;
                            newInventoryLines_temp.Inventory_Item__c =  existingInventoryItemsMap.get(itemKeyStr).id;
                            
                            inventoryLineToinsert.add(newInventoryLines_temp);
                        }else{
                            Decimal currentNewOrderItemConversionRate = 0.0;
                            currentNewOrderItemConversionRate = newOrderItemsConvertionRateMap.get(oiNew.Conversion__c);
                            //if good. add condition.
                            //needs conversion on quantity.
                            if(existingInventoryLinesMap_Return.get(lineKeyStr).SIF_Quantity__c==null){
                                existingInventoryLinesMap_Return.get(lineKeyStr).SIF_Quantity__c=0;
                            }
                            system.debug('%%t starting');
                            system.debug('%%t existingInventoryLinesMap_Return.get(lineKeyStr).SIF_Quantity__c:'+existingInventoryLinesMap_Return.get(lineKeyStr).SIF_Quantity__c);
                            system.debug('%%t existingInventoryLinesMap_Return.get(lineKeyStr).UOM__r.Conversion_Rate__c:'+existingInventoryLinesMap_Return.get(lineKeyStr).UOM__r.Conversion_Rate__c);
                            
                            totalRawReturnQty = existingInventoryLinesMap_Return.get(lineKeyStr).SIF_Quantity__c * existingInventoryLinesMap_Return.get(lineKeyStr).UOM__r.Conversion_Rate__c;
                            
                            
                            if(oiNew.Invoice_Quantity__c == NULL) {
                                oiNew.Invoice_Quantity__c = 0;
                            }
                            
                            if(currentNewOrderItemConversionRate == NULL) {
                                currentNewOrderItemConversionRate = 0;
                            }
                            
                            
                            totalRawReturnQty+=oiNew.Invoice_Quantity__c*currentNewOrderItemConversionRate;
                            system.debug('%%t totalRawReturnQty:'+totalRawReturnQty);
                            // convert the raw quantity with respect to the current UOM.
                            existingInventoryLinesMap_Return.get(lineKeyStr).SIF_Quantity__c=totalRawReturnQty/existingInventoryLinesMap_Return.get(lineKeyStr).UOM__r.Conversion_Rate__c;
                            system.debug('%%t after totalRawReturnQty:'+totalRawReturnQty);
                            inventoryLineToUpdate.add(existingInventoryLinesMap_Return.get(lineKeyStr));  
                            inventoryLineToUpdateMap.put(existingInventoryLinesMap_Return.get(lineKeyStr).id, existingInventoryLinesMap_Return.get(lineKeyStr));                            
                        }
                    }else{ //Deduction of inventory from sales.
                        Decimal rawLineItemQtyAfterDeduct = 0;
                        Decimal rawWeightQtyAfterDeduct = 0;
                        if(existingInventoryLinesMap_Old.get(itemKeyStr)==null){
                            // No existing Invetory for the Item.
                            system.debug('@ without existing');
                            newInventoryLines_temp = new Inventory_Line__c();
                            newInventoryLines_temp.Batch_Code__c = '--NO STOCK--';
                            newInventoryLines_temp.SIF_Quantity__c =  oiNew.Invoice_Quantity__c;
                            newInventoryLines_temp.UOM__c = oiNew.Conversion__c;
                            newInventoryLines_temp.Inventory_Item__c =  existingInventoryItemsMap.get(itemKeyStr).id;
                            // Product with Catch Weight. START
                            if(oiNew.Product__r.Catch_weight__c){
                                if(newInventoryLines_temp.Weight_UOM__c==null){
                                    newInventoryLines_temp.Weight_Quantity__c = oiNew.Weight_Quantity__c;
                                    newInventoryLines_temp.Weight_UOM__c = oiNew.Weight_UOM__c;
                                }else{
                                    //convert quantity to raw and multiply to new coversion.
                                    totalRawWeighQtyToAdd =  oiNew.Weight_Quantity__c/oiNew.Weight_UOM__r.Conversion_Rate__c;
                                    newInventoryLines_temp.Weight_Quantity__c += totalRawWeighQtyToAdd * newInventoryLines_temp.Weight_UOM__r.Conversion_Rate__c;
                                }
                            }
                            // Product with Catch Weight. END
                            inventoryLineToinsert.add(newInventoryLines_temp);
                        }else if(existingInventoryLinesMap_Old.get(itemKeyStr).size()>0){

                            if(oiNew.Invoice_Quantity__c == NULL) {
                                oiNew.Invoice_Quantity__c = 0;
                            }

                            totalRawQtyToAdd = oiNew.Invoice_Quantity__c*oiNew.Conversion__r.Conversion_Rate__c;

                            if(oiNew.Product__r.Catch_weight__c){
                                if(oiNew.Weight_UOM__r.Conversion_Rate__c == null){
                                    totalRawWeighQtyToAdd =  oiNew.Weight_Quantity__c;
                                }else{
                                    totalRawWeighQtyToAdd =  oiNew.Weight_Quantity__c/oiNew.Weight_UOM__r.Conversion_Rate__c;
                                }
                                
                            }

                            for( Inventory_Line__c ilm: (existingInventoryLinesMap_Old.get(itemKeyStr))) {

                                if(oiNew.Product__c==ilm.Linekey__c.split('-')[2]){
 
                                     rawLineItemQtyAfterDeduct = ilm.Ending_Balance__c*ilm.UOM__r.Conversion_Rate__c - totalRawQtyToAdd;

                                     if(oiNew.Product__r.Catch_weight__c){
                                        Decimal wtQty = 0;
                                        Decimal convRate = 0;
                                        if(ilm.Weight_UOM__r.Conversion_Rate__c != null){
                                            wtQty = ilm.Weight_UOM__r.Conversion_Rate__c;
                                        }
                                        
                                        if(ilm.Weight_UOM__r.Conversion_Rate__c != null){
                                            convRate = ilm.Weight_UOM__r.Conversion_Rate__c;
                                        }
                                        
                                        if(totalRawWeighQtyToAdd == null){
                                            totalRawWeighQtyToAdd = 0;
                                        }
                                        
                                        rawWeightQtyAfterDeduct = wtQty*convRate - totalRawWeighQtyToAdd;
                                        
                                     }
                                    //catch null values and set to 0;
                                    if(ilm.SIF_Quantity__c == null){
                                        ilm.SIF_Quantity__c=0;
                                    }
                                    if(ilm.Depleted__c == false){

                                        if(inventoryLineToUpdateMap.get(ilm.id)!=null){
                                            ilm=inventoryLineToUpdateMap.get(ilm.id);
                                        }

                                        if(rawLineItemQtyAfterDeduct>=0){

                                            ilm.SIF_Quantity__c+=(totalRawQtyToAdd/ilm.UOM__r.Conversion_Rate__c).setScale(4);
                                            
                                            if(rawLineItemQtyAfterDeduct==0){
                                                ilm.Depleted__c =true;
                                            }
                                            totalRawQtyToAdd = 0;
                                            inventoryLineToUpdateMap.put(ilm.id, ilm);
                                            mapOrderIdLineKey.put(oiNew.Id, ilm.Linekey__c);
                                            break;
                                        }else if(ilm.Batch_Code__c == '--NO STOCK--'){// No inventory existing. add to unknown batchcode

                                            ilm.SIF_Quantity__c+=(totalRawQtyToAdd/ilm.UOM__r.Conversion_Rate__c).setScale(4);

                                            if(rawLineItemQtyAfterDeduct==0){
                                                //ilm.Depleted__c =true;
                                            }
                                            totalRawQtyToAdd = 0;
                                            inventoryLineToUpdateMap.put(ilm.id, ilm);
                                            mapOrderIdLineKey.put(oiNew.Id, ilm.Linekey__c);
                                            break;
                                        }else{

                                            ilm.SIF_Quantity__c+=ilm.Ending_Balance__c.setScale(4);
                                            system.debug(' %%t 2 newInventoryLines_temp.SIF_Quantity__c:'+ilm.SIF_Quantity__c);
                                            ilm.Depleted__c =true;
                                            totalRawQtyToAdd =rawLineItemQtyAfterDeduct*(-1);
                                        }
                                        inventoryLineToUpdateMap.put(ilm.id, ilm);
                                        system.debug('%%t ilm after['+ilm.id+']:'+ilm);
                                    }
                                }
                            }
                            if(totalRawQtyToAdd>0) {
                                newInventoryLines_temp = new Inventory_Line__c();
                                newInventoryLines_temp.Batch_Code__c = '--NO STOCK--';
                                system.debug(' %%t 1 before newInventoryLines_temp.SIF_Quantity__c:'+newInventoryLines_temp.SIF_Quantity__c);
                                newInventoryLines_temp.SIF_Quantity__c =  (totalRawQtyToAdd/oiNew.Conversion__r.Conversion_Rate__c).setScale(4);
                                system.debug(' %%t 1 newInventoryLines_temp.SIF_Quantity__c:'+newInventoryLines_temp.SIF_Quantity__c);
                                newInventoryLines_temp.UOM__c = oiNew.Conversion__c;
                                newInventoryLines_temp.Inventory_Item__c =  existingInventoryItemsMap.get(itemKeyStr).id;
                                inventoryLineToinsert.add(newInventoryLines_temp);

                            }
                             
                        }   
                    }
                }
                if(inventoryLineToinsert.size()>0){
                    insert inventoryLineToinsert;
                }

                inventoryLineToUpdate= inventoryLineToUpdateMap.values();
                if(inventoryLineToUpdate.size()>0){
                    update inventoryLineToUpdateMap.values();
                }
    }
    
    /* GAAC: METHOD ONLY BEING CALLED ON A BEFORE EVENT, THIS METHOD WILL NOT BE EXECUTED.
    public void validateWarehouseAndDistributorId(List<Order_Item__c> newOrderItemList){
        if(settings.orderItemOldTrigger_beforeUpdate__c == true){
            system.debug('^^^^^ before (validation) update insert trigger works.');
            Set<id> relatedOrderFormIds = new Set<id>();

            Id distributorDirectRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor/ Direct').getRecordTypeId();
            Id distributorCustomerRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
            
            Map <id, Order__c> SalesInformationMap = new Map <id, Order__c> ();
            Order__c orderTemp = new Order__c();
            for(Order_Item__c oi: newOrderItemList){
                relatedOrderFormIds.add(oi.Order_Form__c);
            }
            
            SalesInformationMap = new  Map <id, Order__c> ([SELECT id, Distributor_SFA_ID__c, RecordTypeID,  
                                            Account__r.Warehouse__c, Account__r.Distributor__r.Warehouse__c, Account__r.Distributor__c,
                                            Account__c, Sales_Information_Form__c, Account__r.RecordTypeID, Account__r.name
                                        FROM Order__c 
                                        WHERE Id IN: relatedOrderFormIds 
                                            AND (RecordTypeID = :actualSalesRecordTypeId OR RecordTypeID = :returnRecordTypeId )]);
                                            
            for(Order_Item__c oi: newOrderItemList){
                if(SalesInformationMap.get(oi.Order_Form__c)!=null){
                    orderTemp = SalesInformationMap.get(oi.Order_Form__c);
                    if(orderTemp.Account__r.RecordTypeID == distributorDirectRecordType || orderTemp.Account__r.RecordTypeID == distributorCustomerRecordType ){
                        oi.addError('No Warehouse releated to Account:'+orderTemp.Account__r.name+'. Please specify warehouse.');
                    }
                    if(String.isEmpty(orderTemp.Distributor_SFA_ID__c)){
                        oi.addError('Distributor Id :'+orderTemp.Distributor_SFA_ID__c+' not found.');
                    }
                }else{
                    oi.addError('SalesInformation id:'+oi.Order_Form__c+' not found.');
                }
                if(orderTemp.Account__r.Warehouse__c==null){
                    oi.addError('No Warehouse found for Account:'+orderTemp.Account__r.name+' with SFA ID:('+orderTemp.Account__r+') ');
                }
            }
        }
    } 
    */
}